import { withInfo } from '@storybook/addon-info';
import { configure, addDecorator } from '@storybook/react';
import addonBackgrounds from "@storybook/addon-backgrounds";
import styles from "@sambego/storybook-styles";
import { withThemesProvider } from 'storybook-addon-styled-component-theme';
import { league } from 'themes.js'

const req = require.context('../src/components', true, /\.stories\.(jsx|.js)$/)

function loadStories() {
  addDecorator((story, context) => withInfo({
    header: true, // Toggles display of header with component name and description
    inline: false, // Displays info inline vs click button to view
    source: true, // Displays the source of story Component
  })(story)(context));
  addDecorator(addonBackgrounds([
    { name: "League", value: "hsl(210, 90%, 6%)", default: true },
    { name: "white", value: "#ffffff" },
  ]));
  addDecorator(styles({
    fontFamily: 'Helvetica, Arial, sans-serif',
    color: '#fff',
  }));
  addDecorator(withThemesProvider([league]));
  req.keys().forEach((filename) => req(filename));
}

configure(loadStories, module);
