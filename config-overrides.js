const rewireReactHotLoader = require('react-app-rewire-hot-loader');

const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
  config = injectBabelPlugin(['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], config);
  config = rewireReactHotLoader(config, env);
  config = rewireLess.withLoaderOptions({
    javascriptEnabled: true,
    modifyVars: {
      // primary
      '@primary-color': 'hsl(210, 90%, 27%)',
      '@primary-1': 'hsl(210, 100%, 8%)',
      '@heading-color': 'hsl(211, 92%, 85%)',
      '@text-color': 'hsl(211, 92%, 95%)',
      '@text-color-secondary': 'fade(#fff, 45 %)',


      // background
      '@background-color-base': 'hsl(210, 90%, 6%)',
      '@background-color-light': 'hsl(210, 90%, 7%)',
      '@body-background': 'hsl(210, 90%, 4%)',
      '@component-background': 'hsl(210, 90%, 4%)',
      '@layout-body-background': 'fade(@body-background, 45 %)',
      '@input-bg': 'rgba(0,0,0,0)',
      '@table-selected-row-bg': 'hsl(210, 90%, 9%)',
      '@table-expanded-row-bg': '#00000000',
      '@btn-default-bg': 'hsl(210, 90%, 6%)',

      // Border color
      '@border-color-split': 'hsl(210, 90%, 9%)',
      '@border-color-base': 'hsl(210, 90%, 15%)',


      // Other
      '@disabled-color': 'hsl(210, 21%, 80%)',

    },
  })(config, env);
  return config;
};
