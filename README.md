# League of Legends Math Item Optimizer  
This is a replacement of the flash-based Item Optimizer for League of Legends. 

## Setup

To set up your local development area, clone this repository, install `yarn` globally, then run 
```cmd
yarn 
```
To run the development environment, you can do
```cmd
yarn start
```

## Docs
We have a storybook that is currently not hosted anywhere. You may run it with
```cmd
yarn run storybook
```
We make use of [Ant design](https://ant.design) for a lot of components. You can find their docs [here](https://ant.design/docs/react/introduce).

## Performance considerations  
Everything must be connected to the redux store separately to avoid unnecessary re-renders. For this we made the `ConnectedInput` container. It is important that every component can be connected to this container, therefore there are 2 rules when making an input component: 
1. The component shall have an `onChange` property (function), which takes the next value of the component.
2. The component shall have a `value` property (value), which will be set to the same value as the `onChange`.

It is still uncertain how large the application will become after it is done. We might want to switch to a model that loads the specific components as they appear. 

## Design
### Interconnectedness
Wherever possible, the user should be able to change parameters in context. For example, on the `Build Results` section, the user sees a bunch of items that have been recommended. The user should be able to interact with these items: clicking on one brings up a menu where the user may change parameters, block the item, etc.

---
This project was bootstrapped with create-react-app, for which you can find a readme [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

