import { INPUT_CHANGE } from 'Actions/types';

export const change = (path, value) => ({
  type: INPUT_CHANGE,
  payload: {
    path,
    value,
  },
});
