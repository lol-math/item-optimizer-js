import { CHANGE_BLOCKED_ITEMS, CHANGE_FORCED_ITEM } from './types';

export const changeBlockedItems = (targetKeys, direction, moveKeys) => ({
  type: CHANGE_BLOCKED_ITEMS,
  payload: { targetKeys, direction, moveKeys },
});
export const changeForcedItem = (itemIndex, itemId) => ({
  type: CHANGE_FORCED_ITEM,
  payload: { itemIndex, itemId },
});
