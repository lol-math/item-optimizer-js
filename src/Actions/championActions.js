import { SELECT_CHAMPION } from './types';

export const selectChampion = championKey => dispatch =>
  dispatch({
    type: SELECT_CHAMPION,
    payload: {
      championKey,
    },
  });
