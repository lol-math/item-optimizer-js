import { GET_VIDEOS } from 'Actions/types';

export const getVideos = () => (dispatch) => {
  fetch('http://localhost/League%20Of%20Legends/leagues.json')
    .then(res => res.json())
    .then(data =>
      dispatch({
        type: GET_VIDEOS,
        payload: data,
      }));
};
