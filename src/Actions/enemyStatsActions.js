import { RESET_ENEMY_STATS, GET_LEAGUES } from 'Actions/types';
import { Enemy_Stats as enemyStats } from 'item-optimizer-data';

export const resetEnemyStats = () => dispatch =>
  dispatch({
    type: RESET_ENEMY_STATS,
    payload: enemyStats,
  });

export const getLeagues = summonerId => dispatch =>
  fetch('http://localhost/League%20Of%20Legends/leagues.json')
    .then(res => res.json())
    .then(data =>
      dispatch({
        type: GET_LEAGUES,
        payload: data,
      }));
