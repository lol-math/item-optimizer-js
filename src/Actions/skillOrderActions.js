import { CHANGE_SKILL_ORDER } from './types';

export const changeSkillOrder = skillOrder => ({
  type: CHANGE_SKILL_ORDER,
  payload: skillOrder,
});
