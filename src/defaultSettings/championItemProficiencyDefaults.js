import { Champion_Item_Proficiency as championItemProficiency } from 'item-optimizer-data';
import _ from 'lodash';

export default championKey => ({
  ..._.cloneDeep(championItemProficiency[championKey]),
});
