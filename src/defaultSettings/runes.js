import { everyRunePage as runes } from 'item-optimizer-data';
import _ from 'lodash';

export default (championKey, role) =>
  _.cloneDeep(runes[championKey][role === 'bot' ? 'adc' : role][0].runes).map(rune => _.toInteger(rune));
