import { Champion_Ability_Set as championAbilitySet } from 'item-optimizer-data';

import _ from 'lodash';

export default championKey => ({
  ..._.cloneDeep(championAbilitySet[championKey]),
});
