import championSpecification from './championSpecificationDefaults';
import participants from './participantsDefaults';
import championAbilitySet from './championAbilitySetDefaults';
import combatScenarios from './combatScenarioDefaults';
import items from './itemDefaults';
import itemProficiency from './championItemProficiencyDefaults';
import runes from './runes';
import _ from 'lodash';

const forcedItems = () => _.cloneDeep(_.range(6).map(x => null));

export const getChampionDefaults = (championKey) => {
  const championSpecification2 = championSpecification(championKey);
  return {
    selectedChampion: championKey,
    championSpecification: championSpecification2,
    combatScenarios: combatScenarios(championKey),
    championAbilitySet: championAbilitySet(championKey),
    itemProficiency: itemProficiency(championKey),
    runes: runes(
      championKey,
      championSpecification2.laneorientation.toLowerCase(),
    ),
  };
};
export const getAllDefaults = championKey => ({
  participants,
  items,
  forcedItems: forcedItems(),
  ...getChampionDefaults(championKey),
});

export default getAllDefaults('Aatrox');
