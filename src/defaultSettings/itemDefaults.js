import { Item_Profile as itemProfile } from 'item-optimizer-data';
import _ from 'lodash';

export default _.cloneDeep(itemProfile);

