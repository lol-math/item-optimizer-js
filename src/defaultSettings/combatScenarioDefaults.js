import _ from 'lodash';
import { Champion_Battle_Set as combatScenarios } from 'item-optimizer-data';

export default championKey => ({
  ..._.cloneDeep(combatScenarios[championKey]),
  preset: 'default',
});
