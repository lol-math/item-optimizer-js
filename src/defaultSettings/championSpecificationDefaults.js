import { Champion_Specification as championSpecification } from 'item-optimizer-data';
import _ from 'lodash';

export default championKey => ({
  ..._.set(
    _.assignIn(_.cloneDeep(championSpecification[championKey]), {
      healingTimeOutOfCombat: [8, 6, 4],
    }),
    'buildtype[2]',
    1,
  ),
});
