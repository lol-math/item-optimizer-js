import React from 'react';
import { List, Avatar } from 'antd';

/**
 * https://developers.google.com/apis-explorer/#p/youtube/v3/youtube.playlistItems.list?part=snippet&playlistId=PLanVj8zt_WyeaWK8cvp1ugq1Bz4FPO5Zl&_h=4&
 *
 * Example playlist ID: PLanVj8zt_WyeaWK8cvp1ugq1Bz4FPO5Zl
 * part: snippet
 */

const ApiKey = 'AIzaSyDdbZIEmQJYihVHgazy0XplK7DYsD-Dcac';
class Videos extends React.Component {
  state = {
    listData: [],
  };
  componentDidMount = () => {
    fetch(`https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=PLanVj8zt_WyeaWK8cvp1ugq1Bz4FPO5Zl&key=${ApiKey}`)
      .then(result => result.json())
      .then((json) => {
        if (json.items) {
          this.setState({
            listData: json.items,
          });
        }
      });
  };
  render = () => (
    <div
      style={{
        padding: 24,
      }}
    >
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 3,
        }}
        dataSource={this.state.listData}
        renderItem={item => (
          <List.Item
            key={item.id}
            extra={
              <img
                width={272}
                alt="logo"
                src={item.snippet.thumbnails.medium.url}
              />
            }
            style={{
              padding: 24,
            }}
          >
            <List.Item.Meta
              avatar={<Avatar src={item.avatar} />}
              title={<a href={item.href}>{item.snippet.title}</a>}
              description={item.snippet.description}
            />
          </List.Item>
        )}
      />
    </div>
  );
}

export default Videos;
