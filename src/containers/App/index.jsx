import React from 'react';
import { Layout, Modal } from 'antd';

import styled from 'styled-components';
import MenuBar from 'components/MenuBar';
import { Route, Switch } from 'react-router'; // react-router v4
import routes from './routes';

const AppWrapper = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

const ContentWrapper = styled.div`
  height: 100%;
  display: flex;
  overflow: hidden;
  > * {
    overflow-y: auto;
  }
`;

export default () => (
  <AppWrapper>
    <MenuBar routes={routes} />
    <ContentWrapper>
      <Switch>
        {routes.map(route => (
          <Route exact={route.exact} path={route.to} render={route.render} key={route.to} />
          ))}
        <Route exact path="/" render={() => <div>Match</div>} />
        <Route render={() => <div>Miss</div>} />
      </Switch>
    </ContentWrapper>
  </AppWrapper>
);
