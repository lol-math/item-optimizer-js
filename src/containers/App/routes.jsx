import ItemOP from 'containers/ItemOP';
import LandingPage from 'containers/LandingPage';
import React from 'react';
import Videos from 'containers/Videos';

export default [
  {
    label: 'Home',
    key: 'home',
    to: '/',
    exact: true,
    render: () => <LandingPage />,
  },
  {
    label: 'Item OP',
    key: 'itemop',
    to: '/itemop',
    render: () => <ItemOP />,
  },
  {
    label: 'Item Sets',
    key: 'itemsets',
    to: '/itemsets',
  },
  {
    label: 'Articles',
    key: 'articles',
    to: '/articles',
  },
  {
    label: 'Videos',
    key: 'videos',
    to: '/videos',
    render: () => <Videos />,
  },
  {
    label: 'Donate',
    key: 'donate',
    to: '/donate',
  },
];

