import React, { Children } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

const ConnectedValue = ({ path, childProps, children }) => {
  const component = ({ value }) => (children ? children(value) : value);
  const mapStateToProps = state => ({
    value: _.get(state.settings, path),
  });
  const ComposedComponent = connect(mapStateToProps)(component);
  return Children.only(<ComposedComponent {...childProps} />);
};
ConnectedValue.propTypes = {
  component: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
  childProps: PropTypes.object.isRequired,
  children: PropTypes.node,
};

export default ConnectedValue;
