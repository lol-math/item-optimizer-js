import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { change } from 'Actions/fieldActions';

const ConnectedInput = ({ component, path, childProps }) => {
  const mapDispatchToProps = (dispatch, ownProps) => ({
    onChange: value => dispatch(change(path, value)),
  });
  const mapStateToProps = (state, ownProps) => ({
    value: _.get(state.settings, path),
  });
  const ComposedComponent = connect(mapStateToProps, mapDispatchToProps)(component);
  return <ComposedComponent {...childProps} />;
};
ConnectedInput.propTypes = {
  component: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
  childProps: PropTypes.object.isRequired,
};

export default ConnectedInput;
