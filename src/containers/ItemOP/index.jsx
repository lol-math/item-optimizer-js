import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import FaTint from 'react-icons/lib/fa/tint';

import { LolRunes, Battle } from 'Images/Icons';
import 'Font/battleIcon/iconfont.css';
import Button from 'components/Button';
import Results from 'containers/Results';
import ScrollBar from 'components/ScrollBar';
import styled from 'styled-components';
import SideMenu from 'components/SideMenu';
import { Route, Switch } from 'react-router'; // react-router v4

/**
 * Chat
 */
import Chat from 'containers/Chat';

import routes from './routes';
import './ItemOP.css';

const ItemOPWindow = styled.div`
  padding-right: 10px;
`;

const MenuScrollRegion = styled(ScrollBar).attrs({
  style: {
    display: 'flex',
    flexDirection: 'column',
    width: 200,
    flexShrink: 0,
  },
})`
  border-right: 1px  ${({ theme }) => theme.border.grey};
  border-right-style: solid;
`;

export default () => [
  <MenuScrollRegion>
    <ItemOPWindow>
      <div style={{ display: 'flex', marginTop: 5 }}>
        <Button>Live Game</Button>
      </div>
      <SideMenu routes={routes.settings} header="Settings" />
      <SideMenu routes={routes.results} header="Results" />
    </ItemOPWindow>
  </MenuScrollRegion>,
  <ScrollBar>
    <ItemOPWindow>
      <Layout.Content>
        {[...routes.settings, ...routes.results].map(route => (
          <Route
            path={`/itemop/${route.key}`}
            render={() =>
              (route.noPadding ? (
                route.component
              ) : (
                <div style={{ padding: 10 }}>{route.component} </div>
              ))
            }
          />
        ))}
      </Layout.Content>
    </ItemOPWindow>
  </ScrollBar>,
  <ScrollBar
    style={{
      flexShrink: 0,
      width: 600,
      flexGrow: 0,
    }}
  >
    <ItemOPWindow>
      <Layout.Content style={{ padding: 25 }}>
        <Results />
      </Layout.Content>
    </ItemOPWindow>
  </ScrollBar>,
  <Layout.Sider>
    <ItemOPWindow>
      <Chat />
    </ItemOPWindow>
  </Layout.Sider>,
];
