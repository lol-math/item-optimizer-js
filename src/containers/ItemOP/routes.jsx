import React from 'react';
import { Icon } from 'antd';
import { LolRunes, Battle } from 'Images/Icons';
import FaTint from 'react-icons/lib/fa/tint';

import QuickSettings from 'containers/Settings/General';
import CombatScenarioSettings from 'containers/Settings/CombatScenario';
import ItemSettings from 'containers/Settings/BlockItems/Items';
import ManaSettings from 'containers/Settings/Mana';
import HealthSettings from 'containers/Settings/Health';
import RunesSettings from 'containers/Settings/Runes';
import SkillOrderSettings from 'containers/Settings/SkillOrder';
import ChampionSettings from 'containers/Settings/Champion';
import ItemUtility from 'containers/Settings/ItemUtility';
import ItemBlock from 'containers/Settings/BlockItems';
import CurrentGame from 'containers/Settings/CurrentGame';
import Abilities from 'containers/Settings/Abilities';
import ItemProficiency from 'containers/Settings/ItemProficiency';
import Test from 'containers/Test';
import ExportSettings from 'containers/ExportSettings';

/**
 * Menu Page: After calculation (results)
 */
import BuildResult from 'containers/Results';
import BuildCompare from 'containers/Compare';

export default {
  settings: [
    {
      key: 'quick',
      component: <QuickSettings />,
      icon: <Icon type="rocket" />,
      label: 'Quick',
    },
    {
      key: 'game',
      component: <CurrentGame />,
      icon: <Icon type="rocket" />,
      label: 'In Game',
    },
    {
      key: 'champion',
      component: <ChampionSettings />,
      noPadding: true,
      icon: <Icon type="rocket" />,
      label: 'Champion',
    },
    {
      key: 'mana',
      component: <ManaSettings />,
      icon: <FaTint className="anticon" />,
      label: 'Mana',
    },
    {
      key: 'health',
      component: <HealthSettings />,
      icon: <FaTint className="anticon" />,
      label: 'Health',
    },
    {
      key: 'combat',
      component: <CombatScenarioSettings />,
      icon: <Battle className="anticon" />,
      label: 'Combat Scenario',
    },
    {
      key: 'items',
      component: <ItemSettings />,
      noPadding: true,
      icon: <Battle className="anticon" />,
      label: 'Items',
    },
    {
      key: 'item-utility',
      component: <ItemUtility />,
      noPadding: true,
      icon: <Battle className="anticon" />,
      label: 'Item Utility',
    },
    {
      key: 'item-block',
      component: <ItemBlock />,
      icon: <Battle className="anticon" />,
      label: 'Item Block',
    },
    {
      key: 'item-proficiency',
      component: <ItemProficiency />,
      icon: <Battle className="anticon" />,
      label: 'Item Proficiency',
    },
    {
      key: 'runes',
      component: <RunesSettings />,
      icon: <LolRunes className="anticon" />,
      label: 'Runes',
    },
    {
      key: 'skills',
      component: <SkillOrderSettings />,
      icon: <Battle className="anticon" />,
      label: 'Skill Order',
    },
    {
      key: 'abilities',
      component: <Abilities />,
      icon: <Battle className="anticon" />,
      label: 'Abilities',
    },
    {
      key: 'export',
      component: <ExportSettings />,
      icon: <Icon type="setting" />,
      label: 'Export Settings',
    },
    {
      key: 'test',
      component: <Test />,
      icon: <Icon type="setting" />,
      label: 'Test',
    },
  ],
  results: [
    {
      key: 'results',
      component: <BuildResult />,
      icon: <Icon type="setting" />,
      label: 'Results',
    },
    {
      key: 'compare-items',
      component: <BuildCompare />,
      icon: <Icon type="setting" />,
      label: 'Compare Items',
    },
  ],
};
