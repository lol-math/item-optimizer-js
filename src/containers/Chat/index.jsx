import React, { Component } from 'react';
import { Icon, Mention, Avatar } from 'antd';
// import { IconButton } from 'components/Button';
import { item } from 'item-optimizer-data';
import dd from 'utils/ddragon';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const POSTS_SUBSCRIPTION = gql`
  subscription {
    postAdded {
      author
      message
      id
    }
  }
`;

const POSTS_QUERY = gql`
  query {
    posts {
      author
      message
      id
    }
  }
`;

const PostsQuery = ({ children }) => (
  <Query query={POSTS_QUERY}>
    {({
 loading, error, data, subscribeToMore,
}) => {
      if (loading) {
        return <div style={{ paddingTop: 20 }}>Loading...</div>;
      }
      if (error) return <p>Error :(</p>;
      const subscribeToMorePosts = () => {
        subscribeToMore({
          document: POSTS_SUBSCRIPTION,
          updateQuery: (prev, { subscriptionData }) => {
            if (!subscriptionData.data || !subscriptionData.data.postAdded) {
              return prev;
            }
            const { postAdded } = subscriptionData.data;
            return Object.assign({}, prev, {
              posts: [...prev.posts, postAdded],
            });
          },
        });
      };

      return children(data.posts, subscribeToMorePosts);
    }}
  </Query>
);

const items = item.data;

const names = ['afc163', 'benjycui', 'yiminghe', 'jljsj33', 'dqaria', 'RaoHai'];
const itemValues = Object.values(items);

class Chat extends Component {
  constructor() {
    super();
    this.state = {
      suggestions: [],
    };
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  componentDidUpdate({ posts }) {
    if (
      this.props.posts.length !== posts &&
      this.lastElement &&
      posts.length !== 0 // Only scroll on updates. Don't scroll in the first request
    ) {
      this.lastElement.scrollIntoView({ behavior: 'smooth' });
    }
  }

  onSearchChange(value, trigger) {
    let suggestions = [];
    const searchValue = value.toLowerCase();
    if (trigger === '#') {
      const filtered = itemValues.filter(item => item.name.toLowerCase().indexOf(searchValue) !== -1);
      suggestions = filtered.map(suggestion => (
        <Mention.Nav value={suggestion.name} data={suggestion}>
          <Avatar
            src={dd.images.item(suggestion.image.full)}
            size="small"
            style={{
              width: 14,
              height: 14,
              marginRight: 8,
              top: 2,
              position: 'relative',
            }}
          />
          {suggestion.name}
        </Mention.Nav>
      ));
    } else if (trigger === '@') {
      suggestions = names.filter(item => item.indexOf(value) !== -1);
    }
    this.setState({ suggestions });
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          height: '100%',
        }}
      >
        <div style={{ flex: 1, padding: 5 }}>
          {this.props.posts.map(post => (
            <div key={post.id}>
              <span>{post.author}</span>: <span>{post.message}</span>
            </div>
          ))}
        </div>
        <div style={{ display: 'flex', padding: '0 2px 2px 2px' }}>
          {/* <IconButton >
            <Icon type="setting" style={{ fontSize: 20 }} />
          </IconButton> */}
          <Mention
            style={{ width: '100%' }}
            // onChange={onChange}
            placeholder="Message"
            prefix={['@', '#']}
            onSearchChange={this.onSearchChange}
            suggestions={this.state.suggestions}
            // onSelect={onSelect}
            notFoundContent="No match found"
          />
        </div>
      </div>
    );
  }
}


class ChatWrapper extends React.Component {
  componentDidMount() {
    this.props.subscribeToMorePosts();
  }
  render = () => [<Chat posts={this.props.posts} key={0} />];
}

export default () => (
  <PostsQuery>
    {(posts, subscribeToMorePosts) => (
      <ChatWrapper posts={posts} subscribeToMorePosts={subscribeToMorePosts} />
    )}
  </PostsQuery>
);
// <div className="Settings" >
// {/* Change to popover TODO */}
// <Button className="small" onClick={() => this.toggleSettingsMenu()}>Settings</Button>
// <div className="Menu" style={this.state.settingsMenu ? { display: 'flex' } : { display: 'none' }}>
//   <h1>Chat Settings</h1>
//   <div>
//     <label className="NameLabel" htmlFor="ChatSettingsMenuName">Name </label>
//     <input name="ChatSettingsMenuName" type="text" className="NameText" />
//   </div>
//   <div>
//     <label className="NameLabel" htmlFor="ChatSettingsMenuName">Color </label>
//     <colorpicker />
//   </div>
// </div>
// </div>
