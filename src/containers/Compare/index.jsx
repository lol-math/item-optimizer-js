import React, { Component } from 'react';
import { Table } from 'antd';

import './BuildCompare.less';

const durabilityColumns = [
  { title: 'Description', dataIndex: 'description', key: 'description' },
  {
    title: 'Build 1', dataIndex: 'build1', key: 'build1', className: 'column-value',
  },
  {
    title: 'Build 2', dataIndex: 'build2', key: 'build2', className: 'column-value',
  },
  { title: 'Difference', dataIndex: 'difference', key: 'difference' },
];
const damageColumns = [
  { title: 'Description', dataIndex: 'description', key: 'description' },
  {
    title: 'Build 1 (Damage)', dataIndex: 'build1', key: 'build1', className: 'column-value',
  },
  {
    title: 'Build 2 (Damage)', dataIndex: 'build2', key: 'build2', className: 'column-value',
  },
  { title: 'Difference', dataIndex: 'difference', key: 'difference' },
];
const utilityColumns = [
  { title: 'Description', dataIndex: 'description', key: 'description' },
  {
    title: 'Build 1 (Points)', dataIndex: 'build1', key: 'build1', className: 'column-value',
  },
  {
    title: 'Build 2 (Points)', dataIndex: 'build2', key: 'build2', className: 'column-value',
  },
  { title: 'Difference', dataIndex: 'difference', key: 'difference' },
];
const costColumns = [
  { title: 'Description', dataIndex: 'description', key: 'description' },
  {
    title: 'Build 1 (Gold)', dataIndex: 'build1', key: 'build1', className: 'column-value',
  },
  {
    title: 'Build 2 (Gold)', dataIndex: 'build2', key: 'build2', className: 'column-value',
  },
  { title: 'Difference', dataIndex: 'difference', key: 'difference' },
];
const healingColumns = [
  { title: 'Description', dataIndex: 'description', key: 'description' },
  {
    title: 'Build 1 (s)', dataIndex: 'build1', key: 'build1', className: 'column-value',
  },
  {
    title: 'Build 2 (s)', dataIndex: 'build2', key: 'build2', className: 'column-value',
  },
  { title: 'Difference', dataIndex: 'difference', key: 'difference' },
];

const tableOptions = {
  size: 'middle',
  pagination: false,
};

export default class BuildCompare extends Component {
  constructor(props) {
    super(props);
    this.durabilityData = [
      {
        key: 1,
        description: 'Hit Points (HP)',
      },
      {
        key: 2,
        description: 'Life Recovery (HP)',
      },
      {
        key: 3,
        description: 'Effective HP multiplier',
      },
      {
        key: 4,
        description: 'Total Effective HP',
      },
    ];
    this.damageData = [
      {
        key: 1,
        description: 'cmbtsc1',
      },
      {
        key: 2,
        description: 'cmbtsc2',
      },
      {
        key: 3,
        description: 'cmbtsc3',
      },
    ];
    this.utilityData = [
      {
        key: 1,
        description: 'Total Utility Points',
      },
    ];
    this.costData = [
      {
        key: 1,
        description: 'Items',
      },
      {
        key: 2,
        description: 'Insufficient Mana',
      },
      {
        key: 3,
        description: 'Special',
      },
      {
        key: 4,
        description: 'Gold Generation',
      },
      {
        key: 5,
        description: 'Net Gold Cost',
      },
    ];
    this.healingTime = [
      {
        key: 1,
        description: 'Surviving Time',
      },
      {
        key: 2,
        description: 'Out of Combat',
      },
      {
        key: 3,
        description: 'Total',
      },
    ];
  }
  renderDetailedRows(type, record) {
    switch (type) {
      case 'durability':
      default: return <div />;
    }
    return (
      <div />
    );
  }
  render() {
    return (
      <div className="BuildCompare">
        <Table
          title={() => 'Durability'}
          columns={durabilityColumns}
          dataSource={this.durabilityData}
          expandedRowRender={record => (
            <div style={{ margin: 0 }}>
              <p>Total HP/Heal/Shield that do not depend on how long you live.</p>
              <Table
                columns={[
                  { title: 'Description', dataIndex: 'description', key: 'description' },
                  {
                    title: 'Build 1 (Points)', dataIndex: 'build1', key: 'build1', className: 'column-value',
                  },
                  {
                    title: 'Build 2 (Points)', dataIndex: 'build2', key: 'build2', className: 'column-value',
                  },
                  { title: 'Difference', dataIndex: 'difference', key: 'difference' },
                ]
                }
                dataSource={[
                  { description: 'Base HP' },
                  { description: 'Ability HP' },
                  { description: 'Item HP' },
                  { description: 'Runes HP + Heal' },
                  { description: 'Item Heal/Shield' },
                  { description: 'One Time Heal / Shields' },
                  { description: 'One Time Health Costs' },
                  { description: 'Guaranteed HP' }]
                }

                size="small"
                pagination={false}
                bordered
              />
            </div>)}
          {...tableOptions}
        />
        <Table
          title={() => 'Damage'}
          columns={damageColumns}
          expandedRowRender={record => <p style={{ margin: 0 }}>{record.description}</p>}
          dataSource={this.damageData}
          {...tableOptions}
        />
        <Table
          title={() => 'Utility'}
          columns={utilityColumns}
          expandedRowRender={record => <p style={{ margin: 0 }}>{record.description}</p>}
          dataSource={this.utilityData}
          {...tableOptions}
        />
        <Table
          title={() => 'Costs'}
          columns={costColumns}
          dataSource={this.costData}
          {...tableOptions}
        />
        <Table
          title={() => 'Expected Healing Time'}
          columns={healingColumns}
          dataSource={this.healingTime}
          {...tableOptions}
        />
      </div>
    );
  }
}
