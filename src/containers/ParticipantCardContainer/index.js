import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ParticipantCardContainer extends Component {
  static propTypes = {
    prop: PropTypes,
  }

  render() {
    return (
      <div />
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  leagues: state.enemyStats.leagues,
  // participant: state.participants,
  attributes: state.enemyStats.enemyStats[championKeysFromIds[ownProps.participant.championId]].attributes,
});

export default connect(mapStateToProps, { getLeagues })(ParticipantCardContainer);
