import React from 'react';
import { Tabs } from 'antd';
import Button from 'components/Button';
import CombatScenarioSet from './CombatScenarioSet';

const CombatScenarioList = () => (
  // TODO: change Reset button to actually work.
  <Tabs defaultActiveKey="1" tabBarExtraContent={<Button >reset</Button>}>
    <Tabs.TabPane tab="Early Game" key="1">
      <CombatScenarioSet
        gamePhase="Early"
      />
    </Tabs.TabPane>
    <Tabs.TabPane tab="Mid Game" key="2" >
      <CombatScenarioSet
        gamePhase="Mid"
      />
    </Tabs.TabPane>
    <Tabs.TabPane tab="Late Game" key="3">
      <CombatScenarioSet
        gamePhase="Late"
      />
    </Tabs.TabPane>
  </Tabs>
);

export default CombatScenarioList;
