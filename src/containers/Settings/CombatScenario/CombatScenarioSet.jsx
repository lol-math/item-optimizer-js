import PropTypes from 'prop-types';
import { Collapse } from 'antd';
import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import ConnectedValue from 'containers/ConnectedValue';
import CombatScenario from './CombatScenario';


const CombatScenarioSet = ({ gamePhase, amountOfCombatScenarios }) => (
  <Collapse accordion bordered={false}>
    {_.range(amountOfCombatScenarios).map(i => (
      <Collapse.Panel
        header={
          <span>
            {i + 1}/{amountOfCombatScenarios} |&#160;
            <ConnectedValue path={`combatScenarios.${gamePhase}[${i}].Title`} /> |&#160;
            <ConnectedValue path={`combatScenarios.${gamePhase}[${i}].Value`} />%
          </span>
      }
        key={i}
      >
        <CombatScenario
          key={gamePhase}
          gamePhase={gamePhase}
          scenarioIndex={i}
        />
      </Collapse.Panel>
    ))}

  </Collapse>
);

CombatScenarioSet.propTypes = {
  gamePhase: PropTypes.string.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  amountOfCombatScenarios: state.settings.combatScenarios[ownProps.gamePhase].length,
});

export default connect(mapStateToProps)(CombatScenarioSet);
