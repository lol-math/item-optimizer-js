import React, { Component } from 'react';
import RadioButton from 'components/RadioButton';
import ConnectedInput from 'containers/ConnectedInput';
import ConnectedValue from 'containers/ConnectedValue';
import Disabled from 'components/Disabled';
import CombatScenarioSetGamephaseSet from './CombatScenarioSetGamephaseSet';

const Presets = ({ onChange, value }) => [
  <RadioButton
    checked={value === 'default'}
    onChange={() => onChange('default')}
  >
    Custom
  </RadioButton>,

  <RadioButton
    checked={value === 'marksman'}
    onChange={() => onChange('marksman')}
  >
    Marksman
  </RadioButton>,

  <RadioButton
    checked={value === 'caster'}
    onChange={() => onChange('caster')}
  >
    Caster
  </RadioButton>,
];


class CombatScenarioSettings extends Component {
  getPath = key => `combatScenarios.${key}`;
  render() {
    return (
      <div className="SettingsSection">
        <h2>Combat Scenario</h2>

        <h3>Presets</h3>
        <ConnectedInput path={this.getPath('preset')} component={Presets} />
        <ConnectedValue path={this.getPath('preset')} >{
          preset => (
            <Disabled disabled={preset !== 'default'} message="You cannot edit preset scenarios; please select 'Custom Scenario' if you want to edit this.">
              <CombatScenarioSetGamephaseSet />
            </Disabled>
          )
        }

        </ConnectedValue>
      </div>
    );
  }
}

export default CombatScenarioSettings;
