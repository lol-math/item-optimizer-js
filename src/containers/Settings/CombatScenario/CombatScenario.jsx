import React from 'react';
import PropTypes from 'prop-types';
import { Form, InputNumber } from 'antd';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { UnlabelledFormItem, LabelledFormItem } from 'components/Form/ColumnFormItem';

import CheckBox from 'components/CheckBox';
import EditableText from 'components/EditableText';
import { Slider } from 'components/Slider';
import { SpellImageWithDescription, PassiveImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';

import ConnectedInput from 'containers/ConnectedInput';
import ConnectedValue from 'containers/ConnectedValue';

const SpellInputWrapper = styled.div`
  flex-direction: column;
  border: 1px solid #3C3C41;
  margin: 5px;
  /* padding: 5px; */
  width: 64px;
  box-sizing: content-box;
`;

const SpellInput = ({
  title, onChange, value, children,
}) => (
  <SpellInputWrapper>
    <h4 style={{ textAlign: 'center', marginBottom: 5 }}>{title}</h4>
    <InputNumber
      onChange={onChange}
      value={value}
      style={{ width: '64px' }}
    />

    {children}
  </SpellInputWrapper>
);
SpellInput.propTypes = {
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
};

const CombatScenarioListItem = ({ gamePhase, scenarioIndex, championKey }) => {
  const getPath = key => `combatScenarios.${gamePhase}[${scenarioIndex}].${key}`;
  return (
    <div
      className="CombatScenarioListItem"
    >
      <Form
        layout="horizontal"
      >
        <LabelledFormItem
          label="Title"
        >
          <h3>
            <ConnectedInput component={EditableText} path={getPath('Title')} />
          </h3>
        </LabelledFormItem>
        <UnlabelledFormItem >
          <ConnectedInput
            component={CheckBox}
            path={getPath('ActivateItems')}
            childProps={{
              children: 'Activate Items',
            }}
          />
        </UnlabelledFormItem>
        <UnlabelledFormItem >
          <ConnectedInput
            component={CheckBox}
            path={getPath('IgnoreCDR')}
            childProps={{
              children: 'Burst Attack',
            }}
          />
        </UnlabelledFormItem>
        <UnlabelledFormItem >
          <ConnectedInput
            component={CheckBox}
            path={getPath('IgnoreLowRangeItems')}
            childProps={{
              children: 'Ranged Attack',
            }}
          />
        </UnlabelledFormItem>
        <UnlabelledFormItem >
          <ConnectedInput
            component={CheckBox}
            path={getPath('LiandryDoubleDamage')}
            childProps={{
              children: 'Does Liandry&apos;s deal double damage?',
            }}
          />

        </UnlabelledFormItem>
        <LabelledFormItem
          label="Number of Targets"
        >
          <ConnectedInput
            component={Slider}
            path={getPath('NumberOfTargets')}
            childProps={{
              min: 1,
              max: 5,
              step: 0.5,
            }}
          />
        </LabelledFormItem>
        <LabelledFormItem
          label="Weight"
        >
          <ConnectedInput
            component={Slider}
            path={getPath('Value')}
            childProps={{
              min: 0,
              max: 100,
              step: 1,
            }}
          />

        </LabelledFormItem>
        <LabelledFormItem
          label="Combat Duration"
        >
          <ConnectedInput
            component={InputNumber}
            path={getPath('Duration')}
          />

        </LabelledFormItem>

        <LabelledFormItem
          label="Auto Attack Seconds"
        >
          <ConnectedValue path={getPath('Duration')}>{
          max => (
            <ConnectedInput
              component={Slider}
              path={getPath('C')}
              childProps={{
              min: 0,
              max,
              step: 0.5,
            }}
            />
          )
        }
          </ConnectedValue>

          {/* TODO: change max duration to total duration. */}
          {/* <Slider
            min={0}
            max={this.props.combatScenario.Duration}
            step={0.5}
            value={this.props.combatScenario.C}
            onChange={this.handleChange}
            name="C"
          /> */}
        </LabelledFormItem>
        {console.log(<ConnectedValue path={getPath('Duration')} />)}
        <UnlabelledFormItem>
          <div className="Abilities" style={{ display: 'flex', flexWrap: 'wrap' }}>
            <ConnectedInput
              component={SpellInput}
              path={getPath('P')}
              childProps={{
              children: <PassiveImageWithDescription
                championKey={championKey}
              />,
              title: 'Passive',
            }}
            />
            <ConnectedInput
              component={SpellInput}
              path={getPath('Q')}
              childProps={{
              children: <SpellImageWithDescription
                championKey={championKey}
                size={64}
                spellId={0}
              />,
              title: 'Q',

            }}
            />
            <ConnectedInput
              component={SpellInput}
              path={getPath('W')}
              childProps={{
              children: <SpellImageWithDescription
                championKey={championKey}
                size={64}
                spellId={1}
              />,
              title: 'W',
            }}
            />
            <ConnectedInput
              component={SpellInput}
              path={getPath('E')}
              childProps={{
              children: <SpellImageWithDescription
                championKey={championKey}
                size={64}
                spellId={2}
              />,
              title: 'E',
            }}
            />
            <ConnectedInput
              component={SpellInput}
              path={getPath('R')}
              childProps={{
              children: <SpellImageWithDescription
                championKey={championKey}
                size={64}
                spellId={3}
              />,
              title: 'R',
            }}
            />

          </div>
        </UnlabelledFormItem>
      </Form>
    </div>
  );
};
// CombatScenarioListItem.defaultProps = {
//   combatScenario: {
//     Title: 'New Scenario Title',
//     // ActivateItems: false,
//   },
// };
CombatScenarioListItem.propTypes = {
  gamePhase: PropTypes.string.isRequired,
  scenarioIndex: PropTypes.number.isRequired,
  championKey: PropTypes.string.isRequired,
};
const mapStateToProps = state => ({
  championKey: state.settings.selectedChampion,
});

export default connect(mapStateToProps)(CombatScenarioListItem);
