import React from 'react';
import PropTypes from 'prop-types';
import { Form, InputNumber } from 'antd';
import ConnectedInput from 'containers/ConnectedInput';

const FormItem = Form.Item;

const getPath = key => `championSpecification.${key}`;

const HealthSettings = () =>
  (
    <div>

      <h1>Health and Healing</h1>
      <h2>Healing Time in Combat Until death without items</h2>
      <p>The tankier you are, the more your healing/shielding time improves</p>
      <Form
        layout="inline"
      >
        <FormItem
          label="Early"
        >
          <ConnectedInput path={getPath('ExpectedTimeLive[0]')} component={InputNumber} childProps={{ step: 0.5 }} />
        </FormItem>
        <FormItem
          label="Mid"
        >
          <ConnectedInput path={getPath('ExpectedTimeLive[1]')} component={InputNumber} childProps={{ step: 0.5 }} />
        </FormItem>
        <FormItem
          label="Late"
        >
          <ConnectedInput path={getPath('ExpectedTimeLive[2]')} component={InputNumber} childProps={{ step: 0.5 }} />
        </FormItem>
      </Form>
      <h2>Healing Time Out of Combat</h2>
      <Form
        layout="inline"
      >
        <FormItem
          label="Early"
        >
          <ConnectedInput path={getPath('healingTimeOutOfCombat[0]')} component={InputNumber} childProps={{ step: 0.5 }} />
        </FormItem>
        <FormItem
          label="Mid"
        >
          <ConnectedInput path={getPath('healingTimeOutOfCombat[1]')} component={InputNumber} childProps={{ step: 0.5 }} />
        </FormItem>
        <FormItem
          label="Late"
        >
          <ConnectedInput path={getPath('healingTimeOutOfCombat[2]')} component={InputNumber} childProps={{ step: 0.5 }} />
        </FormItem>
      </Form>
      <h2>Invulnerability Time</h2>
      <Form>

        <Form.Item >
          <ConnectedInput path={getPath('ExtraSurvivingTime')} component={InputNumber} childProps={{ step: 0.5 }} />
        </Form.Item>
      </Form>
    </div>
  );


HealthSettings.propTypes = {
};
export default HealthSettings;
