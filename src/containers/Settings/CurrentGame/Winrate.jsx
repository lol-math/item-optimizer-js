import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import RatingBar from 'components/RatingBar';

import Bronze from 'Images/Ranks/bronze.png';
import Silver from 'Images/Ranks/silver.png';
import Gold from 'Images/Ranks/gold.png';
import Platinum from 'Images/Ranks/platinum.png';
import Diamond from 'Images/Ranks/diamond.png';
import Master from 'Images/Ranks/master.png';
import Challenger from 'Images/Ranks/challenger.png';
import Unranked from 'Images/Ranks/empty_logo.png';
import ChampionCardSection from 'containers/Settings/CurrentGame/ChampionCardSection';

import ChampionCardDivider from 'containers/Settings/CurrentGame/ChampionCardDivider';

const Rank = styled.span`
  margin-left: 10px;
`;

const getImageForTier = (tier) => {
  switch (tier) {
    case 'BRONZE': return Bronze;
    case 'SILVER': return Silver;
    case 'GOLD': return Gold;
    case 'PLATINUM': return Platinum;
    case 'DIAMOND': return Diamond;
    case 'MASTER': return Master;
    case 'CHALLENGER': return Challenger;
    default: return Unranked;
  }
};
const Winrate = ({
  winrate,
  tier,
  rank,
  leaguePoints,
}) => (
  <div style={{ textAlign: 'center' }}>
    <ChampionCardDivider>{winrate && `Winrate: ${(winrate * 100).toFixed(1)}%` }</ChampionCardDivider>
    <ChampionCardSection>
      {winrate && <RatingBar rating={winrate} minRating={0.40} maxRating={0.60} />}
      <div>
        <img src={getImageForTier(tier)} alt={tier || 'UNRANKED'} height={48} width={48} />
        <Rank>{winrate ? `${tier} ${rank} (${leaguePoints}LP)` : 'UNRANKED'}</Rank>
      </div>
    </ChampionCardSection>
  </div>
);

Winrate.propTypes = {
  winrate: PropTypes.number.isRequired,
  tier: PropTypes.string.isRequired,
  rank: PropTypes.string.isRequired,
  leaguePoints: PropTypes.number.isRequired,
};
export default Winrate;

