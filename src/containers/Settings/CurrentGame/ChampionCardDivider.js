import {Divider} from 'antd';
import styled from 'styled-components';

export default styled(Divider)`
  margin-top: 10px !important;
  margin-bottom: 10px !important;
`;
