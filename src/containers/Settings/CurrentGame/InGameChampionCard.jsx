import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Tabs, Modal, Form } from 'antd';
import Checkbox from 'components/CheckBox';
import { LolRunes } from 'Images/Icons';

import summonerKeys from 'Json/summonerKeys.json';
import { getLeagues } from 'Actions/enemyStatsActions';
import {
  championFull,
} from 'item-optimizer-data';
import Button from 'components/Button';
import ConnectedInput from 'containers/ConnectedInput';
import InGameChampionCardStats from 'containers/Settings/CurrentGame/InGameChampionCardStats';
import InGameChampionCardWrapper from 'containers/Settings/CurrentGame/InGameChampionCardWrapper';
import DisplayChampionAndSummoners from 'containers/Settings/CurrentGame/DisplayChampionAndSummoners';
import Winrate from 'containers/Settings/CurrentGame/Winrate';
import ChampionCardSection from 'containers/Settings/CurrentGame/ChampionCardSection';
import ChampionCardDivider from 'containers/Settings/CurrentGame/ChampionCardDivider';
import RunePage from 'components/RunePage';
import { Slider } from 'components/Slider';
import CustomFormItem from 'components/Form/CustomFormItem';

const { TabPane } = Tabs;
const championKeysFromIds = championFull.keys;
const champions = championFull.data;


class InGameChampionCard extends React.Component {
  state = {
    modalOpen: false,
  }

  showModal = () => {
    this.setState({
      modalOpen: true,
    });
  }
  handleOk = () => {
    this.setState({
      modalOpen: false,
    });
  }
  handleCancel = () => {
    this.setState({
      modalOpen: false,
    });
  }

  render = () => {
    const {
      enemy, participantIndex, championId, spell1Id, spell2Id, summonerName, leagues,
    } = this.props;
    const championKey = championKeysFromIds[championId];
    let winrate;
    if (leagues.length > 0) {
      winrate = leagues[0].wins / (leagues[0].wins + leagues[0].losses);
    }
    const {
      tier, rank, leaguePoints,
    } = leagues[0];
    const getPath = key => `participants.${enemy ? 'enemy' : 'ally'}[${participantIndex}]${key}`;
    return (
      <InGameChampionCardWrapper>

        <DisplayChampionAndSummoners
          championKey={championKey}
          summonerKey1={summonerKeys[spell1Id]}
          summonerKey2={summonerKeys[spell2Id]}
          summonerName={summonerName}
        />
        <Winrate
          winrate={winrate}
          tier={tier}
          rank={rank}
          leaguePoints={leaguePoints}
        />

        <ChampionCardDivider />
        <ChampionCardSection>
          <ConnectedInput
            component={Checkbox}
            path={getPath('earlyEnemy')}
            childProps={{
              children: 'Early Enemy',
            }}
          />
        </ChampionCardSection>
        <Tabs defaultActiveKey="0" tabBarGutter={0}>
          <TabPane tab="Early" key="0"><InGameChampionCardStats gamePhase="Early" participantIndex={participantIndex} enemy={enemy} /></TabPane>
          <TabPane tab="Mid" key="1"><InGameChampionCardStats gamePhase="Mid" participantIndex={participantIndex} enemy={enemy} /></TabPane>
          <TabPane tab="Late" key="2"><InGameChampionCardStats gamePhase="Late" participantIndex={participantIndex} enemy={enemy} /></TabPane>
        </Tabs>
        <div
          style={{
          padding: 10,
        }}
        >
          <Form
            style={{ textAlign: 'left' }}
            layout="vertical"
          >
            <CustomFormItem label="HP"><ConnectedInput path={getPath('stats.ratings.health')} component={Slider} childProps={{ min: 0, max: 10, step: 1 }} /></CustomFormItem>
            <CustomFormItem label="Armor"><ConnectedInput path={getPath('stats.ratings.armor')} component={Slider} childProps={{ min: 0, max: 10, step: 1 }} /></CustomFormItem>
            <CustomFormItem label="MR"><ConnectedInput path={getPath('stats.ratings.resist')} component={Slider} childProps={{ min: 0, max: 10, step: 1 }} /></CustomFormItem>
          </Form>
        </div>
        <div style={{ display: 'flex' }}>
          <Button size="default" onClick={this.showModal}>
            <LolRunes className="anticon" />
            <span>Runes</span>
          </Button>
        </div>
        <Modal
          title={<span>Runes for {summonerName} - {champions[championKey].name}</span>}
          visible={this.state.modalOpen}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width={700}
          footer={null}
        >
          <ConnectedInput
            component={RunePage}
            path={getPath('perks.perkIds')}
          />
        </Modal>

      </InGameChampionCardWrapper>
    );
  }
}


InGameChampionCard.propTypes = {
  participantIndex: PropTypes.number.isRequired,
  enemy: PropTypes.bool.isRequired,
  leagues: PropTypes.array.isRequired,
};
InGameChampionCard.defaultProps = {
};

const mapStateToProps = (state, ownProps) => ({
  championId: state.settings.participants[ownProps.enemy ? 'enemy' : 'ally'][ownProps.participantIndex].championId,
  summonerName: state.settings.participants[ownProps.enemy ? 'enemy' : 'ally'][ownProps.participantIndex].summonerName,
  spell1Id: state.settings.participants[ownProps.enemy ? 'enemy' : 'ally'][ownProps.participantIndex].spell1Id,
  spell2Id: state.settings.participants[ownProps.enemy ? 'enemy' : 'ally'][ownProps.participantIndex].spell2Id,
  leagues: state.settings.participants[ownProps.enemy ? 'enemy' : 'ally'][ownProps.participantIndex].leagues,
});

export default connect(mapStateToProps, { getLeagues })(InGameChampionCard);

