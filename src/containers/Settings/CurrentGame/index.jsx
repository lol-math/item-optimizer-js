import React from 'react';
import InGameChampionCard from 'containers/Settings/CurrentGame/InGameChampionCard';
import PropTypes from 'prop-types';

import _ from 'lodash';
import { connect } from 'react-redux';

const teamWrapperStyle = {
  overflowX: 'auto',
  whiteSpace: 'nowrap',
  marginBottom: 10,
};

const CurrentGame = ({ amountOfAllies, amountOfEnemies }) => (
  <div>
    <h2>Current Game</h2>
    <div
      style={teamWrapperStyle}
    >
      {_.range(amountOfAllies).map(participant => (
        <InGameChampionCard
          enemy={false}
          participantIndex={participant}
        />
          ))}
    </div>
    <div
      style={teamWrapperStyle}
    >
      {_.range(amountOfEnemies).map(participant => (
        <InGameChampionCard
          enemy
          participantIndex={participant}
        />
          ))}
    </div>
  </div>

);
CurrentGame.propTypes = {
  amountOfAllies: PropTypes.number.isRequired,
  amountOfEnemies: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  amountOfAllies: state.settings.participants.ally.length,
  amountOfEnemies: state.settings.participants.enemy.length,
});
export default connect(mapStateToProps)(CurrentGame);
