import React from 'react';
import TriSlider from 'tri-slider';

import t from 'prop-types';
import { ToolTip as LeagueToolTip } from 'components/ToolTip';
import { Icon } from 'antd';
import KnobImage from 'Images/slider-btn.png';
import ConnectedInput from 'containers/ConnectedInput';


const AttributeLabel = ({ title, val, tooltipTitle }) => (
  <span>
    <LeagueToolTip title={tooltipTitle}>
      <span>{title}:</span>{' '}
      <span>{(val * 100).toFixed(0)}</span>
      <span>%</span>
    </LeagueToolTip>
  </span>
);
AttributeLabel.propTypes = {
  title: t.string.isRequired,
  val: t.number.isRequired,
  tooltipTitle: t.string.isRequired,
};
const GamePhaseDictionary = {
  Early: 0,
  Mid: 1,
  Late: 2,
};

const InGameChampionCardStats = ({
  gamePhase,
  enemy,
  participantIndex,
}) => (
  <div
    style={{
        textAlign: 'center',
      }}
  >
    <div
      style={{ display: 'inline-block', textAlign: 'left' }}
    >
      <h3>Damage Type</h3>
      <div
        style={{ float: 'right' }}
      >
        <LeagueToolTip title="The percentages are based on raw damage stats and therefore true damage might seem to be less than expected">
          <Icon type="question-circle-o" />
        </LeagueToolTip>
      </div>
      <ConnectedInput
        component={TriSlider}
        path={`participants.${enemy ? 'enemy' : 'ally'}[${participantIndex}]stats.attributes[${GamePhaseDictionary[gamePhase]}]`}
        childProps={{
            labelA: val => <AttributeLabel title="P" val={val} tooltipTitle="Physical Damage" />,
            labelB: val => <AttributeLabel title="M" val={val} tooltipTitle="Magical Damage" />,
            labelC: val => <AttributeLabel title="T" val={val} tooltipTitle="True Damage" />,
            knob: (<div
              style={{
                background: `url(${KnobImage})`,
                backgroundSize: '100%',
                height: 30,
                width: 30,
              }}
            />),
            size: 130,
            backgroundColor: 'hsl(210, 95%, 12%)',
          }}
      />
    </div>
  </div>
);
InGameChampionCardStats.propTypes = {
  gamePhase: t.string.isRequired,
  enemy: t.bool.isRequired,
  participantIndex: t.number.isRequired,
};

export default InGameChampionCardStats;
