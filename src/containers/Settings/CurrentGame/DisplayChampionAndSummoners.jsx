import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { SummonerImageWithDescription, TileImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';

const NameAndSpellWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 10px;
  pointer-events:none;
`;
const Name = styled.div`
  bottom: 0;
  left: 0;
  right: 0;
  position: absolute;
  padding-top: 40px;
  text-align: center;
  background: linear-gradient(to bottom, #00000000 00%, ${({ theme }) => theme.background.main} 100%);
  a{
    color: ${({ theme }) => theme.font.bright};
    pointer-events: auto;
    font-size: 20px;
  }
  background:   linear-gradient(
    to top,
    hsla(210, 90%, 4%, 1) 0%,
    hsla(210, 90%, 4%, 0.989) 14.9%,
    hsla(210, 90%, 4%, 0.958) 28.3%,
    hsla(210, 90%, 4%, 0.91) 40.4%,
    hsla(210, 90%, 4%, 0.848) 51.1%,
    hsla(210, 90%, 4%, 0.774) 60.6%,
    hsla(210, 90%, 4%, 0.691) 68.9%,
    hsla(210, 90%, 4%, 0.602) 76.1%,
    hsla(210, 90%, 4%, 0.51) 82.2%,
    hsla(210, 90%, 4%, 0.417) 87.3%,
    hsla(210, 90%, 4%, 0.326) 91.4%,
    hsla(210, 90%, 4%, 0.24) 94.7%,
    hsla(210, 90%, 4%, 0.162) 97.1%,
    hsla(210, 90%, 4%, 0.094) 98.7%,
    hsla(210, 90%, 4%, 0.039) 99.7%,
    hsla(210, 90%, 4%, 0) 100%
  );
`;
const Spell = styled.div`
  pointer-events: auto;
  display: inline;
`;

const DisplayChampionAndSummoners = ({
  summonerKey1,
  summonerKey2,
  championKey,
  summonerName,
}) => (
  <div style={{ position: 'relative' }}>
    <TileImageWithDescription
      championKey={championKey}
      num={0}
      width={200}
    />
    <NameAndSpellWrapper>
      <Spell>
        <SummonerImageWithDescription summonerKey={summonerKey1} width={30} />{' '}
        <SummonerImageWithDescription summonerKey={summonerKey2} width={30} />
      </Spell>
      <Name>
        <a href={`https://www.leagueofgraphs.com/summoner/na/${summonerName}`} target="_blank" rel="noopener noreferrer">{summonerName}</a>
      </Name>
    </NameAndSpellWrapper>
  </div>
);

DisplayChampionAndSummoners.propTypes = {
  summonerKey1: PropTypes.string.isRequired,
  summonerKey2: PropTypes.string.isRequired,
  championKey: PropTypes.string.isRequired,
  summonerName: PropTypes.string.isRequired,
};
export default DisplayChampionAndSummoners;

