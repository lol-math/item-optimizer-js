import styled from 'styled-components';

export default styled.div`
  box-sizing: content-box;
  width: 200px;
  display: inline-block;
  border: 1px solid ${({ theme }) => theme.border.grey};
  margin-right: 10px;
`;
// style={{
//   width: 200,
//   display: 'inline-block',
//   marginRight: 10,
//   border: '1px solid #011E3B',
//   padding: '10px 0',
// }}
