import React from 'react';
import { Form } from 'antd';
import { PropTypes as t } from 'prop-types';
import Trislider from 'tri-slider';
import { Slider } from 'components/Slider';
import Checkbox from 'components/CheckBox';
import { UnlabelledFormItem, LabelledFormItem } from 'components/Form/ColumnFormItem';
import ConnectedInput from 'containers/ConnectedInput';

class QuickSettings extends React.Component {
  static propTypes = {
  }

  state = {
    expandedSlider: false,
  }
  getPath = key => `general.${key}`
  getSliderPath = key => `championSpecification.buildtype[${key}]`
  changeSlider = () => {
    this.setState({
      expandedSlider: !this.state.expandedSlider,
    });
  }
  render = () => (
    <div>
      <Form>
        <UnlabelledFormItem>
          <h2>Quick Settings </h2>
        </UnlabelledFormItem>
        <UnlabelledFormItem>
          <button onClick={()=>this.changeSlider()}>fds</button>
          <ConnectedInput path={'championSpecification.buildtype'} component={Trislider} childProps={{isThreeWay: this.state.expandedSlider}} />

        </UnlabelledFormItem>
        <LabelledFormItem
          label="Damage"
        >
          <ConnectedInput path={this.getSliderPath(0)} component={Slider} childProps={{ min: 0, max: 1, step: 0.01 }} />
        </LabelledFormItem>
        <LabelledFormItem
          label="Defense"
        >
          <ConnectedInput path={this.getSliderPath(1)} component={Slider} childProps={{ min: 0, max: 1, step: 0.01 }} />
        </LabelledFormItem>
        <LabelledFormItem
          label="Utility"
        >
          <ConnectedInput path={this.getSliderPath(2)} component={Slider} childProps={{ min: 0, max: 1, step: 0.01 }} />
        </LabelledFormItem>
        <UnlabelledFormItem>
          <ConnectedInput
            component={Checkbox}
            path={this.getPath('shouldUpgradeBoots')}
            childProps={{
              children: 'Upgrade boots by third item',
            }}
          />
        </UnlabelledFormItem>
        <UnlabelledFormItem>
          <ConnectedInput
            component={Checkbox}
            path={this.getPath('fulfillManaRequirement')}
            childProps={{
              children: 'Fulfill mana requirement',
            }}
          />

        </UnlabelledFormItem>
        <UnlabelledFormItem>
          <ConnectedInput
            component={Checkbox}
            path={this.getPath('disableSupportItems')}
            childProps={{
              children: 'Disable Support Items',
            }}
          />
        </UnlabelledFormItem>
        <UnlabelledFormItem>
          <ConnectedInput
            component={Checkbox}
            path={this.getPath('valueGrievousWound3x')}
            childProps={{
              children: 'Value Grievous Wound 3x',
            }}
          />
        </UnlabelledFormItem>
      </Form >

    </div>
  );
}

export default QuickSettings;
