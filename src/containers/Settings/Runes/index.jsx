import React from 'react';
import Icon from 'react-icon-base';
import RunePage from 'components/RunePage';
import ConnectedInput from 'containers/ConnectedInput';

const RunesSettings = () => (
  <div className="RunesSettings">
    <h2>Runes</h2>
    <ConnectedInput
      component={RunePage}
      path="runes"
    />

  </div>);
export default RunesSettings;
