import React from 'react';

import './ItemUtility.less';

import EditableItemTable from 'components/Table/EditableItemTable';

const ItemUtility = () => (
  <div className="ItemUtility">
    <h2>Items: Utility</h2>
    <EditableItemTable />
  </div>);
export default ItemUtility;
