import React from 'react';
import { Form, Icon } from 'antd';
import { UnlabelledFormItem, LabelledFormItem } from 'components/Form/ColumnFormItem';
import ConnectedInput from 'containers/ConnectedInput';
import CheckBox from 'components/CheckBox';
import { Slider } from 'components/Slider';
import { ToolTip } from 'components/ToolTip';

const getPath = subPath => `itemProficiency.${subPath}`;

const ItemProficiency = () => (
  <Form>
    <UnlabelledFormItem>
      <h2>Cooldown Reduction</h2>
      <p>How good is CDR just for the utility?</p>
    </UnlabelledFormItem>
    <LabelledFormItem
      label="CDR general"
    >

      <ConnectedInput
        component={Slider}
        path={getPath('cdrGeneral')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
                0.2: {
                    label: (
                      <ToolTip title="Champion has no dash or CC and is OK with long CD for ultimates" style={{ fontSize: 15 }}>
                        0.2 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                      </ToolTip>),
                },
                1: {
                    label: (
                      <ToolTip title="Champion has tons of CC, dashes and needs low CD for ultimates" style={{ fontSize: 15 }}>
                        1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                      </ToolTip>),
                },
            },
        }}
      />
    </LabelledFormItem>
    <UnlabelledFormItem><p>Cooldown reduction&#39;s impact on Ability DPS</p></UnlabelledFormItem>
    <LabelledFormItem
      label="Passive"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('cdrP')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
    </LabelledFormItem>
    <LabelledFormItem
      label="Q"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('cdrQ')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
    </LabelledFormItem>

    <LabelledFormItem
      label="W"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('cdrW')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
    </LabelledFormItem>

    <LabelledFormItem
      label="E"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('cdrE')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
    </LabelledFormItem>

    <LabelledFormItem
      label="R"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('cdrR')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
    </LabelledFormItem>

    <UnlabelledFormItem>
      <h2>Average stacks</h2>
      <p>Reminder: No matter how fast your champion is able to &#34;stack up&#34; to the limit, it is impossible for their average stacks to be at maximum (1.00) since your first few attacks will not be benefitting from max stacks.</p>
    </UnlabelledFormItem>

    <LabelledFormItem
      label="Tear"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('tear')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1 stack every 4 seconds" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
    </LabelledFormItem>

    <LabelledFormItem
      label="Guinsoo's"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('guinsoo')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p><Icon type="exclamation-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} /> Only auto-attacks increase the stacks</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Black Cleaver"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('cleaver')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p><Icon type="exclamation-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} /> Only physical attacks increase the stacks</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Wit's End"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('witsend')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p><Icon type="exclamation-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} /> Only auto-attacks increase the stacks</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Conqueror"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('conquerorPrestack')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p>Higher means less wait time to proc in combat</p>
    </LabelledFormItem>

    <UnlabelledFormItem>
      <h2>Items</h2>
    </UnlabelledFormItem>

    <LabelledFormItem
      label="Tiamat"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('tiamatproficiency')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is 100% AD damage every 10s" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>Active up-time</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Liandires"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('liandry')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is always applying" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>In-effect rating</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Sunfire"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('sunfire')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is always applying" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>In-effect rating</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Abbysal"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('abyssalproficiency')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is always applying" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>In-effect rating</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Essence Flare"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('essencereaver')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is rapid autos after ult" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>Cooldown Efficiency</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Rylai"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('rylais')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is perma-slow" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>In-effect rating</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Zhonya's"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('rylais')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is excellent" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>Utility multiplier</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Duskblade"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('duskblade')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is 100%" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>proc chance</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Ardent Censer"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('ardentcenser')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1.0 is excellent" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}
      />
      <p>Usage rating</p>
    </LabelledFormItem>

    <UnlabelledFormItem>
      <h2>Movement</h2>
    </UnlabelledFormItem>

    <LabelledFormItem
      label="Impairment"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('movementimpair')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p>How often do you keep your enemy their movement impaired? (This value will account for Oppressor Mastery, Liandries' double damage, Rylais' effectiveness, etc.)</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Movement utility"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('movementspeedproficiency')}
        childProps={{
            min: 0,
            max: 1.5,
            step: 0.01,
            marks: {
              1: {
                  label: (
                    <ToolTip title="1 is recommended" style={{ fontSize: 15 }}>
                     1 <Icon type="info-circle" style={{ fontSize: 20, verticalAlign: 'middle' }} />
                    </ToolTip>),
              },
          },
        }}

      />
      <p>How well does your champion benefit from movement speed?</p>
    </LabelledFormItem>

    <LabelledFormItem
      label="Tenacity"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('tenacity')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p>How well does your champion benefit from Tenacity?</p>
    </LabelledFormItem>

    <UnlabelledFormItem>
      <h2>Generic</h2>
    </UnlabelledFormItem>


    <LabelledFormItem
      label="Sudden Impact"
    >
      <ConnectedInput
        component={Slider}
        path={getPath('SuddenImpact')}
        childProps={{
            min: 0,
            max: 1,
            step: 0.01,
        }}
      />
      <p>How effectively this champion could use the sudden impact rune.</p>
    </LabelledFormItem>
    <UnlabelledFormItem>
      <ConnectedInput
        component={CheckBox}
        path={getPath('HasHardCC')}
        childProps={{
            children: 'Has Hard CC',
        }}
      />
    </UnlabelledFormItem>

    <UnlabelledFormItem>
      <ConnectedInput
        component={CheckBox}
        path={getPath('meleechampion')}
        childProps={{
            children: 'Is a Melee champion',
        }}
      />
    </UnlabelledFormItem>
  </Form>
);

export default ItemProficiency;
