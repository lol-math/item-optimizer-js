import React from 'react';
import PropTypes from 'prop-types';
import ChampionListFilterable from 'components/ChampionList/FilterableChampionList';
import { connect } from 'react-redux';
import { selectChampion } from 'Actions/championActions';

const ChampionSettings = ({ value, onChange }) => (
  <ChampionListFilterable value={value} onChange={onChange} />
);

const mapDispatchToProps = {
  onChange: selectChampion,
};
const mapStateToProps = state => ({
  value: state.settings.selectedChampion,
});
ChampionSettings.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChampionSettings);
