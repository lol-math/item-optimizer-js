import React from 'react';

import FilterableItemList from 'components/ItemList/FilterableItemList';

const ItemSettings = () => (
  <FilterableItemList />
);
export default ItemSettings;
