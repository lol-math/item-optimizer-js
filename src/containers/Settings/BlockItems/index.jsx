import React from 'react';
import { Transfer } from 'antd';
import { connect } from 'react-redux';
import { item } from 'item-optimizer-data';

import { changeBlockedItems } from 'Actions/itemActions';

const itemInfo = item.data;

function ItemBlock({ items, changeItems }) {
  const sortedItemKeys = Object.keys(items).sort((a, b) => {
    const nameA = itemInfo[a].name.toUpperCase(); // ignore upper and lowercase
    const nameB = itemInfo[b].name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    // equal
    return 0;
  });
  const dataSource = sortedItemKeys.map(itemId => ({ key: itemId }));
  const filterOption = (inputValue, option) =>
    itemInfo[option.key].name.toLowerCase().indexOf(inputValue.toLowerCase()) >
    -1;
  return (
    <div className="ItemUtility">
      <h2>Items: Block</h2>
      <Transfer
        titles={['Enabled', 'Blocked']}
        listStyle={{
          width: 300,
          height: 400,
        }}
        filterOption={filterOption}
        showSearch
        dataSource={dataSource}
        targetKeys={sortedItemKeys.filter(itemId => !items[itemId].enabled)}
        render={item => itemInfo[item.key].name}
        onChange={changeItems}
      />
    </div>
  );
}
const mapStateToProps = state => ({
  items: state.settings.items,
});

const mapDispatchToProps = {
  changeItems: changeBlockedItems,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ItemBlock);
