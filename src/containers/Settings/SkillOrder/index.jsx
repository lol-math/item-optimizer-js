import React from 'react';
import _ from 'lodash';
import SkillOrderSetter from 'components/SkillOrderSetter';
import { connect } from 'react-redux';
import { changeSkillOrder } from 'Actions/skillOrderActions';
import ConnectedInput from 'containers/ConnectedInput';
import { SpellImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import {
  SkillBlock,
  SkillBlockContainer,
  SkillBlocks,
  SkillOrderTotalBlock,
} from './SkillStyles';

const SkillsColumn = ({ value, onChange, level }) => (
  <SkillBlocks>
    <div className="level">{level}</div>
    {['Q', 'W', 'E', 'R'].map(skill => (
      <SkillBlock
        value={skill}
        onChange={() => onChange(skill)}
        checked={value === skill}
      >
        {skill}
      </SkillBlock>
    ))}
  </SkillBlocks>
);

const SkillOrderSettings = ({
  championKey,
  changeSkillOrder,
  skillOrderTotals,
}) => (
  <div className="SkillOrderSettings">
    <h2>Skill order</h2>
    <div
      style={{
        margin: '25px 0',
      }}
    >
      <SkillOrderSetter championKey={championKey} onApply={changeSkillOrder} />
    </div>
    <SkillBlockContainer>
      <SkillBlocks>
        <div className="level">level</div>
        {_.range(4).map(index => (
          <SpellImageWithDescription
            championKey={championKey}
            width={40}
            spellId={index}
          />
        ))}
      </SkillBlocks>

      {_.range(18).map(index => (
        <ConnectedInput
          path={`championAbilitySet.Skill_Order[${index}]`}
          component={SkillsColumn}
          childProps={{
            level: index + 1,
          }}
        />
      ))}
      <SkillBlocks>
        <div className="level">total</div>
        {['Q', 'W', 'E', 'R'].map(skill => (
          <SkillOrderTotalBlock>{skillOrderTotals[skill]}</SkillOrderTotalBlock>
        ))}
      </SkillBlocks>
    </SkillBlockContainer>
  </div>
);
const mapDispatchToProps = {
  changeSkillOrder,
};
const mapStateToProps = state => ({
  championKey: state.settings.selectedChampion,
  skillOrderTotals: state.settings.championAbilitySet.Skill_Order.reduce(
    (accumulator, currentValue) => {
      accumulator[currentValue] += 1; // eslint-ignore-line no-param-reassign
      return accumulator;
    },
    {
      Q: 0,
      W: 0,
      E: 0,
      R: 0,
    },
  ),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SkillOrderSettings);
