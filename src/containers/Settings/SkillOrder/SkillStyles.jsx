import React from 'react';
import styled from 'styled-components';


const ClickableDiv = ({ onChange, ...rest }) => (
  <div
    onClick={onChange}
    role="radio"
    aria-checked={rest.checked}
    tabIndex={0}
    onKeyDown={(event) => {
      // insert your preferred method for detecting the keypress
      if (event.keycode === 13) onChange(event);
    }}

    {...rest}
  />);

export const SkillBlock = styled(ClickableDiv)`
  width: 40px;
  height: 40px;
  text-align: center;
  line-height: 40px;
  /* border-width: 1px; */
  /* border-style: solid;  */
  /* border-color: ${({ checked, theme }) => (checked ? theme.border.gold : '#00000000')}; */
  background-color: ${({ checked, theme }) => (checked ? theme.background.bright : '#00000000')};
`;

export const SkillBlocks = styled.div`
/* margin:1px; */
  display: flex;
  flex-direction: column;
  .level {
    text-align: center;
  }
`;

export const SkillOrderTotalBlock = styled.div`
  width: 40px;
  height: 40px;
  text-align: center;
  line-height: 40px;
  color: ${({ theme }) => theme.font.yellow};
`;

export const SkillBlockContainer = styled.div`
  display: flex;
  overflow-x: auto;
`;
