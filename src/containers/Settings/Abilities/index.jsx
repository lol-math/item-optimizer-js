import React from 'react';
import Ability from 'components/Ability';

const Abilities = () => (
  <div>
    <h2>Abilities</h2>
    <div>P<Ability ability="P" /></div>
    <div>Q<Ability ability="Q" /></div>
    <div>W<Ability ability="W" /></div>
    <div>R<Ability ability="R" /></div>
    <div>E<Ability ability="E" /></div>
    
  </div>
);

export default Abilities;
