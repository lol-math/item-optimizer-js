import React from 'react';
import { Form, InputNumber } from 'antd';
import { UnlabelledFormItem, LabelledFormItem } from 'components/Form/ColumnFormItem';
import ConnectedInput from 'containers/ConnectedInput';

const getPath = key => `championSpecification.${key}`;
const ManaSettings = props => (
  <Form >
    <UnlabelledFormItem >
      <h2>Mana</h2>
    </UnlabelledFormItem>
    <LabelledFormItem
      label="Required Mana"
    >
      <ConnectedInput path={getPath('requiredmana')} component={InputNumber} childProps={{ step: 0.5 }} /> (Mana)
    </LabelledFormItem>
    <LabelledFormItem
      label="Tear Stacking speed"
    >
      {/* NOTE tearStackingSpeed is not provided in json files. */}
      <ConnectedInput path={getPath('tearStackingSpeed')} component={InputNumber} childProps={{ step: 0.5 }} /> (Stacks per second)

    </LabelledFormItem>
    <LabelledFormItem
      label="Mana regen to Mana Conversion"
    >
      {/* NOTE manaRegenToMana is not provided in json files. */}
      <ConnectedInput path={getPath('manaRegenToMana')} component={InputNumber} childProps={{ step: 0.5 }} /> (seconds)
    </LabelledFormItem>
  </Form>);
export default ManaSettings;
