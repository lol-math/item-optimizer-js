import React from 'react';
import { connect } from 'react-redux';

const ExportSettings = ({ settings }) => (JSON.stringify(settings, null, 2));

const mapStateToProps = state => ({
  settings: state.settings,
});

export default connect(mapStateToProps)(ExportSettings);
