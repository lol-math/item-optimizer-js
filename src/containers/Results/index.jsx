import React from 'react';
// import PropTypes from 'prop-types';
import Button from 'components/Button';
import ChampionStats from 'components/ChampionStats';
import { ClickableItem } from 'components/Item';
import { connect } from 'react-redux';
import AlgoRequest from 'item-optimizer-algo';
import {
  UpgradePointers,
  UpgradePointer,
  UltimateBuildToggle,
  OverView,
} from './styles';
import BuildCard from 'components/BuildCard';
import _ from 'lodash';
import { ItemImageWithDescription as Item } from 'components/DdragonImages/DdImageWithDescription';
import CheckBox from 'components/CheckBox';

class BuildResult extends React.PureComponent {
  state = {
    build: {
      items: [3001, 2065, 2051, 3340, 2015, 2015],
      cost: 12345,
      statIncreases: {
        offense: 2.36,
        defense: 2.01,
        utility: 0.2,
      },
    },
    normalBuild: [3001, 2065, 2051, 3340, 2015, 2015],
    ultimateBuild: [1401, 2065, 2051, 1402, 1419, 3340],
    showUltimate: false,
    stats: {
      hp: 888,
      mp: 888,
      attackDamage: 888,
      armor: 888,
      attackSpeed: 888,
      criticalStrikeChance: 888,
      abilityPower: 888,
      magicResist: 888,
      coolDownReduction: 888,
      movementSpeed: 888,
      healthRegeneration: 888,
      armorPenetration: 888,
      lifeSteal: 888,
      range: 888,
      manaRegeneration: 888,
      magicPenetration: 888,
      spellVamp: 888,
      tenacity: 888,
      damageAmp: 888,
      physicalDamageAmp: 888,
      magicalDamageAmp: 888,
      resistanceReduction: 888,
    },
  };
  getLatestBuildOrder = () => {
    const algoRequest = new AlgoRequest(this.props.settings);
    const { currentBuild } = algoRequest;
    this.setState({
      build: {
        ...this.state.build,
        items: currentBuild,
      },
    });
  };
  render() {
    const build = {
      items: [3001, 2065, 2051, 3340, 2015, 2015],
      cost: 12345,
      statIncreases: {
        offense: 2.36,
        defense: 2.01,
        utility: 0.2,
      },
    };
    const includeItems = [3001, 2065];
    const totalCalculatedPaths = 30000;
    const calculationTime = 30.3;
    return (
      <div>
        <h2>Calculated Build order</h2>
        <Button onClick={this.getLatestBuildOrder}> Recalculate </Button>
        <OverView>
          <div className="numbers">
            <span>
              Builds evaluated: <number>{totalCalculatedPaths}</number>
            </span>
            <span>
              Evaluation time: <number>{calculationTime}</number>s
            </span>
          </div>
          <div className="includeItems">
            <div>Include items:</div>
            {includeItems.map(itemId => (
              <Item itemId={itemId} width={28} />
            ))}
          </div>
          {/* <div className="Buttons">
            Filter Button
            ?
          </div> */}
          <div className="ultimatebuildtoggle">
            <CheckBox>Ignore item cost (Ultimate build)</CheckBox>
          </div>
        </OverView>
        <BuildCard {...this.state.build} />
        {/* {_.range(5).map(index => (
          <BuildCard
            {...build}
            title={index === 0 ? 'Best Build' : null}
            highlight={index === 0}
          />
        ))} */}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  settings: state.settings,
});
BuildResult.propTypes = {};
export default connect(mapStateToProps)(BuildResult);
