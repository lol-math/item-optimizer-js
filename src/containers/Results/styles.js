import styled from 'styled-components';
import ArrowDown from 'Images/arrows down.svg';

export const UpgradePointers = styled.div`
  div {
    width: 48px;
    height: 48px;
  }
  display: flex;
`;

export const UpgradePointer = styled.div`
    background-image: url("${ArrowDown}");
    background-size: cover;
`;

export const UltimateBuildToggle = styled.button`
  border: 1px solid #c8aa6e;
  text-align: center;
  color: #c8aa6e;
  background: #ffffff00;
  width: 100%;
  cursor: pointer;
  margin: 10px 0;
  outline: 0;
  &:hover {
    color: #e8dfcc;
  }
`;

export const OverView = styled.div`
  background: ${({ theme }) => theme.background.secondary};
  margin-bottom: 5px;
  padding: 10px;
  .numbers {
    display: flex;
    > * {
      flex: 1;
    }
    number {
      font-weight: 600;
    }
  }
  .includeItems {
    & > * {
      margin-right: 5px;
    }
  }
`;
