import React from 'react';
import { Card, Table } from 'antd';
import { ItemImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import _ from 'lodash';
import styled from 'styled-components';
import Jungle from 'Images/jungle.png';
import Support from 'Images/support.png';

import { item } from 'item-optimizer-data';

import statDictionary from 'Json/StatDictionary.json';
import modDictionary from 'Json/ModifierDictionary.json';
import statPercentages from 'Json/StatPercentages.json';

import iconBlocked from 'Images/icon-ban-red.png';
import iconNotBlocked from 'Images/icon-ban.png';

import './ItemEditor.css';

const items = item.data;
const YellowText = styled.span`
  color: ${({ theme }) => theme.font.yellow};
`;

/**
 * Character used for splitting the string of a stat which is located within the supporting tree or jungling tree.
 */
const splitCharacter = '%';

const getOriginalStatKey = (statKey) => {
  const splitTextArray = _.split(statKey, splitCharacter);
  if (splitTextArray.length > 1) {
    return splitTextArray[1];
  }
  return statKey;
};
/**
 * The plus sign before (value*100).toFixed(1) makes sure that the rounding is always appropriate.
 */
const percentify = (key, value) =>
  (statPercentages.includes(getOriginalStatKey(key))
    ? `${+(value * 100).toFixed(1)}%`
    : value);

const renderAmount = (text, record) => {
  let amount = text;
  if (_.isArray(amount)) { return text.map(value => <div>{percentify(record.key, value)}</div>); }
  if (amount.value !== undefined) amount = amount.value;
  if (_.isString(amount)) {
    return amount;
  }
  return percentify(record.key, amount);
};

const renderStatNameText = text =>
  (statDictionary[text] !== undefined ? statDictionary[text] : text);

const statsColumns = [
  {
    title: 'Statistic',
    dataIndex: 'name',
    key: 'name',
    render: (text) => {
      const splitTextArray = _.split(text, splitCharacter);
      if (splitTextArray.length > 1) {
        switch (splitTextArray[0]) {
          case 'Supporting':
            return [
              <img
                src={Support}
                height={14}
                alt="Support"
                style={{ marginRight: 5 }}
              />,
              renderStatNameText(splitTextArray[1]),
            ];
          case 'Jungling':
            return [
              <img
                src={Jungle}
                height={14}
                alt="Jungle"
                style={{ marginRight: 5 }}
              />,
              renderStatNameText(splitTextArray[1]),
            ];
          default:
            return renderStatNameText(splitTextArray[1]);
        }
      }
      return renderStatNameText(text);
    },
  },
  {
    title: 'Value',
    dataIndex: 'amount',
    className: 'column-amount',
    key: 'amount',
    render: renderAmount,
  },
];
const modsColumns = [
  {
    title: 'Modifier',
    dataIndex: 'name',
    key: 'name',
    render: text => (modDictionary[text] ? modDictionary[text] : text),
  },
  {
    title: 'Value',
    dataIndex: 'amount',
    className: 'column-amount',
    key: 'amount',
    render: (text, record) => (
      <YellowText>{renderAmount(text, record)}</YellowText>
    ),
  },
];

const getDataSource = (_stats) => {
  let stats = _stats;
  ['Supporting', 'Jungling'].forEach((role) => {
    if (stats[role]) {
      _.forIn(stats[role], (value, key) => {
        stats[`${role}${splitCharacter}${key}`] = value;
      });
      stats = _.omit(stats, role);
    }
  });
  return Object.keys(stats).map(stat => ({
    key: stat,
    name: stat,
    amount: stats[stat],
    mod: stats[stat].mod,
  }));
};

const renderUniqueTitle = unique =>
  (unique.name ? (
    <h3>
      Named Unique: <span style={{ fontStyle: 'italic' }}>{unique.name}</span>
    </h3>
  ) : (
    <h3>Unique</h3>
  ));
const renderExpandedRow = record => (
  <Table
    {...{
      bordered: false,
      pagination: false,
      size: 'small',
      showHeader: false,
    }}
    title={() => <span>Modifiers</span>}
    dataSource={Object.keys(record.mod).map(mod => ({
      key: mod,
      name: mod,
      amount: record.mod[mod],
    }))}
    columns={modsColumns}
  />
);
const rowClassName = record => (record.mod ? '' : 'hideExpandIcon');

const tableSettings = {
  bordered: false,
  pagination: false,
  size: 'small',
  showHeader: false,
  expandedRowRender: renderExpandedRow,
  rowClassName,
};
const renderStatsTable = (stats, additionalTableSettings) => {
  if (!stats) return null;
  const dataSource = getDataSource(stats);
  return (
    <Table
      {...tableSettings}
      {...additionalTableSettings}
      dataSource={dataSource}
      columns={statsColumns}
    />
  );
};

const ItemEditor = ({ itemId, value }) => (
  <Card
    title={
      <div>
        <ItemImageWithDescription itemId={itemId} width={30} />
        <span
          style={{
            marginLeft: 20,
          }}
        >
          {items[itemId].name}
        </span>
      </div>
    }
    bordered
    type="inner"
    style={{ maxWidth: 500 }}
    extra={
      <div>
        <span style={{ marginRight: 20 }}>
          {value.allowed ? 'Not Blocked' : 'Blocked'}
        </span>
        <img
          src={value.allowed ? iconNotBlocked : iconBlocked}
          alt="Item block status"
          height="30"
        />
      </div>
    }
  >
    {renderStatsTable(value.stats)}
    {value.unique &&
      Object.keys(value.unique).map((unique) => {
        const currentUnique = value.unique[unique];
        return renderStatsTable(value.unique[unique].stats, {
          title: () => renderUniqueTitle(currentUnique),
          style: { marginTop: 5 },
        });
      })}
    {value.special && <div>Special: {value.special}</div>}
  </Card>
);

export default ItemEditor;
