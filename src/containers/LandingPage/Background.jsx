import styled from 'styled-components';

export default styled.div`
  flex: 1;
  background: radial-gradient(circle, hsl(211, 92%, 15%), hsl(211, 92%, 10%));
  overflow-y: auto;
`;
