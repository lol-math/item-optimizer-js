import React from 'react';
import { Row, Col, AutoComplete, Icon, Button, Select } from 'antd';

import { championFull } from 'item-optimizer-data';
import regionalEndpoints from 'Json/regionalEndpoints.json';
import Logo from 'Images/LOGO.svg';
import { ChampionImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';

import './LandingPage.less';
import LandingPageBackground from 'containers/LandingPage/Background';

const champions = championFull.data;
const championKeys = Object.keys(champions);

function renderOption(championKey) {
  return (
    <AutoComplete.Option
      key={champions[championKey].key}
      text={champions[championKey].name}
    >
      <ChampionImageWithDescription championKey={championKey} sizeRatio={0.3} />
      {champions[championKey].name}
    </AutoComplete.Option>
  );
}
export default class LandingPage extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '',
    };
  }

  render() {
    return (
      <LandingPageBackground>
        <Row type="flex" justify="center">
          <Col span={12}>
            <div
              style={{
                height: '40vh',
                minHeight: 190,
              }}
            >
              <img
                src={Logo}
                alt="Logo"
                width={250}
                height={150}
                style={{
                  position: 'absolute',
                  bottom: 20,
                  left: '50%',
                  transform: 'translate(-50%, 0)',
                }}
              />
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="center">
          <Col span={12} style={{ display: 'flex' }}>
            <AutoComplete
              className="global-search"
              size="large"
              dataSource={championKeys
                .filter(championId =>
                  champions[championId].name
                    .toLowerCase()
                    .includes(this.state.search.toLowerCase()))
                .slice(0, 9)
                .map(renderOption)}
              // onSelect={onSelect}
              onSearch={val => this.setState({ search: val })}
              placeholder="Search a Champion or Summoner"
              optionLabelProp="text"
              style={{ flex: 1 }}
              autoFocus
            />
            <Select
              defaultValue="NA1"
              style={{
                borderRadius: 0,
                width: 100,
              }}
              className="region-select"
            >
              {regionalEndpoints.map(endpoint => (
                <Select.Option
                  value={endpoint.platform}
                  key={endpoint.platform}
                >
                  {endpoint.region}
                </Select.Option>
              ))}
            </Select>
            <Button
              className="search-btn"
              size="large"
              type="primary"
              style={{ borderBottomLeftRadius: 0, borderTopLeftRadius: 0 }}
            >
              <Icon type="search" />
            </Button>
          </Col>
        </Row>
      </LandingPageBackground>
    );
  }
}
