export const league = {
  name: 'league',
  background: {
    main: 'hsl(210, 90%, 4%)',
    secondary: 'hsl(210, 90%, 8%)',
    bright: 'hsla(210, 90%, 30%, 1)',
    bestBuild: 'hsl(30, 100%, 50%)',
  },
  font: {
    main: 'hsl(210, 90%, 27%)',
    bright: 'hsl(211, 92%, 85%)',
    yellow: 'hsl(48, 92%, 59%)',
    gold: 'hsla(48, 100%, 53%, 0.74);',
  },
  border: {
    grey: '#3C3C41',
    gold: '#372E18',
    brightGold: '#785A28',
    brown: 'hsl(42, 57%, 18%)',
  },
  menu: {
    bgActive: '#ffffff09',
    font: '#CDBE91',
    fontActive: 'hsl(37, 41%, 86%)',
  },
  stats: {
    offense: 'hsl(2, 100%, 69%)',
    defense: 'hsl(120, 52%, 49%)',
    utility: 'hsl(285, 79%, 70%)',
  },
  scrollBar: {
    backgroundHover: 'hsl(40, 45%, 61%)',
    background: 'hsl(38, 50%, 31%)',
  },
};
export const bright = {};
export default league;
