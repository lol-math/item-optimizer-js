import React from 'react';
import styled from 'styled-components';

const EditableTextStyle = styled.input.attrs({
  type: 'text',
})`
  outline: none;
  &:hover, &:focus {
    background: rgba(255, 255, 255, 0.034);
    border: 1px solid #3C3C41;
    border-radius: 2px;
  }
  &:focus {
    border-color: hsla(211, 87%, 30%, 1);
  }
  font: inherit;
  background: unset;
  color: inherit;
  text-align: inherit;
  padding: 1em .2em;
  height: 1.9em;
  box-sizing: border-box;
  border: 1px solid rgba(255, 255, 255, 0);
  width: 100%;
  display: inline;
`;
const EditableText = ({ onChange, value, rest }) => <EditableTextStyle onChange={e => onChange(e.target.value)} value={value} {...rest} />;
EditableText.displayName = 'EditableText';
export default EditableText;
