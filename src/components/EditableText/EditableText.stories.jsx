import React from 'react';
import { storiesOf } from '@storybook/react';
import EditableText from 'components/EditableText';

storiesOf('Editable Text', module)
  .add('Basic', () => (
    <EditableText
      value={0}
    />
  ));

