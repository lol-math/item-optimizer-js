import React from 'react';
import PropTypes from 'prop-types';

import RadioButtonInput from 'components/RadioButton/RadioButtonInput';
import RadioButtonWrapper from 'components/RadioButton/RadioButtonWrapper';
import RadioButtonLabel from 'components/RadioButton/RadioButtonLabel';

const RadioButton = ({
  name, value, onChange, checked, children,
}) => (
  <RadioButtonWrapper>
    <RadioButtonLabel>
      <RadioButtonInput
        name={name}
        checked={checked}
        onChange={onChange}
        value={value}
      />
      {children}
    </RadioButtonLabel>
  </RadioButtonWrapper>
);
RadioButton.defaultProps = {
  children: '',
};
RadioButton.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.node,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
};
export default RadioButton;

