import React from 'react';
import { storiesOf } from '@storybook/react';
import RadioButton from './index';

storiesOf('Radio Button', module)
  .add('Basic', () => (
    <RadioButton> Radio Button Text </RadioButton>
  ));

