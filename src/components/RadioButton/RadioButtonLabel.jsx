import styled from 'styled-components';

export default styled.label`
  cursor: pointer;
  padding: .1em 1em 1em 0;
  user-select: none;
`;

