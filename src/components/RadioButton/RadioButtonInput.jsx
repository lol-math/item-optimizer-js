import styled from 'styled-components';
import ButtonImage from 'Images/btn_icon.png';

export default styled.input.attrs({
  type: 'radio',
})`
  appearance: none;
  margin-right: 10px;
  &:before {
      position: relative;
      display: inline-block;
      vertical-align: middle;
      content: '';
      height: 20px;
      width: 20px;
      background: url(${ButtonImage}) no-repeat 0 0;
      background-size: 100%;
      cursor: pointer;
  }
  &:focus {
      outline: none;
  }
  &:before {
      background-position-y: 0%;
  }
  &:hover:before {
      background-position-y: ${(100 / 7) * 1}%;
  }
  &:active:before {
      background-position-y: ${(100 / 7) * 2}%;
  }
  &:checked {
      &:before {
          background-position-y: ${(100 / 7) * 4}%;
      }
      &:hover:before {
          background-position-y: ${(100 / 7) * 5}%;
      }
      &:active:before {
          background-position-y: ${(100 / 7) * 6}%;
      }
  }
`;

