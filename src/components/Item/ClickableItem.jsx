import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Modal, Menu, Dropdown } from 'antd';
import { ItemImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import { PopOver } from 'components/ToolTip';
import ItemPopOver from './ItemPopOver';
import ItemEditor from 'containers/ItemEditor';
import {
  Lock,
  Ban,
  Edit,
  CustomContextMenu,
  LockItemIcon,
  BanItemIcon,
} from './StyledItem';
import ConnectedValue from 'containers/ConnectedValue';
import { changeForcedItem } from 'Actions/itemActions';
import { ContextMenu, MenuItem, ContextMenuTrigger } from 'react-contextmenu';

class ClickableItem extends React.Component {
  static propTypes = {
    itemId: PropTypes.number.isRequired,
    width: PropTypes.number,
    isForced: PropTypes.bool.isRequired,
    isBanned: PropTypes.bool.isRequired,
    itemIndex: PropTypes.number,
  };
  static defaultProps = {
    width: 48,
    itemIndex: null,
  };
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      visible: false,
    });
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  render = () => {
    const {
      itemId,
      width,
      itemIndex,
      forcedItem,
      isBlocked,
      ...otherProps
    } = this.props;
    const isForced = forcedItem === itemId;
    return [
      <ContextMenuTrigger id={`index${itemIndex}id${itemId}`}>
        <div style={{ position: 'relative' }}>
          <LockItemIcon isForced={isForced} />
          <BanItemIcon isBlocked={isBlocked} />
          <ItemImageWithDescription
            itemId={itemId}
            width={width}
            {...otherProps}
          />
        </div>
      </ContextMenuTrigger>,

      <CustomContextMenu id={`index${itemIndex}id${itemId}`}>
        <div className="shadow">
          <MenuItem key={{ action: 'block' }} onClick={this.onContextMenuClick}>
            <Ban />
            {isBlocked ? 'Block' : 'Unblock'}
          </MenuItem>
          {itemIndex !== null && (
            <MenuItem
              onClick={() => {
                this.props.forceItem(this.props.itemIndex, this.props.itemId);
              }}
            >
              <Lock />
              {isForced ? 'Unforce Here' : 'Force here'}
            </MenuItem>
          )}
          <MenuItem
            onClick={() => {
              this.props.onForceOther();
            }}
          >
            <Lock />
            Force Other
          </MenuItem>
          <MenuItem
            onClick={() => {
              this.showModal();
            }}
          >
            <Edit />
            Edit
          </MenuItem>
        </div>
      </CustomContextMenu>,
      <Modal
        title="Edit Item"
        visible={this.state.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        footer={null}
        key="2"
      >
        <ConnectedValue path={`items[${itemId}]`}>
          {item => <ItemEditor itemId={itemId} value={item} />}
        </ConnectedValue>
      </Modal>,
    ];
  };
}

const mapDispatchToProps = {
  forceItem: changeForcedItem,
};
const mapStateToProps = (state, ownProps) => ({
  forcedItem:
    ownProps.itemIndex !== null &&
    state.settings.forcedItems[ownProps.itemIndex],
  isBlocked: state.settings.items[ownProps.itemId].enabled,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ClickableItem);
