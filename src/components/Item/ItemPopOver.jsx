import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import styled from 'styled-components';
import Locked from 'Images/skin-carousel-locked.png';
import LockedHover from 'Images/skin-carousel-locked-hover.png';
import BanRed from 'Images/icon-ban-red.png';
import Ban from 'Images/icon-ban.png';

const ItemMenu = styled.div`
width: 100px;
    height: 100px;
    z-index: 1000;
    display: grid;
    grid-template-columns: 50% 50%;
    grid-template-rows: 50% 50%;
    .Force {
        background: url(${Locked}) no-repeat center;
        background-size: 100%;
    }
    .UnForce {
        background: url(${LockedHover}) no-repeat center;
        background-size: 100%;
    }
    .Ban {
        background: url(${BanRed}) no-repeat center;
        background-size: 60%;
    }
    .UnBan {
        background: url(${Ban}) no-repeat center;
        background-size: 60%;
    }
    .Utility {
        width: 100%;
        height: 100%;
        border: 0;
        .ant-input-number-input-wrap,
        input {
            height: 100%;
        }
        outline: 0;
    }
    button {
        cursor: pointer;
        background: 0;
        border: 0;
        outline: 0;
        &:hover {
          background-color: hsl(210, 90%, 15%);
        }
    }

`;

const ItemPopOver = ({
  itemId,
  isForced,
  isBanned,
  onForceItem,
  onUnbanItem,
  onUnForceItem,
  slot,
  showModal,
  onBanItem,
}) =>
  (

    <ItemMenu>
      {(isForced && <button
        className="UnForce"
        onClick={() => onUnForceItem(itemId, slot)}
      />)
      || <button
        className="Force"
        onClick={() => onForceItem(itemId, slot)}
      />}
      {(isBanned && <button
        className="UnBan"
        onClick={() => onUnbanItem(itemId, slot)}
      />)
      || <button
        className="Ban"
        onClick={() => onBanItem(itemId, slot)}
      />}
      {/* <InputNumber className="Utility" /> */}
      <button onClick={() => showModal()}><Icon type="edit" /></button>
    </ItemMenu>

  );
ItemPopOver.defaultProps = {
  slot: undefined,
};
ItemPopOver.propTypes = {
  isForced: PropTypes.bool.isRequired,
  isBanned: PropTypes.bool.isRequired,
  onUnForceItem: PropTypes.func.isRequired,
  onForceItem: PropTypes.func.isRequired,
  onUnbanItem: PropTypes.func.isRequired,
  onBanItem: PropTypes.func.isRequired,
  itemId: PropTypes.number.isRequired,
  showModal: PropTypes.func.isRequired,
  slot: PropTypes.number,
};
export default ItemPopOver;
