import React from 'react';
import { storiesOf } from '@storybook/react';

import { ClickableItem } from './index';

storiesOf('Item', module)
  .add('Basic', () => (
    <ClickableItem
      itemId={3001}
      isForced
      isBanned
    />
  ));

