import React from 'react';
import { ItemImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import { item } from 'item-optimizer-data';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ClickableItem from './ClickableItem';

const items = item.data;

const NamedItemWrapper = styled.div`
  margin: 5px;
  width: ${({ width }) => width}px;
  span {
    display: block;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    font-size: 10px;
  }
`;

const NamedItem = ({
  clickable, itemId, width, ...props
}) => {
  const item = items[itemId];
  return (
    <NamedItemWrapper width={width}>
      {!clickable ? (
        <ItemImageWithDescription itemId={itemId} {...props} width={width} />
      ) : (
        <ClickableItem itemId={itemId} {...props} width={width} />
      )}
      <span>{item.name}</span>
    </NamedItemWrapper>
  );
};
NamedItem.propTypes = {
  itemId: PropTypes.string.isRequired,
  clickable: PropTypes.bool,
  width: PropTypes.number,
};
NamedItem.defaultProps = {
  clickable: false,
  width: 56,
};
export default NamedItem;
