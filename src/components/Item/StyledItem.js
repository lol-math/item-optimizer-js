import styled from 'styled-components';
import LockImage from 'Images/lock.svg';
import BanRed from 'Images/icon-ban-red.png';
import Gear from 'Images/gear.svg';
import Stylus from 'Images/icon-edit.png';
import { Modal, Menu, Dropdown } from 'antd';
import { ContextMenu } from 'react-contextmenu';

export const ItemContextMenu = styled(Menu)`
  box-shadow: 0 0px 15px rgb(0, 0, 0) !important;
  @supports (backdrop-filter: blur(4px)) {
    box-shadow: none !important;
    background-color: #00000000 !important;
    backdrop-filter: blur(4px);
    border: 1px solid #000;
  }
  border-radius: 0px !important;
`;

const MenuImage = styled.div`
  height: 20px;
  width: 20px;
  display: inline-block;
  background-size: contain;
  vertical-align: text-bottom;
  background-repeat: no-repeat;
  margin-right: 5px;
  background-position: center center;
`;

export const Lock = styled(MenuImage)`
  background-image: url(${LockImage});
`;
export const Ban = styled(MenuImage)`
  background-image: url(${BanRed});
`;
export const Edit = styled(MenuImage)`
  background-image: url(${Stylus});
  background-size: 17px;
`;

const ItemIcon = styled.div`
  position: absolute;
  height: 20px;
  width: 20px;

  background-size: cover;
  z-index: 100;
  pointer-events: none;
`;

export const LockItemIcon = styled(ItemIcon)`
  top: -10px;
  right: -10px;
  background-image: ${({ isForced }) =>
    (isForced ? `url(${LockImage})` : 'none')};
`;
export const BanItemIcon = styled(ItemIcon)`
  top: -10px;
  right: 10px;
  background-image: ${({ isBlocked }) =>
    (isBlocked ? 'none' : `url(${BanRed})`)};
`;

export default styled.div`
  user-select: none;
  position: relative;
  label {
    font-size: 9px;
  }
  .ban {
    top: -10px;
    right: 10px;
    position: absolute;
    height: 20px;
    width: 20px;
    background-image: url(${BanRed});
    background-size: cover;
    z-index: 100;
    pointer-events: none;
  }
  &:hover {
    &:after {
      opacity: 1;
      content: '';
      transition: opacity 0.2s ease-in-out, background-color 0.2s ease-in-out;
      background-color: rgba(0, 0, 0, 0.74);
      background-image: url(${Gear});
      background-size: 50%;
      background-position: center;
      background-repeat: no-repeat;
      user-select: none;
      pointer-events: none;
      position: absolute;
      bottom: 0;
      top: 0;
      left: 0;
      right: 0;
      cursor: pointer;
    }
  }
  &:after {
    content: '';
    transition: opacity 0.2s ease-in-out, background-color 0.2s ease-in-out;
    opacity: 0.2;
  }
`;

export const CustomContextMenu = styled(ContextMenu)`
  border-radius: 0.25rem;
  color: hsla(210, 90%, 70%, 1);
  outline: none;
  display: none;
  text-align: left;

  box-shadow: 0 0px 15px rgb(0, 0, 0);

  .shadow {
    background: hsla(210, 90%, 4%, 1);
    @supports (backdrop-filter: blur(4px)) {
      backdrop-filter: blur(4px);
      background: hsla(210, 90%, 4%, 0.51);
    }
  }

  &.react-contextmenu--visible {
  display: block;

    pointer-events: auto;
    z-index: 9999;
  }

  .react-contextmenu-item {
    background: 0 0;
    border: 0;
    user-select: none;
    font-weight: 400;
    line-height: 1.5;
    text-align: inherit;
    white-space: nowrap;
    padding: 3px 10px 3px 5px;
    &:hover {
      background: hsla(0, 0%, 100%, 0.14);
    }
    &.react-contextmenu-item--active,
    &.react-contextmenu-item--selected {
      color: #fff;
      background-color: #20a0ff;
      border-color: #20a0ff;
      text-decoration: none;
    }
    &.react-contextmenu-item--disabled,
    &.react-contextmenu-item--disabled:hover {
      background-color: transparent;
      border-color: rgba(0, 0, 0, 0.15);
      color: #878a8c;
    }
    &.react-contextmenu-submenu {
      padding: 0;
      & > .react-contextmenu-item {
        &:after {
          content: '▶';
          display: inline-block;
          position: absolute;
          right: 7px;
        }
      }
    }
  }

  .react-contextmenu-item--divider {
    border-bottom: 1px solid rgba(0, 0, 0, 0.15);
    cursor: inherit;
    margin-bottom: 3px;
    padding: 2px 0;
    &:hover {
      background-color: transparent;
      border-color: rgba(0, 0, 0, 0.15);
    }
  }

  .example-multiple-targets::after {
    content: attr(data-count);
    display: block;
  }
`;
