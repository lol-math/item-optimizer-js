import React from 'react';
import { SortableElement } from 'react-sortable-hoc';
import styled from 'styled-components';
import { SpellImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import SkillDict from 'Json/SkillDictionary.json';

const SkillOrderSortableElement = styled.div`
  margin-right: 32px;
  cursor: move;
  user-select: none;
  padding-top: 4px;
  background: #010A13;
  &.active{
    box-shadow: 0 0 10px #000;
  }
`;
const SkillTitle = styled.h3`
  text-align: center;
  margin-bottom: 0;
  border-top: 4px ${({ theme }) => theme.border.gold} dotted;
  padding-top: 3px;
  margin-top: 0;
`;


export default SortableElement(({ value, championKey }) =>
  (
    <SkillOrderSortableElement>
      <SkillTitle>{value}</SkillTitle>
      <SpellImageWithDescription
        championKey={championKey}
        width={32}
        spellId={SkillDict[value]}
      />
    </SkillOrderSortableElement>
  ));
