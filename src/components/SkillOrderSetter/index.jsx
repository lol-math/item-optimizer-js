import React from 'react';
import PropTypes from 'prop-types';
import { arrayMove } from 'react-sortable-hoc';
import SortableList from 'components/SkillOrderSetter/SortableSkillList';
import Button from 'components/Button';

class SkillOrderSetter extends React.PureComponent {
  static displayName = 'SkillOrderSetter'
  static propTypes = {
    /** The champion to render the skill images for. */
    championKey: PropTypes.string.isRequired,
  }

  state = {
    skills: [
      'Q',
      'W',
      'E',
    ],
  };


  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState({
      skills: arrayMove(this.state.skills, oldIndex, newIndex),
    });
  };

  render() {
    return [
      <SortableList
        items={this.state.skills}
        onSortEnd={this.onSortEnd}
        axis="x"
        championKey={this.props.championKey}
        lockAxis="x"
        helperClass="active"
      />,
      <Button style={{ verticalAlign: 'bottom', margin: '10px 0' }} onClick={() => this.props.onApply(this.state.skills)}>
        Apply
      </Button>,
    ];
  }
}


SkillOrderSetter.propTypes = {
  onApply: PropTypes.func.isRequired,
};

export default SkillOrderSetter;
