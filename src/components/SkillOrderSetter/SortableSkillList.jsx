import React from 'react';
import { SortableContainer } from 'react-sortable-hoc';
import styled from 'styled-components';
import arrowsRight from 'Images/arrows right.svg';
import SortableItem from 'components/SkillOrderSetter/SortableSkillListItem';

const SortableSkillList = styled.div`
  background-image: url('${arrowsRight}'), url('${arrowsRight}'), url('${arrowsRight}');
  background-repeat: no-repeat;
  background-size: 20px;
  background-position: 38px 30px, 102px 30px;
  display: inline-flex;
`;

export default SortableContainer(({ items, championKey }) => (
  <SortableSkillList>
    {items.map((value, index) => (
      <SortableItem
        key={`item-${value}`}
        index={index}
        value={value}
        championKey={championKey}
      />
    ))}
  </SortableSkillList>
));
