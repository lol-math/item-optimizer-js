import styled from 'styled-components';

export default styled.div`
  margin-bottom: 5px;
  background: ${({ theme }) => theme.background.main};
  /* background: #010A13cc; */
  padding: 5px 0;
  /* border-bottom: 2px solid #3c3c41; */
  /* box-shadow: 0 0 20px 4px #000000; */
  filter: drop-shadow(0 0 4px #000000);
  position: sticky;
  z-index: 100;
  top: 0;
  /* -webkit-backdrop-filter:  blur(4px); */
  /* backdrop-filter: blur(4px); */
`;

/**
 * Experimenting with backdrop filter on Filter Bar.
 */
