import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DisabledWrapper = styled.div`
  position: relative;
`;

const DisabledItemWrapper = styled.div`
  filter: ${({ disabled }) => (disabled ? 'blur(4px)' : 'none')};
  pointer-events: ${({ disabled }) => (disabled ? 'none' : 'auto')};
  user-select: ${({ disabled }) => (disabled ? 'none' : 'auto')};
`;

const DisabledMessage = styled.div`
  position: absolute;
  top:0;
  bottom:0;
  left:0;
  right:0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #ffffff06;
  font-size: 16px;
  padding: 25px;
`;

const Disabled = ({ children, message, disabled }) => (
  <DisabledWrapper>
    <DisabledItemWrapper disabled={disabled}>
      {children}
    </DisabledItemWrapper>
    {disabled && <DisabledMessage>{message}</DisabledMessage>}
  </DisabledWrapper>

);

Disabled.propTypes = {
  children: PropTypes.node.isRequired,
  message: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
};
export default Disabled;

