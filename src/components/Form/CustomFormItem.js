import Styled from 'styled-components';
import { Form } from 'antd';

const FormItem = Form.Item;
const CustomFormItem = Styled(FormItem)`
  margin-bottom: 0;
`;

export default CustomFormItem;
