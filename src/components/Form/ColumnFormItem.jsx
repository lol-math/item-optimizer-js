import React from 'react';
import { Form } from 'antd';

const FormItem = Form.Item;

const LabelledFormItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const UnlabelledFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 4,
    },
  },
};

export const UnlabelledFormItem = props => (
  <FormItem
    {...props}
    {...UnlabelledFormItemLayout}
  />
);
export const LabelledFormItem = props => (
  <FormItem
    {...props}
    {...LabelledFormItemLayout}
  />
);
