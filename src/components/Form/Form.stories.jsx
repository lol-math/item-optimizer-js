import React from 'react';
import { Form } from 'antd';
import 'antd/lib/form/style/index.css';
import 'antd/lib/grid/style/index.css';
import { storiesOf } from '@storybook/react';
import { UnlabelledFormItem, LabelledFormItem } from 'components/Form/ColumnFormItem';

storiesOf('Form', module)
  .add('Unlabelled', () => (
    <Form>
      <UnlabelledFormItem>
        <input type="checkbox" />
      </UnlabelledFormItem>
    </Form>
  ))
  .add('Labelled', () => (
    <Form>
      <LabelledFormItem
        label={<span style={{color: '#fff'}}>label</span>}
      >
        <input type="checkbox" />
      </LabelledFormItem>
    </Form>
  ));

