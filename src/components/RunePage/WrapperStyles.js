import Styled from 'styled-components';

export const RunePageWrapper = Styled.div`
  width: 270px;
  margin-right: 30px;
  /* display: inline-block; */
  vertical-align: top;
`;
export const RuneTreeWrapper = Styled.div`

`;
export const RuneRowWrapper = Styled.div`
  display: flex;
  justify-content: space-around;
  margin: 20px 0;
`;
export const RuneWrapper = Styled.div`

`;
export const RuneTreeWrapperWrapper = Styled.div`
  display: grid;
  grid-template-columns: auto auto;
`;

