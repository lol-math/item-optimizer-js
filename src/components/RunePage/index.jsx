import React, { PureComponent } from 'react';
import { Tabs } from 'antd';
import PropTypes from 'prop-types';
import { runesReforged as runes } from 'item-optimizer-data';
import { RuneTreeImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import SelectableRune from 'components/RunePage/SelectableRune';
import {
  RunePageWrapper,
  RuneTreeWrapper,
  RuneRowWrapper,
  RuneTreeWrapperWrapper,
} from 'components/RunePage/WrapperStyles';

const { TabPane } = Tabs;

const SearchRune = (runeId) => {
  const path = [];
  runes.find((tree, treeIndex) => {
    if (
      tree.slots.find((slot, slotIndex) => {
        if (
          slot.runes.find((rune, runeIndex) => {
            if (rune.id === runeId) {
              path.push(runeIndex);
              return true;
            }
            return false;
          })
        ) {
          path.push(slotIndex);
          return true;
        }
        return false;
      })
    ) {
      path.push(treeIndex);
      return true;
    }
    return false;
  });
  return path.reverse();
};

class RunePage extends PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    value: PropTypes.array,
    onChange: PropTypes.func.isRequired,
  };
  static defaultProps = {
    value: [],
  };

  onChangeSubTree = (val) => {
    if (this.props.value.includes(val)) return;
    const changedValuePath = SearchRune(val);
    const value4 = SearchRune(this.props.value[4]);
    if (value4[0] === changedValuePath[0]) {
      // If tree is the same.
      if (value4[1] === changedValuePath[1]) {
        // If row is the same.
        const stateToReturn = [...this.props.value];
        stateToReturn[4] = val;
        this.props.onChange(stateToReturn);
      } else {
        const stateToReturn = [...this.props.value];
        stateToReturn[5] = this.props.value[4];
        stateToReturn[4] = val;
        this.props.onChange(stateToReturn);
      }
    } else {
      const stateToReturn = [...this.props.value];
      stateToReturn[4] = val;
      stateToReturn[5] = null;
      this.props.onChange(stateToReturn);
    }
  };

  onChangeMainTree = (val) => {
    const changedValuePath = SearchRune(val);
    const pathOfRuneThatWillBeChanged = SearchRune(this.props.value[changedValuePath[1]]);
    if (
      pathOfRuneThatWillBeChanged[0] !== changedValuePath[0] &&
      this.props.value[changedValuePath[1]] !== null
    ) {
      // Different tree
      const stateToReturn = [...this.props.value];
      stateToReturn[0] = null;
      stateToReturn[1] = null;
      stateToReturn[2] = null;
      stateToReturn[3] = null;
      stateToReturn[changedValuePath[1]] = val;
      /** if the subtree will be the same as main tree. */
      console.log(SearchRune(this.props.value[4])[0], changedValuePath[0]);
      if (
        SearchRune(this.props.value[4])[0] === changedValuePath[0] ||
        SearchRune(this.props.value[5])[0] === changedValuePath[0]
      ) {
        stateToReturn[4] = null;
        stateToReturn[5] = null;
      }
      this.props.onChange(stateToReturn);
    } else {
      const stateToReturn = [...this.props.value];
      stateToReturn[changedValuePath[1]] = val;
      this.props.onChange(stateToReturn);
    }
  };

  RuneTree = ({ tree, treeIndex, isSubTree }) => {
    const { RuneRow } = this;
    return (
      <RuneTreeWrapper>
        {tree.slots.map((row, rowIndex) => (
          <RuneRow
            row={row}
            treeIndex={treeIndex}
            rowIndex={rowIndex}
            isSubTree={isSubTree}
          />
        ))}
      </RuneTreeWrapper>
    );
  };

  RuneRow = ({
    row, treeIndex, rowIndex, isSubTree,
  }) => {
    if (rowIndex === 0 && isSubTree) return null;
    return (
      <RuneRowWrapper>
        {row.runes.map((rune, runeIndex) => (
          <SelectableRune
            isSubTree={isSubTree}
            selectedTree={treeIndex}
            selectedRow={rowIndex}
            selectedRune={runeIndex}
            onChange={
              isSubTree
                ? () => this.onChangeSubTree(rune.id)
                : () => this.onChangeMainTree(rune.id)
            }
            value={this.props.value.includes(rune.id)}
            width={rowIndex === 0 ? 50 : 40}
          />
        ))}
      </RuneRowWrapper>
    );
  };

  render() {
    const { RuneTree } = this;
    let primaryTree = 0;
    if (this.props.value[0]) {
      [primaryTree] = SearchRune(this.props.value[0]);
    } else if (this.props.value[1]) {
      [primaryTree] = SearchRune(this.props.value[1]);
    } else if (this.props.value[2]) {
      [primaryTree] = SearchRune(this.props.value[0]);
    } else if (this.props.value[3]) {
      [primaryTree] = SearchRune(this.props.value[3]);
    }
    let secondaryTree = 1;
    if (this.props.value[4]) {
      [secondaryTree] = SearchRune(this.props.value[4]);
    } else if (this.props.value[5]) {
      [secondaryTree] = SearchRune(this.props.value[5]);
    }
    return (
      <RuneTreeWrapperWrapper>
        <RunePageWrapper>
          <Tabs
            defaultActiveKey={`${primaryTree}`}
            tabBarGutter={0}
            tabBarStyle={{ marginBottom: 0 }}
          >
            {runes.map((tree, treeIndex) => (
              <TabPane
                tab={
                  <RuneTreeImageWithDescription
                    selectedTree={treeIndex}
                    width={20}
                  />
                }
                key={treeIndex}
              >
                <RuneTree tree={tree} treeIndex={treeIndex} isSubTree={false} />
              </TabPane>
            ))}
          </Tabs>
        </RunePageWrapper>
        <RunePageWrapper>
          <Tabs
            defaultActiveKey={`${secondaryTree}`}
            tabBarGutter={0}
            tabBarStyle={{ marginBottom: 0 }}
          >
            {runes.map((tree, treeIndex) =>
                (this.props.value[0]
                  ? runes[primaryTree].id !== tree.id
                  : true) && (
                  <TabPane
                    tab={
                      <RuneTreeImageWithDescription
                        selectedTree={treeIndex}
                        width={20}
                      />
                    }
                    key={treeIndex}
                  >
                    <RuneTree tree={tree} treeIndex={treeIndex} isSubTree />
                  </TabPane>
                ))}
          </Tabs>
        </RunePageWrapper>
      </RuneTreeWrapperWrapper>
    );
  }
}

export default RunePage;
