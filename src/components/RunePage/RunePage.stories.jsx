import React from 'react';
import { storiesOf } from '@storybook/react';
import 'antd/lib/tabs/style/index.css';
import RunePage from 'components/RunePage';
// import 'antd/lib/grid/style/index.css';
import SelectableRune from 'components/RunePage/SelectableRune';


storiesOf('Rune Page', module)
  .add('Basic', () => (
    <RunePage
      value={[
        8112,
        8143,
        8138,
        8135,
        8014,
        9111,
      ]}
      onChange={e => console.log(e)}
    />
  ))
  .add('Selectable Rune', () => (
    <SelectableRune
      selectedTree={0}
      selectedRow={2}
      selectedRune={0}
    />
  ));

