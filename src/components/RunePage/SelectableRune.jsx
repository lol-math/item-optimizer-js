import React from 'react';
import PropTypes from 'prop-types';
import Styled from 'styled-components';
import { RuneImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';

// import './Rune.less';
const PseudoInputRadio = Styled.input.attrs({
  type: 'checkbox',
})`
  display: none;
  & + div {
    transition: filter 200ms ease;
  }
  & + div:not(:hover) {
    filter: grayscale(100%);
  }
  &:checked + div {
    filter: grayscale(0%);
  }
`;

const RuneSelectableWrapper = Styled.div`
  display: inline-block;
  position: relative;
  border-radius: 50%;
  border-width: 2px;
  border-style: ${({ row }) => (row === 0 ? 'none' : 'solid')};
  border-color: ${({ tree }) => {
    switch (tree) {
      case 0:
        return '#D84646';
      case 1:
        return '#48AFBB';
      case 2:
        return '#B9A87C';
      case 3:
        return '#A3D38A';
      case 4:
        return '#838DF8';
      default:
        return '#fff';
    }
  }};
  cursor: pointer;
  &:after{
    content: '';
    border: 3px solid;
    transition: border-color 200ms ease;
    border-color: #00000000;
    position: absolute;
    border-radius: 50%;
    top: -8px;
    bottom: -8px;
    right: -8px;
    left: -8px;
    pointer-events: none;
  }
  &:hover {
    &:after {
      border-color: ${({ tree }) => {
    switch (tree) {
      case 0:
        return '#D84646';
      case 1:
        return '#48AFBB';
      case 2:
        return '#B9A87C';
      case 3:
        return '#A3D38A';
      case 4:
        return '#838DF8';
      default:
        return '#fff';
    }
  }};
    }
  }
`;

const SelectableRune = ({
  selectedTree,
  selectedRow,
  selectedRune,
  value,
  onChange,
  width,
}) => (
  <label>
    <PseudoInputRadio
      checked={value}
      onChange={e => onChange(e.target.checked)}
    />
    <RuneSelectableWrapper tree={selectedTree} row={selectedRow}>
      <RuneImageWithDescription
        selectedTree={selectedTree}
        selectedRow={selectedRow}
        selectedRune={selectedRune}
        width={width}
      />
    </RuneSelectableWrapper>
  </label>
);
SelectableRune.propTypes = {
  /** The tree the rune is in */
  selectedTree: PropTypes.number.isRequired,
  /** The row the rune is in inside the tree the rune is in */
  selectedRow: PropTypes.number.isRequired,
  /** The column the rune is in the row the rune is in inside the tree the rune is in   */
  selectedRune: PropTypes.number.isRequired,
  /** Is the rune selected? */
  value: PropTypes.bool.isRequired,
  /** Callback when the rune is clicked. */
  onChange: PropTypes.func.isRequired,
  /** The size of the image */
  width: PropTypes.number,
};
SelectableRune.defaultProps = {
  width: undefined,
};
SelectableRune.displayName = 'SelectableRune';
export default SelectableRune;
