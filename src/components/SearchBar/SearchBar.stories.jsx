import React from 'react';
import { storiesOf } from '@storybook/react';
import SearchBar from './index';

storiesOf('Search Bar', module)
  .add('Basic', () => (
    <SearchBar
      onChange={v => console.log(v)}
    />
  ));

