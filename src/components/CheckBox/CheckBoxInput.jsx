import styled from 'styled-components';
import checkBoxSpritesheet from 'Images/checkbox-spritesheet.png';

export default styled.input.attrs({
  type: 'checkbox',
})`
  appearance: none;
  margin-right: 5px;
  width: 14px;
  &:before {
      position: relative;
      display: inline-block;
      vertical-align: baseline;
      content: '';
      height: 14px;
      width: 14px;
      background: url(${checkBoxSpritesheet}) no-repeat 0 0;
      background-size: 100%;
      cursor: pointer;
  }
  &:focus {
      outline: none;
  }
  &:before {
      background-position-y: 0;
  }
  &:hover:before {
      background-position-y: -14px;
  }
  &:active:before {
      background-position-y: -14px;
  }
  &:checked {
      &:before {
          background-position-y: -28px;
      }
      &:hover:before {
          background-position-y: -42px;
      }
      &:active:before {
          background-position-y: -42px;
      }
  }
`;
