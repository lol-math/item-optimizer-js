import React from 'react';
import PropTypes from 'prop-types';

import CheckBoxLabel from 'components/CheckBox/CheckBoxLabel';
import CheckBoxInput from 'components/CheckBox/CheckBoxInput';

const CheckBox = ({
  value, onChange, children,
}) => (
  <CheckBoxLabel>
    <CheckBoxInput
      checked={value}
      onChange={e => onChange(e.target.checked)}
    />
    {children}
  </CheckBoxLabel>
);
CheckBox.defaultProps = {
  children: '',
};
CheckBox.propTypes = {
  children: PropTypes.node,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.bool.isRequired,
};
export default CheckBox;

