import React from 'react';
import { storiesOf } from '@storybook/react';

import CheckBox from 'components/CheckBox';

storiesOf('CheckBox', module)
  .add('Checked', () => (
    <CheckBox
      checked
    />
  ))
  .add('Unchecked', () => (
    <CheckBox
      checked={false}
    />
  ))
  .add('With label', () => (
    <CheckBox>
      Label
    </CheckBox>
  ))
  .add('On Change', () => (
    <CheckBox
      onChange={e => console.log(e)}
    >
      Label
    </CheckBox>
  ));

