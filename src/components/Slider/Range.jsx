import PropTypes from 'prop-types';
import React from 'react';
import 'antd/lib/input-number/style/index.css';
import Handle from 'components/Slider/LeagueStyledHandle';
import { StyledRange } from './SliderStyles';


const LeagueRange = ({ onChange, value, ...rest }) => (
  <StyledRange
    onChange={onChange}
    value={value}
    handle={Handle}
    {...rest}
  />
);
LeagueRange.propTypes = {
  /** Triggers when dragging slider or changing input value. */
  onChange: PropTypes.func.isRequired,
  /** The value of each of the nodes in the slider range. */
  value: PropTypes.arrayOf(PropTypes.number).isRequired,
};
LeagueRange.displayName = 'LeagueRange';

export default LeagueRange;
