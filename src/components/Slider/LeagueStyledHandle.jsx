import React from 'react';
import Slider from 'rc-slider';
import PropTypes from 'prop-types';

const Handle = ({
  value,
  ...restProps
}) => (
  <Slider.Handle value={value} {...restProps} />
);
Handle.propTypes = {
  value: PropTypes.number.isRequired,
};

export default Handle;
