import Styled from 'styled-components';
import Slider from 'rc-slider';
import SliderButton from 'Images/slider-btn.png';
import 'rc-slider/assets/index.css';

const SliderRange = Slider.Range;


export const LeagueSliderWrapper = Styled.div`
  display: flex;
  text-align: center;
`;
const StyledHoC = Component => Styled(Component)`
  margin: 7.5px 15px;
  .rc-slider-handle {
    background-color: rgba(17, 34, 51, 0);
    border: 0;
    border-radius: 0;
    background-image: url(${SliderButton});
    background-size: 100%;
    width: 30px;
    height: 30px;
    margin-left: -15px;
    margin-top: -10px;
  }
  .rc-slider-rail, .rc-slider-track {
    height: 10px;
    border-radius: 0;
    border: 1px solid #372E18;
  }
  .rc-slider-track{
    background: rgba(255, 255, 255, 0.37);
  }
  .rc-slider-rail{
    background: rgba(0, 0, 0, 0);
  }
  .rc-slider-mark-text {
    line-height: 1;
  }
  .rc-slider-dot{
    display: none;
  }
`;
export const StyledSlider = StyledHoC(Slider);
export const StyledRange = StyledHoC(SliderRange);
