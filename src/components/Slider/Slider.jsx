import React from 'react';
import { InputNumber } from 'antd';
import PropTypes from 'prop-types';
import { LeagueSliderWrapper, StyledSlider } from 'components/Slider/SliderStyles';
import Handle from './LeagueStyledHandle';

const LeagueSlider = ({ onChange, value, ...props }) => (
  <LeagueSliderWrapper>
    <InputNumber
      value={value}
      onChange={onChange}
      {...props}
    />
    <StyledSlider
      handle={Handle}
      {...props}
      onChange={onChange}
      value={value}
    />
  </LeagueSliderWrapper>
);
LeagueSlider.propTypes = {
  /** Triggers when dragging slider or changing input value. */
  onChange: PropTypes.func.isRequired,
  /** The current value to render. These should be accompanied by the exact values at all times. */
  value: PropTypes.number.isRequired,
};
LeagueSlider.displayName = 'LeagueSlider';

export default LeagueSlider;
