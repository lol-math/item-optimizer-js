import React from 'react';
import { storiesOf } from '@storybook/react';
import 'antd/lib/input-number/style/index.css';
import { Slider, Range } from 'components/Slider';

storiesOf('Slider', module)
  .add('Basic', () => (
    <Slider
      value={100}
      onChange={e => console.log(e)}
    />
  ))
  .add('Range', () => (
    <Range
      value={[10, 20]}
      onChange={e => console.log(e)}
    />
  ));

