import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  item as itemData,
  Item_Profile as ItemProfile,
} from 'item-optimizer-data';
import SearchBar from 'components/SearchBar';
import styled from 'styled-components';
import ItemList from './ItemList';
import FilterBar from 'components/FilterBar';

const itemsObject = itemData.data;

const items = Object.keys(ItemProfile).map(itemId => ({
  ...itemsObject[itemId],
  id: itemId,
}));

const filterItemsByName = ({ item, searchText }) =>
  (searchText === ''
    ? true
    : item.name.toLowerCase().includes(searchText.toLowerCase()));

const sortItemsByName = (a, b) => {
  if (typeof a !== 'undefined') {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
  }
  return 0;
};
const getItems = searchText =>
  items
    .filter(item => filterItemsByName({ item, searchText }))
    .sort(sortItemsByName);

const FilterableListWrapper = styled.div`
  position: relative;
`;

/** Some Description */
export default class FilterableChampionList extends Component {
  static propTypes = {
    /** Gets called when the user clicks a champion. Returns the championKey. */
    onChange: PropTypes.func,
    /** The championKey of the currently selected champion. (who will get a "selected border") */
    value: PropTypes.string,
  };
  static defaultProps = {
    onChange: () => {},
    value: null,
  };
  static displayName = 'FilterableChampionList';

  /**
   * This has to be before State, as state references this function.
   * It won't be called until render anyway, so the dependency on state doesn't matter.
   */
  search = (e) => {
    this.setState({
      search: e.target.value,
    });
  };
  state = {
    search: '',
  };

  render() {
    return (
      <FilterableListWrapper>
        <FilterBar>
          <SearchBar onChange={this.search} placeholder="Search Item" />
        </FilterBar>
        <ItemList
          items={getItems(this.state.search)}
          onChange={this.props.onChange}
          value={this.props.value}
        />
      </FilterableListWrapper>
    );
  }
}
