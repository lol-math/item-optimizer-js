import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import RoleFilterDisabled from 'Images/role-filter-disabled.png';
import RoleFilterEnabled from 'Images/role-filter-enabled.png';

const RoleButton = styled.div.attrs({
  role: 'button',
  tabIndex: 0,
})`
  width: 28px;
  height: 20px;
  background-size: cover;
  cursor: pointer;
  outline: none;
  background-image: url(${RoleFilterDisabled});
  background-repeat: no-repeat;
  background-position-X: ${({ gameRole }) => {
    switch (gameRole) {
      case 'Fighter': return -2.2;
      case 'Tank': return 18.7;
      case 'Mage': return 40;
      case 'Assassin': return 60.5;
      case 'Support': return 82;
      default: return 102.5;
    }
  }}%;
  &:hover, &[data-selected=true]{
    background-image: url(${RoleFilterEnabled});
  }
`;

const RoleButtonsWrapper = styled.div`
  vertical-align: middle;
  display: inline-flex;
  margin: 0 5px;
`;

const RoleFilter = ({ value, onChange }) => (
  <RoleButtonsWrapper>
    {['Fighter', 'Tank', 'Mage', 'Assassin', 'Support', 'Marksman'].map(gameRole => (
      <RoleButton gameRole={gameRole} data-selected={gameRole === value} onClick={() => onChange(gameRole === value ? '' : gameRole)} />
    ))}
  </RoleButtonsWrapper>
);
RoleFilter.propTypes = {
  /** The currently selected role. */
  value: PropTypes.string.isRequired,
  /** Gets called when a role is clicked. Returns the value of the role. */
  onChange: PropTypes.func.isRequired,
};
RoleFilter.displayName = 'RoleFilter';
export default RoleFilter;
