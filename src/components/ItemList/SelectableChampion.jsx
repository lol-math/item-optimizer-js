import React from 'react';
import PropTypes from 'prop-types';
import { ChampionImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';
import { championFull } from 'item-optimizer-data';
import styled from 'styled-components';
import RectangleSprite from 'Images/reticle-sprite-324x324-2x6f.png';
// import './Champion.less';
const champions = championFull.data;

const SelectableChampionWrapper = styled.div`
  border-style: solid;
  border-color: ${({ theme }) => theme.border.grey};
  border-width: 1px;
  position: relative;
`;

const SelectableChampionWrapperPositioner = styled.div`
  position: absolute;

  text-align: center;
  user-select: none;
  outline: none;
  &[data-selectable='true'] {
    &[data-selected='false']:hover,
    &[data-selected='false']:focus,
    &[data-selected='true'] {
      .ChampionHoverBorderTarget:after {
        content: '';
        position: absolute;
        top: -7px;
        bottom: -7px;
        right: -7px;
        left: -7px;
        background: url(${RectangleSprite});
        background-size: 200%;
        background-position-y: 40%;
      }
    }
    &[data-selected='false']:hover .ChampionHoverBorderTarget:after,
    &[data-selected='false']:focus .ChampionHoverBorderTarget:after {
      background-position-y: 20%;
    }
  }
`;

const ChampionName = styled.div`
  font-size: 10px;
`;

const SelectableChampion = ({
  onClick,
  championKey,
  selected,
  selectable,
  displayName,
}) => (
  <SelectableChampionWrapperPositioner
    {...selectable && {
      'data-selected': selected,
      'data-selectable': selectable,
      onClick: () => onClick(championKey),
      onKeyPress: e =>
        (e.key === ' ' || e.key === 'Enter') && onClick(championKey),
      role: 'button',
      tabIndex: 0,
    }}
  >
    <SelectableChampionWrapper
      selected={selected}
      className="ChampionHoverBorderTarget"
    >
      <ChampionImageWithDescription championKey={championKey} width={56} />
    </SelectableChampionWrapper>
    <ChampionName>
      {' '}
      {displayName ? champions[championKey].name : null}
    </ChampionName>
  </SelectableChampionWrapperPositioner>
);

SelectableChampion.defaultProps = {
  selected: false,
  selectable: true,
  displayName: true,
};
SelectableChampion.displayName = 'SelectableChampion';
SelectableChampion.propTypes = {
  /** Can the user click on this champion? When this is true, hovering will show a border. */
  selectable: PropTypes.bool,
  /** Which champion to render */
  championKey: PropTypes.string.isRequired,
  /** Is this champion currently selected? Shows a different border than the selected border. */
  selected: PropTypes.bool,
  /** function that trigger when the user either clicks on element or presses enter/space when the element is tab-selected. */
  onClick: PropTypes.func,
  /** Should the name of the champion be displayed? */
  displayName: PropTypes.bool,
};

export default SelectableChampion;
