import { Item_Profile as itemProfile, item } from 'item-optimizer-data';
import React from 'react';
import { storiesOf } from '@storybook/react';

import { ItemList, FilterableItemList } from './index';

const itemsObject = item.data;

const items = Object.keys(itemProfile).map(itemId => ({
  ...itemsObject[itemId],
  id: itemId,
}));

storiesOf('Item List', module)
  .add('Basic', () => <ItemList items={items} />)
  .add('Filterable Champion List', () => <FilterableItemList />);
