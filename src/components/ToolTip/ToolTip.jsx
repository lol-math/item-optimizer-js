import { Tooltip } from 'react-tippy';
import PropTypes from 'prop-types';
import React from 'react';

const LeagueToolTip = props => (
  <Tooltip
    style={{ display: 'inline-block' }}
    arrow="true"
    delay="500"
    theme="custom"
    maxWidth="300px"
    {...props}
  >
    {props.children}
  </Tooltip >
);
LeagueToolTip.propTypes = {
  /** The element to tip about */
  children: PropTypes.node.isRequired,
  /** The content of the tooltip */
  html: PropTypes.node,
  /** The content of the tooltip. Do not use the html if you use title! */
  title: PropTypes.string,
};
LeagueToolTip.displayName = 'LeagueToolTip';

export default LeagueToolTip;