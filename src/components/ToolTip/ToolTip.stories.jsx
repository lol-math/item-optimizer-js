import React from 'react';
import { storiesOf } from '@storybook/react';
import { ToolTip, PopOver } from './index';

storiesOf('ToolTip', module)
  .add('Basic', () => (
    <ToolTip
      html={<span>Some text</span>}
    >
      Hover me
    </ToolTip>
  ))
  .add('Title', () => (
    <ToolTip
      title="Title"
    >
      Hover me
    </ToolTip>
  ))
  .add('PopOver', () => (
    <PopOver
      html={<span>Some text</span>}
    >
      Click me!
    </PopOver>
  ));

