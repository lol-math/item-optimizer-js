import { Tooltip } from 'react-tippy';
import PropTypes from 'prop-types';
import React from 'react';

const LeaguePopOver = props => (
  <Tooltip
    style={{ display: 'inline-block' }}
    animateFill={false}
    trigger="click"
    interactive
    theme="popover"
    animation="scale"
    position="bottom"
    transitionFlip={false}
    inertia={false}
    sticky
    stickyDuration={0}
    duration={100}
    {...props}
  >
    {props.children}
  </Tooltip>
);
LeaguePopOver.propTypes = {
  /** The element to tip about */
  children: PropTypes.node.isRequired,
  /** The content of the tooltip */
  html: PropTypes.node,
  /** The content of the tooltip. Do not use the html if you use title! */
  title: PropTypes.string,
};
LeaguePopOver.displayName = 'LeaguePopOver';
export default LeaguePopOver;
