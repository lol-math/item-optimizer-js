import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { TileImageWithDescription } from 'components/DdragonImages/DdImageWithDescription';

const ChampionImageWithLevelWrapper = styled.div`
  display: inline-block;
  position: relative;
`;
const LevelPositioner = styled.div`
  position: absolute;
  width: 100%;
  text-align:center;
  bottom: 0%;
`;
const LevelWrapper = styled.span`
  background: ${({ theme }) => theme.background.main};
  border-color: ${({ theme }) => theme.border.brightGold};
  border-width: 2px;
  border-style: solid;
  border-radius: 15px;
  padding: 5px 15px;
`;

const TileImageWrapper = styled.div`
  overflow: hidden;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  height: 128px;
`;
const ChampionTileWithLevel = ({ level, championKey }) => (
  <ChampionImageWithLevelWrapper>
    <LevelPositioner>
      <LevelWrapper>
        {level}
      </LevelWrapper>
    </LevelPositioner>
    <TileImageWrapper>
      <TileImageWithDescription
        championKey={championKey}
        num={0}
        width={128}
      />
    </TileImageWrapper>
  </ChampionImageWithLevelWrapper>
);
ChampionTileWithLevel.propTypes = {
  /** Which level to render */
  level: PropTypes.number.isRequired,
  /** Which champion to render. */
  championKey: PropTypes.string.isRequired,
};
ChampionTileWithLevel.displayName = 'ChampionTileWithLevel';
export default ChampionTileWithLevel;
