import React from 'react';
import { storiesOf } from '@storybook/react';

import ChampionStats from 'components/ChampionStats';
import ChampionTileWithLevel from 'components/ChampionStats/ChampionTileWithLevel';
import ResourceBar from 'components/ChampionStats/ResourceBar';
import ChampionStat from 'components/ChampionStats/ChampionStat';

storiesOf('Champion Stats', module)
  .add('Basic', () => (
    <ChampionStats
      championKey="Rengar"
      level={18}
      stats={{
        attackDamage: 1000,
        armor: 1000,
        attackSpeed: 1000,
        criticalStrikeChance: 1000,
        abilityPower: 1000,
        magicResist: 1000,
        coolDownReduction: 1000,
        movementSpeed: 1000,
        healthRegeneration: 1000,
        armorPenetration: 1000,
        lifeSteal: 1000,
        range: 1000,
        manaRegeneration: 1000,
        magicPenetration: 1000,
        spellVamp: 1000,
        tenacity: 1000,
        damageAmp: 1000,
        physicalDamageAmp: 1000,
        magicalDamageAmp: 1000,
        resistanceReduction: 1000,
      }}
    />
  ))
  .add('Champion Image With Level', () => (
    <ChampionTileWithLevel
      championKey="Aatrox"
      level={12}
    />
  ))
  .add('Health Bar', () => (
    <ResourceBar
      amount={1000}
      baseAmount={800}
      type="Health"
    />
  ))
  .add('Champion Stat', () => (
    <ChampionStat
      statType="attackDamage"
      amount={1000}
      baseAmount={200}
    />
  ));

