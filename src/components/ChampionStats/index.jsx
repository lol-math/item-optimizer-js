import React from 'react';
import PropTypes from 'prop-types';

import { championFull } from 'item-optimizer-data';

import ChampionStatsWrapper from 'components/ChampionStats/ChampionStatsWrapper';
import ChampionTileWithLevel from 'components/ChampionStats/ChampionTileWithLevel';
import TextStatsWrapper, {
  TextStatColumn,
} from 'components/ChampionStats/TextStatsWrapper';

import ResourceBar from 'components/ChampionStats/ResourceBar';
import ChampionStat from 'components/ChampionStats/ChampionStat';

const champions = championFull.data;

const ChampionStats = ({ championKey, level, stats }) => {
  const champion = champions[championKey];
  return (
    <ChampionStatsWrapper>
      <ChampionTileWithLevel level={level} championKey={championKey} />
      <ResourceBar
        amount={stats.hp}
        baseAmount={champion.stats.hp + champion.stats.hpperlevel * level}
        type="Health"
      />
      <ResourceBar
        amount={stats.mp}
        baseAmount={champion.stats.mp + champion.stats.mpperlevel * level}
        type={champion.partype}
      />
      <TextStatsWrapper>
        {[
          [
            {
              // Attack Damage
              statType: 'attackDamage',
              amount: stats.attackDamage,
              baseAmount:
                champion.stats.attackdamage +
                champion.stats.attackdamageperlevel * level,
            },
            {
              // Armor
              statType: 'armor',
              amount: stats.armor,
              baseAmount:
                champion.stats.armor + champion.stats.armorperlevel * level,
            },
            {
              // Attack Speed = SPECIAL, attackspeed per level should be done in the algo.
              statType: 'attackSpeed',
              amount: 0.625 / (1 + champion.stats.attackspeedoffset),
              baseAmount: 90,
            },
            {
              // Critical Strike Chance
              statType: 'criticalStrikeChance',
              amount: stats.criticalStrikeChance,
              baseAmount:
                champion.stats.crit + champion.stats.critperlevel * level,
            },
          ],
          [
            {
              // Ability Power
              statType: 'abilityPower',
              amount: stats.abilityPower,
              baseAmount: 0,
            },
            {
              // Magic Resist
              statType: 'magicResist',
              amount: stats.magicResist,
              baseAmount:
                champion.stats.spellblock +
                champion.stats.spellblockperlevel * level,
            },
            {
              // Cool Down Reduction
              statType: 'coolDownReduction',
              amount: stats.coolDownReduction,
              baseAmount: 0,
            },
            {
              // Movement Speed
              statType: 'movementSpeed',
              amount: stats.movementSpeed,
              baseAmount: champion.stats.movespeed,
            },
          ],
          [
            {
              // Health Regeneration
              statType: 'healthRegeneration',
              amount: stats.healthRegeneration,
              baseAmount:
                champion.stats.hpregen + champion.stats.hpregenperlevel * level,
            },
            {
              // Armor Penetration
              statType: 'armorPenetration',
              amount: stats.armorPenetration,
              baseAmount: 0,
            },
            {
              // Life Steal
              statType: 'lifeSteal',
              amount: stats.lifeSteal,
              baseAmount: 0,
            },
            {
              // Range
              statType: 'range',
              amount: stats.range,
              baseAmount: champion.stats.attackrange,
            },
          ],
          [
            {
              // Other Resource Regeneration (e.g. Mana Regeneration)
              statType: 'manaRegeneration',
              amount: stats.manaRegeneration,
              baseAmount:
                champion.stats.mpregen + champion.stats.mpregenperlevel * level,
            },
            {
              // Magic Penetration
              statType: 'magicPenetration',
              amount: stats.magicPenetration,
              baseAmount: 0,
            },
            {
              // Spell Vampirism
              statType: 'spellVamp',
              amount: stats.spellVamp,
              baseAmount: 0,
            },
            {
              // Tenacity
              statType: 'tenacity',
              amount: stats.tenacity,
              baseAmount: 0,
            },
          ],
          [
            {
              // Damage Amplification
              statType: 'damageAmp',
              amount: stats.damageAmp,
              baseAmount: 0,
            },
            {
              // Physical Damage Amplification
              statType: 'physicalDamageAmp',
              amount: stats.physicalDamageAmp,
              baseAmount: 0,
            },
            {
              // Magical Damage Amplification
              statType: 'magicalDamageAmp',
              amount: stats.magicalDamageAmp,
              baseAmount: 0,
            },
            {
              // Resistance Reduction
              statType: 'resistanceReduction',
              amount: stats.resistanceReduction,
              baseAmount: 0,
            },
          ],
        ].map((column, i) => (
          <TextStatColumn key={i}>
            {column.map(props => (
              <ChampionStat {...props} key={props.statType} />
            ))}
          </TextStatColumn>
        ))}
      </TextStatsWrapper>
    </ChampionStatsWrapper>
  );
};

ChampionStats.propTypes = {
  /** An object containing stats to display */
  stats: PropTypes.shape({
    hp: PropTypes.number.isRequired,
    mp: PropTypes.number.isRequired,
    attackDamage: PropTypes.number.isRequired,
    armor: PropTypes.number.isRequired,
    attackSpeed: PropTypes.number.isRequired,
    criticalStrikeChance: PropTypes.number.isRequired,
    abilityPower: PropTypes.number.isRequired,
    magicResist: PropTypes.number.isRequired,
    coolDownReduction: PropTypes.number.isRequired,
    movementSpeed: PropTypes.number.isRequired,
    healthRegeneration: PropTypes.number.isRequired,
    armorPenetration: PropTypes.number.isRequired,
    lifeSteal: PropTypes.number.isRequired,
    range: PropTypes.number.isRequired,
    manaRegeneration: PropTypes.number.isRequired,
    magicPenetration: PropTypes.number.isRequired,
    spellVamp: PropTypes.number.isRequired,
    tenacity: PropTypes.number.isRequired,
    damageAmp: PropTypes.number.isRequired,
    physicalDamageAmp: PropTypes.number.isRequired,
    magicalDamageAmp: PropTypes.number.isRequired,
    resistanceReduction: PropTypes.number.isRequired,
  }).isRequired,
  /** The champion which is rendered */
  championKey: PropTypes.string.isRequired,
  /** The level on which the stats must be based */
  level: PropTypes.number.isRequired,
};
ChampionStats.displayName = 'ChampionStats';
export default ChampionStats;
