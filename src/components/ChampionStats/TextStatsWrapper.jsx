import styled from 'styled-components';

export default styled.div`
box-sizing: border-box;
  display: grid;
  grid-template-columns: repeat(5, auto);
  height: 128px;
  border: 2px solid ${({ theme }) => theme.border.gold};
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-left: 0;
`;

export const TextStatColumn = styled.div`
  grid-template-rows: repeat(3, 1fr);
  display: grid;
  padding: 2px 5px;
  &:nth-child(2n) {
    background: #ffffff21;
  }
`;
