import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { ToolTip as LeagueToolTip } from 'components/ToolTip';
import StatToolTipStyle from 'components/ChampionStats/StatToolTipStyle';
import StatDictionary from './StatDictionary.json';
import StatImageDictionary from 'components/ChampionStats/StatImageDictionary';
import BaseAmountSpan from 'components/ChampionStats/BaseAmountSpan';
import BonusAmountSpan from 'components/ChampionStats/BonusAmountSpan';

const ChampionStatImage = styled.img`
  vertical-align: middle;
`;

const ChampionStat = ({ statType, amount, baseAmount }) => (
  <LeagueToolTip
    html={
      <StatToolTipStyle>
        <h3>{StatDictionary[statType]}</h3>
        <BaseAmountSpan>{baseAmount}</BaseAmountSpan>
        {' + '}
        <BonusAmountSpan>{amount - baseAmount}</BonusAmountSpan>
      </StatToolTipStyle>
    }
  >
    <span>
      <ChampionStatImage
        src={StatImageDictionary[statType]}
        alt={StatDictionary[statType]}
      />
      {+amount.toFixed(2)}
    </span>
  </LeagueToolTip>
);
ChampionStat.propTypes = {
  statType: PropTypes.string.isRequired,
  baseAmount: PropTypes.number.isRequired,
  amount: PropTypes.number.isRequired,
};
export default ChampionStat;
