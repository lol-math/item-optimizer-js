import AttackDamage from 'Images/HUDAtlas/statIcons/AttackDamage.gif';
import AbilityPower from 'Images/HUDAtlas/statIcons/AbilityPower.gif';
import Armor from 'Images/HUDAtlas/statIcons/Armor.gif';
import MagicResist from 'Images/HUDAtlas/statIcons/MagicResist.gif';
import AttackSpeed from 'Images/HUDAtlas/statIcons/AttackSpeed.gif';
import CooldownReduction from 'Images/HUDAtlas/statIcons/CooldownReduction.gif';
import CriticalStrike from 'Images/HUDAtlas/statIcons/CriticalStrike.gif';
import MovementSpeed from 'Images/HUDAtlas/statIcons/MovementSpeed.gif';
import HealthRegen from 'Images/HUDAtlas/statIcons/HealthRegen.gif';
import ResourceRegen from 'Images/HUDAtlas/statIcons/ResourceRegen.gif';
import ArmorPenetration from 'Images/HUDAtlas/statIcons/ArmorPenetration.gif';
import MagicPenetration from 'Images/HUDAtlas/statIcons/MagicPenetration.gif';
import Lifesteal from 'Images/HUDAtlas/statIcons/Lifesteal.gif';
import Spellvamp from 'Images/HUDAtlas/statIcons/Spellvamp.gif';
import AttackRange from 'Images/HUDAtlas/statIcons/Range.gif';
import Tenacity from 'Images/HUDAtlas/statIcons/Tenacity.gif';
import DamageAmplification from 'Images/Hemoplague_old.png';
import PhysicalDamageAmplification from 'Images/Giant_Slayer_item.png';
import MagicalDamageAmplification from 'Images/Abyssal_Scepter_item.png';
import ResistanceReduction from 'Images/Gatling_Gun_old.png';

export default {
  attackDamage: AttackDamage,
  armor: Armor,
  attackSpeed: AttackSpeed,
  criticalStrikeChance: CriticalStrike,
  abilityPower: AbilityPower,
  magicResist: MagicResist,
  coolDownReduction: CooldownReduction,
  movementSpeed: MovementSpeed,
  healthRegeneration: HealthRegen,
  armorPenetration: ArmorPenetration,
  lifeSteal: Lifesteal,
  range: AttackRange,
  manaRegeneration: ResourceRegen,
  magicPenetration: MagicPenetration,
  spellVamp: Spellvamp,
  tenacity: Tenacity,
  damageAmp: DamageAmplification,
  physicalDamageAmp: PhysicalDamageAmplification,
  magicalDamageAmp: MagicalDamageAmplification,
  resistanceReduction: ResistanceReduction,
};
