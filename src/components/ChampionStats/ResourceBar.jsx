import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import HealthImage from 'Images/HUDAtlas/Bars/HealthBig.gif';
import ManaImage from 'Images/HUDAtlas/Bars/ManaBig.gif';
import FerocityImage from 'Images/HUDAtlas/Bars/ResourceWhiteBig.gif';
import {ToolTip as LeagueToolTip} from 'components/ToolTip';
import RedStuffImage from 'Images/HUDAtlas/Bars/ResourceRedBig.gif';

import BaseAmountSpan from 'components/ChampionStats/BaseAmountSpan';
import BonusAmountSpan from 'components/ChampionStats/BonusAmountSpan';

const ResourceBarImage = styled.div`
  background: url(${
  ({ type }) => {
    switch (type) {
      case 'Health':
        return HealthImage;
      case 'Mana':
        return ManaImage;
      case 'Ferocity':
        return FerocityImage;
      default:
        return RedStuffImage;
    }
  }});
  background-position: right;
  width: 128px;
  height: 16px;
  transform: rotate(90deg) translateY(-16px);
  transform-origin: top left;
  text-align: center;
  position: absolute;
  color: #000;
  font-weight: 900;
  line-height: 16px;
`;
const ResourceBarWrapper = styled.div`
  display: block;
  height: 128px;
  width: 16px;
`;

const ResourceBar = ({ amount, type, baseAmount }) => (
  <LeagueToolTip
    html={
      <div>
        <h3>{type}</h3>
        <BaseAmountSpan>
          {baseAmount}
        </BaseAmountSpan>
        {' + '}
        <BonusAmountSpan>
          {amount - baseAmount}
        </BonusAmountSpan>
      </div>}
  >
    <ResourceBarWrapper>
      <ResourceBarImage type={type}>
        {amount}
      </ResourceBarImage>
    </ResourceBarWrapper>
  </LeagueToolTip>
);
ResourceBar.propTypes = {
  /** The total amount of the stat */
  amount: PropTypes.number.isRequired,
  /** The type of resource, must be either Health, Mana or Ferocity (white). Any other stat will result in a red bar. */
  type: PropTypes.string.isRequired,
  /** The base amount of the stat */
  baseAmount: PropTypes.number.isRequired,
};
export default ResourceBar;
