import styled from 'styled-components';

export default styled.div`
    margin: 5px 0;
    display: inline-grid;
    grid-template-columns: auto auto auto auto;
    max-width: 100%;
    border-radius: 5px;
    background: #00000033;
    height: 128px;
`;