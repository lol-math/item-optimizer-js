import styled, { keyframes } from 'styled-components';

const ButtonHover = keyframes`
  0% {
    box-shadow: 0 0 10px 2px rgba(240, 230, 210, 0.3);
  }
  100% {
    box-shadow: 0 0 2px 0px rgba(240, 230, 210, 0);
  }
`;

const ButtonHoverDiv = keyframes`
  0% {
    box-shadow: inset 0 0 10px rgba(240, 230, 210, 0.3);
  }
  100% {
    box-shadow: inset 0 0 3px rgba(240, 230, 210, 0);
  }
`;

/* can't use button, so instead use div https://codepen.io/anon/pen/YjryXp  */
export default styled.div.attrs({
  role: 'button',
  tabIndex: 0,
})` 
  /* Text */
  font-size: ${({ size }) => {
    if (size === 'default' || !size) return 16;
    if (size === 'large') return 20;
    if (size === 'small') return 10;
    return size;
  }}px;
  line-height:100%;
  margin: 1px 6px;
  font-family: 'Beaufort for LOL';
  font-weight: 900;
  font-style: normal;
  text-transform: uppercase;
  color: #cdbe91;
  cursor: pointer;
  user-select: none;  
  background: linear-gradient(to bottom, #c8aa6e 0%, #795c28 100%); /* This is actually the border */
  display: inline-flex;
  flex: 1;
  background-color-provider{
    display: flex;
    background: #1e2328;
    margin: 2px;
    flex: 1; 
    text-align: center;
  }
  child-text-wrapper{
    justify-content: center;
    flex-direction: column;
    text-align: center;
    display: flex;
    padding: ${({ icon }) => (
    icon ? '.4em 1.4em .4em 1em'
      : '.4em 1.4em'
  )};
    flex:1;
  }
  &:focus {
    outline: none;
  }
  &:hover {
    color: #f0e6d2;
    background: #f0e6d7;
    background: linear-gradient(to bottom, #f0e6d7 0%, #c89d3d 100%);
    animation-name: ${ButtonHover};
    animation-duration: 0.75s;
    animation-iteration-count: 1;
    animation-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1);
    background-color-provider {
      background: #1e232a;
      background: linear-gradient(to bottom, #1e232a 20%, #433d2b 100%);
      animation-name: ${ButtonHoverDiv};
      animation-duration: 0.75s;
      animation-iteration-count: 1;
      animation-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1);
    }
    &:active {
      color: #5a5955;
    }
  }
`;

