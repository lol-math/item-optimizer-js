import React from 'react';
import { storiesOf } from '@storybook/react';

import { LolRunes } from 'Images/Icons';
import Button from 'components/Button';

storiesOf('Button', module)
  .add('Basic', () => (
    <Button>
      Button Text
    </Button>
  ))
  .add('Small', () => (
    <Button size="small">
      Small Button
    </Button>
  ))
  .add('Large', () => (
    <Button size="large">
      Large Button
    </Button>
  ))
  .add('Custom Font Size', () => (
    <Button size={33}>
      Custom Size Button
    </Button>
  ))
  .add('Inside Flex Div', () => (
    <div style={{ display: 'flex', height: 100, width: 100 }}>
      <Button>
        Button
      </Button>
    </div>
  ))
  .add('Multiple Buttons', () => (
    <div>
      <Button>
        Button
      </Button>
      <Button>
        Button
      </Button>
    </div>
  ))
  .add('On Click', () => (
    <Button
      onClick={() => { console.log('Button Clicked'); }}
    >
      Button
    </Button>
  ))
  .add('With Icon', () => (
    <Button
      icon={<LolRunes className="anticon" />}
    >
      Runes
    </Button>
  ));
