import React from 'react';
import PropTypes from 'prop-types';
import StyledButton from 'components/Button/StyledButton';
// import './Button.less';

const Button = ({
  size,
  onClick,
  children,
  icon,
}) => (
  <StyledButton
    onClick={onClick}
    size={size}
    icon={icon}
  >
    <background-color-provider>
      <child-text-wrapper>
        <span>{icon || ''}{children}</span>
      </child-text-wrapper>
    </background-color-provider>
  </StyledButton>
);
Button.propTypes = {
  /** Called when the button is clicked by the user. */
  onClick: PropTypes.func.isRequired,
  /** Renders inside of the button */
  children: PropTypes.node,
  /** The size of the button; should be either "small", "default", "large" or a custom number. Represents font-size */
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  /** A possible icon the Button should render. */
  icon: PropTypes.node,
};
Button.defaultProps = {
  children: '',
  size: 'default',
};
Button.displayName = 'Button';
export default Button;

