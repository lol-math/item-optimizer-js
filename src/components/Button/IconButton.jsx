import React from 'react';
import PropTypes from 'prop-types';

const IconButton = ({ children, onClick }) => (
  <button
    style={{
      all: 'inherit',
      padding: '0 5px',
      cursor: 'pointer',
    }}
    onClick={() => onClick()}
  >{children}
  </button>);
IconButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};
export default IconButton;

