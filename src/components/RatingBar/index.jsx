import React from 'react';
import t from 'prop-types';

const RatingBar = ({
  rating, minRating, maxRating, color,
}) => {
  const ratingRange = maxRating - minRating;
  let adaptedRating = (rating - minRating) / ratingRange;
  if (adaptedRating > 1) adaptedRating = 1;
  if (adaptedRating < 0) adaptedRating = 0;
  return (
    <div style={{
    width: '100%', borderWidth: 1, borderStyle: 'solid', borderColor: '#372E18',
  }}
    >
      <div style={{
      width: `${100 * adaptedRating}%`,
      height: 5,
      background: color,
    }}
      />
    </div >);
};
RatingBar.propTypes = {
  rating: t.number.isRequired,
  minRating: t.number,
  maxRating: t.number,
  color: t.string,
};
RatingBar.defaultProps = {
  minRating: 0,
  maxRating: 0,
  color: '#ccc',
};

export default RatingBar;
