import React from 'react';
import t from 'prop-types';
import dd from 'utils/ddragon';

import {
  championFull,
  summoner,
  item,
  map,
  sticker,
  profileicon,
  runesReforged as runes,
} from 'item-optimizer-data';

const champions = championFull.data;
const summoners = summoner.data;
const items = item.data;
const maps = map.data;
const profileicons = profileicon.data;
const stickers = sticker.data;


export const SplashImage = ({ championKey, num, width }) => (
  <DdImage src={dd.images.splash(championKey, num)} alt={champions[championKey].name} height={0.59 * width} width={width} />
);
SplashImage.propTypes = {
  championKey: t.string.isRequired,
  num: t.number.isRequired,
  width: t.number,
};
SplashImage.defaultProps = {
  width: 1215,
};

export const LoadingImage = ({ championKey, num, width }) => (
  <DdImage src={dd.images.loading(championKey, num)} alt={champions[championKey].name} height={1.818 * width} width={width} />
);
LoadingImage.propTypes = {
  championKey: t.string.isRequired,
  num: t.number.isRequired,
  width: t.number,
};
LoadingImage.defaultProps = {
  width: 308,
};

export const TileImage = ({ championKey, num, width }) => (
  <DdImage src={dd.images.tile(championKey, num)} alt={champions[championKey].name} height={width} width={width} />
);
TileImage.propTypes = {
  championKey: t.string.isRequired,
  num: t.number.isRequired,
  width: t.number,
};
TileImage.defaultProps = {
  width: 380,
};

export const ProfileIconImage = ({ iconId, width }) => (
  <DdImage src={dd.images.profileIcon(profileicons[iconId].image.full)} alt={iconId} height={width} width={width} />
);
ProfileIconImage.propTypes = {
  iconId: t.number.isRequired,
  width: t.number,
};
ProfileIconImage.defaultProps = {
  width: 128,
};

export const ChampionImage = ({ championKey, width }) => (
  <DdImage src={dd.images.champion(champions[championKey].image.full)} alt={champions[championKey].name} height={width} width={width} />
);
ChampionImage.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
};
ChampionImage.defaultProps = {
  width: 128,
};

export const PassiveImage = ({ championKey, width }) => (
  <DdImage src={dd.images.passive(champions[championKey].passive.image.full)} alt={champions[championKey].passive.name} height={width} width={width} />
);
PassiveImage.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
};
PassiveImage.defaultProps = {
  width: 64,
};

export const SpellImage = ({ championKey, spellId, width }) => (
  <DdImage src={dd.images.spell(champions[championKey].spells[spellId].image.full)} alt={champions[championKey].spells[spellId].name} height={width} width={width} />
);
SpellImage.propTypes = {
  championKey: t.string.isRequired,
  spellId: t.number.isRequired,
  width: t.number,
};
SpellImage.defaultProps = {
  width: 64,
};

export const SummonerImage = ({ summonerKey, width }) => (
  <DdImage src={dd.images.summoner(summoners[summonerKey].image.full)} alt={summoners[summonerKey].name} height={width} width={width} />
);
SummonerImage.propTypes = {
  summonerKey: t.string.isRequired,
  width: t.number,
};
SummonerImage.defaultProps = {
  width: 64,
};

export const ItemImage = ({ itemId, width }) => (
  <DdImage src={dd.images.item(items[itemId].image.full)} alt={items[itemId].name} height={width} width={width} />
);
ItemImage.propTypes = {
  itemId: t.number.isRequired,
  width: t.number,
};
ItemImage.defaultProps = {
  width: 64,
};

export const MapImage = ({ mapId, width }) => (
  <DdImage src={dd.images.map(maps[mapId].image.full)} alt={maps[mapId].MapName} height={width} width={width} />
);
MapImage.propTypes = {
  mapId: t.number.isRequired,
  width: t.number,
};
MapImage.defaultProps = {
  width: 512,
};

export const StickerImage = ({ stickerName, width }) => (
  <DdImage src={dd.images.sticker(stickers[stickerName].image.full)} alt={stickerName} height={width} width={width} />
);
StickerImage.propTypes = {
  stickerName: t.string.isRequired,
  width: t.number,
};
StickerImage.defaultProps = {
  width: 500,
};

export const RuneImage = ({
  width, selectedTree, selectedRow, selectedRune,
}) => {
  const rune = runes[selectedTree].slots[selectedRow].runes[selectedRune];
  return (
    <DdImage
      src={dd.images.rune(rune.icon)}
      alt={rune.name}
      height={width}
      width={width}
    />
  );
};
RuneImage.propTypes = {
  selectedTree: t.number.isRequired,
  selectedRow: t.number.isRequired,
  selectedRune: t.number.isRequired,
  width: t.number,
};
RuneImage.defaultProps = {
  width: 64,
};

export const RuneTreeImage = ({
  width,
  selectedTree,
}) => (
  <DdImage
    src={dd.images.rune(runes[selectedTree].icon)}
    alt={runes[selectedTree].name}
    height={width}
    width={width}
  />
);
RuneTreeImage.propTypes = {
  selectedTree: t.number.isRequired,
  width: t.number,
};
RuneTreeImage.defaultProps = {
  width: 32,
};

const DdImage = ({
  src, alt, height, width,
}) => (
  <img src={src} alt={alt} height={height} width={width} style={{ verticalAlign: 'bottom' }} />
);
DdImage.propTypes = {
  src: t.string.isRequired,
  alt: t.string.isRequired,
  height: t.number.isRequired,
  width: t.number.isRequired,
};
