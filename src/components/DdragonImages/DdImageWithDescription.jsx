import React from 'react';
import t from 'prop-types';
import {
  championFull,
  summoner,
  item,
  map,
  runesReforged as runes,
} from 'item-optimizer-data';

import { ToolTip as LeagueToolTip } from 'components/ToolTip';
import {
  SplashImage,
  LoadingImage,
  MapImage,
  SummonerImage,
  TileImage,
  ItemImage,
  PassiveImage,
  SpellImage,
  ChampionImage,
  RuneImage,
  RuneTreeImage,
} from 'components/DdragonImages';

const champions = championFull.data;
const summoners = summoner.data;
const items = item.data;
const maps = map.data;

export const withDescription = ({
  name, description, Image, ...rest
}) => (
  <LeagueToolTip
    html={
      <div>
        <h3>{name}</h3>
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>
    }
  >
    <Image {...rest} />
  </LeagueToolTip>
);
withDescription.propTypes = {
  name: t.string.isRequired,
  description: t.string.isRequired,
  Image: t.node.isRequired,
};
withDescription.defaultProps = {};

/**
 * Map has no description, therefore we need a tooltip without description.
 * @param {mapId} number
 * @param {width} number
 */
export const MapImageWithDescription = ({ mapId, ...rest }) => (
  <LeagueToolTip
    html={
      <div>
        <h3>{maps[mapId].MapName}</h3>
      </div>
    }
  >
    <MapImage {...rest} />
  </LeagueToolTip>
);
MapImageWithDescription.propTypes = {
  mapId: t.number.isRequired,
  Image: t.node.isRequired,
};
MapImageWithDescription.defaultProps = {};

export const LoadingImageWithDescription = ({ championKey, ...rest }) =>
  withDescription({
    championKey,
    Image: LoadingImage,
    name: champions[championKey].name,
    description: champions[championKey].title,
    ...rest,
  });
LoadingImageWithDescription.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
};

export const SplashImageWithDescription = ({ championKey, ...rest }) =>
  withDescription({
    championKey,
    Image: SplashImage,
    name: champions[championKey].name,
    description: champions[championKey].title,
    ...rest,
  });
SplashImageWithDescription.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
  num: t.number.isRequired,
};

export const SummonerImageWithDescription = ({ summonerKey, ...rest }) =>
  withDescription({
    summonerKey,
    Image: SummonerImage,
    name: summoners[summonerKey].name,
    description: summoners[summonerKey].description,
    ...rest,
  });
SummonerImageWithDescription.propTypes = {
  summonerKey: t.string.isRequired,
  width: t.number,
};

export const TileImageWithDescription = ({ championKey, ...rest }) =>
  withDescription({
    championKey,
    Image: TileImage,
    name: champions[championKey].name,
    description: champions[championKey].title,
    ...rest,
  });
TileImageWithDescription.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
};

export const ItemImageWithDescription = ({ itemId, ...rest }) =>
  withDescription({
    itemId,
    Image: ItemImage,
    name: items[itemId].name,
    description: items[itemId].description,
    ...rest,
  });
ItemImageWithDescription.propTypes = {
  itemId: t.number.isRequired,
  width: t.number,
};

export const SpellImageWithDescription = ({ championKey, spellId, ...rest }) =>
  withDescription({
    championKey,
    spellId,
    Image: SpellImage,
    name: champions[championKey].spells[spellId].name,
    description: champions[championKey].spells[spellId].description,
    ...rest,
  });
SpellImageWithDescription.propTypes = {
  championKey: t.string.isRequired,
  spellId: t.number.isRequired,
  width: t.number,
};

export const PassiveImageWithDescription = ({ championKey, ...rest }) =>
  withDescription({
    championKey,
    Image: PassiveImage,
    name: champions[championKey].passive.name,
    description: champions[championKey].passive.description,
    ...rest,
  });
PassiveImageWithDescription.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
};

export const ChampionImageWithDescription = ({ championKey, ...rest }) =>
  withDescription({
    championKey,
    Image: ChampionImage,
    name: champions[championKey].name,
    description: champions[championKey].title,
    ...rest,
  });
ChampionImageWithDescription.propTypes = {
  championKey: t.string.isRequired,
  width: t.number,
};

export const RuneImageWithDescription = ({
  selectedTree,
  selectedRow,
  selectedRune,
  ...rest
}) =>
  withDescription({
    selectedTree,
    selectedRow,
    selectedRune,
    Image: RuneImage,
    name: runes[selectedTree].slots[selectedRow].runes[selectedRune].name,
    description:
      runes[selectedTree].slots[selectedRow].runes[selectedRune].longDesc,
    ...rest,
  });
RuneImageWithDescription.propTypes = {
  selectedTree: t.number.isRequired,
  selectedRow: t.number.isRequired,
  selectedRune: t.number.isRequired,
  width: t.number,
};

export const RuneTreeImageWithDescription = ({ selectedTree, ...rest }) =>
  withDescription({
    selectedTree,
    Image: RuneTreeImage,
    name: runes[selectedTree].name,
    description: '',
    ...rest,
  });
RuneTreeImageWithDescription.propTypes = {
  selectedTree: t.number.isRequired,
  width: t.number,
};
