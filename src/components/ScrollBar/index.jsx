import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import styled from 'styled-components';
import './LeagueScrollBars.css';

const renderThumb = ({ style, ...props }) => (
  <div
    className="LeagueScrollBarsThumb"
    style={{ ...style }}
    {...props}
    tabIndex={0}
  />
);

export default class LeagueScrollBars extends Component {
  render() {
    return (
      <Scrollbars
        renderThumbHorizontal={renderThumb}
        renderThumbVertical={renderThumb}
        renderView={props => <div {...props} className="LeagueScrollBarsView" />}
        onUpdate={this.handleUpdate}
        {...this.props}
      />
    );
  }
}
