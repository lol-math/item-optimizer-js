import PropTypes from 'prop-types';
import React from 'react';
import ConnectedValue from 'containers/ConnectedValue';
import ConnectedInput from 'containers/ConnectedInput';
import {
  PassiveImageWithDescription,
  SpellImageWithDescription,
} from 'components/DdragonImages/DdImageWithDescription';
import SkillDictionary from 'Json/SkillDictionary.json';
import { InputNumber } from 'antd';

const getAbilitiesImage = (type, championKey) => {
  switch (type) {
    case 'P':
      return <PassiveImageWithDescription championKey={championKey} />;
    default:
      return (
        <SpellImageWithDescription
          championKey={championKey}
          spellId={SkillDictionary[type]}
        />
      );
  }
};

const CooldownGraph = ({ value, onChange }) =>
  value.map((cooldown, i) => (
    <InputNumber
      value={cooldown}
      onChange={(e) => {
        const newValue = [...value];
        newValue[i] = e;
        onChange(newValue);
      }}
      style={{ width: 60 }}
    />
  ));
const Bool = ({ value }) => (value ? '👍' : '👎');

/**
  Cooldown: PropTypes.arrayOf(PropTypes.number),
  Type: PropTypes.string,
  Target: PropTypes.string,
  SpellEffects: PropTypes.bool,
  PhantomHit: PropTypes.bool,
  LifeSteal: PropTypes.bool,
  CritInteraction: PropTypes.string,
  CanDamageTurret: PropTypes.bool,
  OnHitCount: PropTypes.number,
  MuramanaCount: PropTypes.number
 */
const Ability = ({ ability }) => {
  const getPath = subPath => `championAbilitySet.${ability}.${subPath}`;
  return (
    <div>
      <ConnectedValue path="selectedChampion">
        {championKey => getAbilitiesImage(ability, championKey)}
      </ConnectedValue>
      <div>
        Cooldown:{' '}
        <ConnectedInput path={getPath('Cooldown')} component={CooldownGraph} />
      </div>
      <div>
        Type: <ConnectedValue path={getPath('Type')} />
      </div>
      <div>
        Target: <ConnectedValue path={getPath('Target')} />
      </div>
      <div>
        SpellEffects:{' '}
        <ConnectedInput path={getPath('SpellEffects')} component={Bool} />
      </div>
      <div>
        PhantomHit:{' '}
        <ConnectedInput path={getPath('PhantomHit')} component={Bool} />
      </div>
      <div>
        LifeSteal:{' '}
        <ConnectedInput path={getPath('LifeSteal')} component={Bool} />
      </div>
      <div>
        CritInteraction: <ConnectedValue path={getPath('CritInteraction')} />
      </div>
      <div>
        CanDamageTurret:{' '}
        <ConnectedInput path={getPath('CanDamageTurret')} component={Bool} />
      </div>
      <div>
        OnHitCount: <ConnectedValue path={getPath('OnHitCount')} />
      </div>
      <div>
        MuramanaCount: <ConnectedValue path={getPath('MuramanaCount')} />
      </div>
    </div>
  );
};
Ability.propTypes = {
  ability: PropTypes.string.isRequired,
};

export default Ability;
