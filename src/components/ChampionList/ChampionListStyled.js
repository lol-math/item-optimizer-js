import styled from 'styled-components';

export default styled.div`
  margin: 10px 5px;
  display: grid;
  &:empty:after {
    display: block;
    content: 'No champions match your filters';
    position: absolute;
    margin-top: 15px;
  }
  grid-template-columns: repeat(auto-fill, minmax(64px, 1fr));
  grid-gap: 10px;
`;

