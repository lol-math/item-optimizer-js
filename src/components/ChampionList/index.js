import ChampionList from 'components/ChampionList/ChampionList';
import FilterableChampionList from 'components/ChampionList/FilterableChampionList';

export {
  ChampionList,
  FilterableChampionList,
};
