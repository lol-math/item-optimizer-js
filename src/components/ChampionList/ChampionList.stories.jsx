import React from 'react';
import { storiesOf } from '@storybook/react';

import { championFull } from 'item-optimizer-data';
import { ChampionList, FilterableChampionList } from 'components/ChampionList';
import SelectableChampion from 'components/ChampionList/SelectableChampion';
import RoleFilter from 'components/ChampionList/RoleFilter';
import FilterBar from 'components/ChampionList/FilterBar';

const championsValues = Object.values(champions);

storiesOf('ChampionList', module)
  .add('Basic', () => (
    <ChampionList
      champions={championsValues}
      displayName
      selectedChampion="Elise"
      onSelect={(champion) => { console.log(champion); }}
      selectable
    />
  ))
  .add('Not Selectable', () => (
    <ChampionList
      champions={championsValues}
      displayName
      selectable={false}
    />
  ))
  .add('Selectable Champion Image', () => (
    <SelectableChampion
      displayName
      championKey="Zilean"
    />
  ))
  .add('Filterable Champion List', () => (
    <FilterableChampionList />
  ))
  .add('Filter Bar', () => (
    <FilterBar>
      Some content
    </FilterBar>
  ))
  .add('Role Filter', () => (
    <RoleFilter onChange={value => console.log(value)} />
  ));

