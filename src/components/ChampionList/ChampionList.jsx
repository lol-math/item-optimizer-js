import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
// import './ChampionList.less';
import SelectableChampion from 'components/ChampionList/SelectableChampion';
import StyledChampionList from 'components/ChampionList/ChampionListStyled';

const Wrapper = styled.div`
  justify-content: center;
  align-items: center;
  position: relative;
  display: flex;  
  &::after {
    content: "";
    display: block;
    padding-top: 100%;
  }
`;

const ChampionList = ({
  onChange,
  value,
  shouldDisplayNames,
  selectable,
  champions,
}) =>
  (
    <StyledChampionList>
      {champions.map(champion => (
        <Wrapper
          key={champion.id}
        >
          <SelectableChampion
            championKey={champion.id}
            onClick={onChange}
            selected={champion.id === value}
            displayName={shouldDisplayNames}
            selectable={selectable}
          />
        </Wrapper >

      ))}
    </StyledChampionList>
  );
ChampionList.defaultProps = {
  selectedChampion: '',
  shouldDisplayNames: true,
  selectable: true,
};
ChampionList.displayName = 'ChampionList';
ChampionList.propTypes = {
  /** Triggers when the user selects a champion */
  onChange: PropTypes.func.isRequired,
  /** The currently selected champion (championKey) */
  value: PropTypes.string,
  /** Should names be rendered beneath the images? */
  shouldDisplayNames: PropTypes.bool,
  /** Can the user select a champion? */
  selectable: PropTypes.bool,
  /** A list of champions to be rendered. List consist of Champion Objects */
  champions: PropTypes.arrayOf(PropTypes.shape({
    blurb: PropTypes.string,
    id: PropTypes.string,
    info: PropTypes.shape({
      attack: PropTypes.number,
      defense: PropTypes.number,
      difficulty: PropTypes.number,
      magic: PropTypes.number,
    }),
    name: PropTypes.string,
    stats: PropTypes.shape({
      armor: PropTypes.number,
      armorperlevel: PropTypes.number,
      attackdamage: PropTypes.number,
      attackdamageperlevel: PropTypes.number,
      attackrange: PropTypes.number,
      attackspeedoffset: PropTypes.number,
      attackspeedperlevel: PropTypes.number,
      crit: PropTypes.number,
      critperlevel: PropTypes.number,
      hp: PropTypes.number,
      hpperlevel: PropTypes.number,
      hpregen: PropTypes.number,
      hpregenperlevel: PropTypes.number,
      movespeed: PropTypes.number,
      mp: PropTypes.number,
      mpperlevel: PropTypes.number,
      mpregen: PropTypes.number,
      mpregenperlevel: PropTypes.number,
      spellblock: PropTypes.number,
      spellblockperlevel: PropTypes.number,
    }),
    tags: PropTypes.arrayOf(PropTypes.string),
    title: PropTypes.string,
  })).isRequired,
};
export default ChampionList;
