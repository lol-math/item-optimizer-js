import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SearchBar from 'components/SearchBar';
import styled from 'styled-components';
import { championFull } from 'item-optimizer-data';

import ChampionList from 'components/ChampionList/ChampionList';
import FilterBar from 'components/ChampionList/FilterBar';
import RoleFilter from 'components/ChampionList/RoleFilter';

const championsObject = championFull.data;

const FilterableListWrapper = styled.div`
  position: relative;
`;
const champions = Object.values(championsObject);


const filterChampionsByName = ({ champion, searchText }) => champion.name.toLowerCase().includes(searchText.toLowerCase());
const filterChampionsByRole = ({ champion, role }) => {
  if (!role) return true;
  return champion.tags.includes(role);
};

const sortChampionsByName = (a, b) => {
  if (typeof a !== 'undefined') {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
  }
  return 0;
};
const getChampions = (searchText, role) => champions
  .filter(champion => filterChampionsByName({ champion, searchText }))
  .filter(champion => filterChampionsByRole({ champion, role }))
  .sort(sortChampionsByName);

/** Some Description */
export default class FilterableChampionList extends Component {
  static propTypes = {
    /** Gets called when the user clicks a champion. Returns the championKey. */
    onChange: PropTypes.func,
    /** The championKey of the currently selected champion. (who will get a "selected border") */
    value: PropTypes.string,
  }
  static defaultProps = {
    onChange: () => { },
    value: null,
  }
  static displayName = 'FilterableChampionList'

  static sortChampionsByName = (a, b) => {
    if (typeof a !== 'undefined') {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
    }
    return 0;
  }

  /**
   * This has to be before State, as state references this function.
   * It won't be called until render anyway, so the dependency on state doesn't matter.
   */


  search = (e) => {
    this.setState({
      search: e.target.value,
    });
  }
  roleChange = (role) => {
    this.setState({
      role,
    });
  }
  state = {
    sort: this.sortChampionsByName,
    search: '',
    role: '',
  }


  render() {
    return (
      <FilterableListWrapper>
        <FilterBar>
          <SearchBar
            onChange={this.search}
            placeholder="Search Champion"
          />
          <RoleFilter onChange={this.roleChange} value={this.state.role} />
        </FilterBar>
        <ChampionList
          champions={
            getChampions(this.state.search, this.state.role, this.state.sort)
          }
          onChange={this.props.onChange}
          value={this.props.value}
        />
      </FilterableListWrapper >
    );
  }
}
