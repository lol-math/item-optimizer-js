import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';


// import {
//   SplashImage,
//   LoadingImage,
//   TileImage,
//   ProfileIconImage,
//   ChampionImage,
//   PassiveImage,
//   SpellImage,
//   SummonerImage,
//   ItemImage,
//   MapImage,
//   StickerImage,
// } from 'components/DdragonImages';

// import {
//   SplashImageWithDescription,
//   LoadingImageWithDescription,
//   TileImageWithDescription,
//   ChampionImageWithDescription,
//   PassiveImageWithDescription,
//   SpellImageWithDescription,
//   SummonerImageWithDescription,
//   ItemImageWithDescription,
// } from 'components/DdragonImages/DdImageWithDescription';

// storiesOf('Ddragon Images/Without Tooltip', module)
//   .add('Splash', () => (
//     <SplashImage
//       championKey="Aatrox"
//       num={0}
//     />
//   ))
//   .add('Loading', () => (
//     <LoadingImage
//       championKey="Aatrox"
//       num={0}
//     />
//   ))
//   .add('Tile', () => (
//     <TileImage
//       championKey="Aatrox"
//       num={0}
//     />
//   ))
//   .add('Profile Icon', () => (
//     <ProfileIconImage
//       iconId={0}
//     />
//   ))
//   .add('Champion', () => (
//     <ChampionImage
//       championKey="Aatrox"
//     />
//   ))
//   .add('Passive', () => (
//     <PassiveImage
//       championKey="Aatrox"
//     />
//   ))
//   .add('Spell', () => (
//     <SpellImage
//       championKey="Aatrox"
//       spellId={0}
//     />
//   ))
//   .add('Summoner', () => (
//     <SummonerImage
//       summonerKey="SummonerBarrier"
//     />
//   ))
//   .add('Item', () => (
//     <ItemImage
//       itemId={3001}
//     />
//   ))
//   .add('Map', () => (
//     <MapImage
//       mapId={10}
//     />
//   ))
//   .add('Sticker', () => (
//     <StickerImage
//       stickerName="poro-coolguy"
//     />
//   ));


// storiesOf('Ddragon Images/With Tooltip', module)
//   .add('Splash', () => (
//     <SplashImageWithDescription
//       championKey="Aatrox"
//       num={0}
//     />
//   ))
//   .add('Loading', () => (
//     <LoadingImageWithDescription
//       championKey="Aatrox"
//       num={0}
//     />
//   ))
//   .add('Tile', () => (
//     <TileImageWithDescription
//       championKey="Aatrox"
//       num={0}
//     />
//   ))
//   .add('Champion', () => (
//     <ChampionImageWithDescription
//       championKey="Aatrox"
//     />
//   ))
//   .add('Passive', () => (
//     <PassiveImageWithDescription
//       championKey="Aatrox"
//     />
//   ))
//   .add('Spell', () => (
//     <SpellImageWithDescription
//       championKey="Aatrox"
//       spellId={0}
//     />
//   ))
//   .add('Summoner', () => (
//     <SummonerImageWithDescription
//       summonerKey="SummonerBarrier"
//     />
//   ))
//   .add('Item', () => (
//     <ItemImageWithDescription
//       itemId={3001}
//     />
//   ));
