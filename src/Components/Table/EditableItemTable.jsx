import React, { Component } from 'react';
import EditableText from 'components/EditableText';
import _ from 'lodash';
import { InputNumber, Table } from 'antd';
import ConnectedInput from 'containers/ConnectedInput';

import Jungle from 'Images/jungle.png';
import Support from 'Images/support.png';

import { ClickableItem as Item } from 'components/Item';
import { Item_Profile as itemProfile, item } from 'item-optimizer-data';

import styled from 'styled-components';

const items = item.data;

const UniqueStats = styled.div`
  background: #cccccc11;
`;

const gamePhase = {
  0: 'Early',
  1: 'Mid',
  2: 'Late',
};

const UtilityInputNumber = props => (
  <InputNumber
    {...{
      formatter: value => `${(value * 100).toFixed(1)}%`,
      step: 0.005,
      parser: (value) => {
        console.log(value.replace('%', ''));
        console.log(value.replace('%', '').match(/\d+([.]\d+)?/));
        return (
          +value
            /** replace '%' */
            .replace('%', '')
            /** replace , or . with .0 if it is at the end. */
            .replace(/[,.]+$/, '.0')
            /** replace any non-digit except . */
            .replace(/[^\d.]+/g, '')
            /** select only first two . */
            .match(/\d+([.]\d+)?/)[0] / 100
        );
      },
    }}
    {...props}
  />
);

const UtilityEditor = ({ utility, itemId, path }) => {
  let pathEnding = '';
  if (_.has(utility, 'value')) {
    pathEnding = '.value';
  }
  if (_.isString(utility) || _.isString(utility.value)) {
    return (
      <ConnectedInput
        component={EditableText}
        path={`items.${itemId}.${path}${pathEnding}`}
      />
    );
  } else if (_.isArray(utility) || _.isArray(utility.value)) {
    return (_.has(utility, 'value') ? utility.value : utility).map((v, index) => (
      <div>
        {gamePhase[index]}:{' '}
        <ConnectedInput
          component={UtilityInputNumber}
          path={`items.${itemId}.${path}${pathEnding}[${index}]`}
        />
      </div>
    ));
  }
  return (
    <ConnectedInput
      component={UtilityInputNumber}
      path={`items.${itemId}.${path}${pathEnding}`}
    />
  );
};

export default class EditableItemTable extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'image',
        dataIndex: 'key',
        width: '30px',
        render: (key, record) => <Item itemId={key} />,
      },
      {
        title: 'name',
        dataIndex: 'name',
      },
      {
        title: 'utility',
        dataIndex: 'utility',
        render: (value, record) => (
          <div style={{ textAlign: 'right', margin: 5 }}>
            {/**
             * NORMAL STATS
             */}
            {[
              _.has(record, 'stats.utility') && (
                <div>
                  {[
                    <UtilityEditor
                      itemId={record.key}
                      utility={record.stats.utility}
                      path="stats.utility"
                    />,
                  ]}
                </div>
              ),
              _.has(record, 'stats.Supporting.utility') && (
                <div>
                  {[
                    <img
                      src={Support}
                      alt="Support"
                      style={{ height: 20, marginRight: 10 }}
                    />,
                    <UtilityEditor
                      itemId={record.key}
                      utility={record.stats.Supporting.utility}
                      path="stats.Supporting.utility"
                    />,
                  ]}
                </div>
              ),
              _.has(record, 'stats.Jungling.utility') && (
                <div>
                  {[
                    <img
                      src={Jungle}
                      alt="Jungle"
                      style={{ height: 20, marginRight: 10 }}
                    />,
                    <UtilityEditor
                      itemId={record.key}
                      utility={record.stats.Jungling.utility}
                      path="stats.Jungling.utility"
                    />,
                  ]}
                </div>
              ),
            ]}
            {/**
             * UNIQUE STATS
             */}
            {_.has(record, 'unique') && (
              <UniqueStats>
                {record.unique.map((unique, index) => [
                  _.has(unique, 'stats.utility') && (
                    <div>
                      {[
                        unique.name && `${unique.name}: `,
                        <UtilityEditor
                          itemId={record.key}
                          utility={unique.stats.utility}
                          path={`unique[${index}].stats.utility`}
                        />,
                      ]}
                    </div>
                  ),
                  _.has(unique, 'stats.Supporting.utility') && (
                    <div>
                      {[
                        <img
                          src={Support}
                          alt="Support"
                          style={{ height: 20, marginRight: 10 }}
                        />,
                        <UtilityEditor
                          itemId={record.key}
                          utility={unique.stats.Supporting.utility}
                          path={`unique[${index}].stats.Supporting.utility`}
                        />,
                      ]}
                    </div>
                  ),
                  _.has(unique, 'stats.Jungling.utility') && (
                    <div>
                      {[
                        <img
                          src={Jungle}
                          alt="Jungle"
                          style={{ height: 20, marginRight: 10 }}
                        />,
                        <UtilityEditor
                          itemId={record.key}
                          utility={unique.stats.Jungling.utility}
                          path={`unique[${index}].stats.Jungling.utility`}
                        />,
                      ]}
                    </div>
                  ),
                ])}
              </UniqueStats>
            )}
          </div>
        ),
      },
    ];

    this.state = {
      dataSource: Object.keys(itemProfile).map(key => ({
        key,
        ...itemProfile[key],
        name: items[key].name,
      })),
    };
    this.onCellChange = this.onCellChange.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
  }
  onCellChange(key, dataIndex) {
    return (value) => {
      const dataSource = [...this.state.dataSource];
      const target = dataSource.find(item => item.key === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource });
      }
    };
  }
  onDelete(key) {
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
  }
  handleAdd() {
    const { count, dataSource } = this.state;
    const newData = {
      key: count,
      name: `Edward King ${count}`,
      age: 32,
      address: `London, Park Lane no. ${count}`,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  }
  render() {
    const { dataSource } = this.state;
    return (
      <Table
        pagination={false}
        dataSource={dataSource.filter((item) => {
          if (
            _.has(item, 'stats.utility') ||
            _.has(item, 'stats.Supporting.utility') ||
            _.has(item, 'stats.Jungling.utility')
          ) { return true; }
          if (_.has(item, 'unique')) {
            if (
              item.unique.find(unique =>
                  _.has(unique, 'stats.utility') ||
                  _.has(unique, 'stats.Supporting.utility') ||
                  _.has(unique, 'stats.Jungling.utility'))
            ) {
              return true;
            }
          }
          return false;
        })}
        columns={this.columns}
        size="middle"
      />
    );
  }
}
