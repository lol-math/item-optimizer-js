import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Drawer } from 'antd';
import { NavLink } from 'react-router-dom';
import NavHighLight from 'Images/nav-highlight.png';
import Logo from 'Images/LeagueOfLegendsMathLogo.svg';
import Hamburger from 'Images/hamburger.png';

const ClickableElement = styled.div.attrs({
  tabindex: 0,
})``;

const breakWidth = 768;
const menuBarHeight = 50;
const MenuBarStyle = styled.div`
  height: ${menuBarHeight}px;
  background-color: ${({ theme }) => theme.background.main};
  border-bottom: 1px solid ${({ theme }) => theme.border.grey};
  a {
    font-family: 'Beaufort for LOL';
    color: ${({ theme }) => theme.menu.font};
    display: inline-block;
    text-align: center;
    line-height: ${menuBarHeight}px;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: bottom;
    text-decoration: none;
    text-transform: uppercase;
    font-weight: 500;
    width: 100px;
    &.active {
      color: ${({ theme }) => theme.menu.fontActive};
      background-color: ${({ theme }) => theme.menu.bgActive};
      background-image: url("${NavHighLight}");
    }
  }
  @media (max-width: ${breakWidth}px) {
    a{
      display: block;
      width: auto;
    }
    height: auto;
    width: 100%;
  }
`;

const LogoWrapper = styled.div`
  display: inline-block;
  line-height: ${menuBarHeight}px;
  padding: 0 20px;
  box-sizing: content-box;
  text-align: center;
  @media (max-width: ${breakWidth}px) {
    cursor: pointer;
    background-image: url(${Hamburger});
    background-repeat: no-repeat;
    background-position: left 20px center;
    background-size: 20px;
    display: block;
    padding: 0;
  }
`;

class MenuBar extends React.Component {
  static propTypes = {
    routes: PropTypes.arrayOf(PropTypes.shape({
      to: PropTypes.string,
      exact: PropTypes.bool,
      key: PropTypes.string,
    })).isRequired,
  };
  static displayName = 'MenuBar';
  state = {
    width: document.body.clientWidth,
    drawerOpen: false,
  };

  componentDidMount = () => {
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  };
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateDimensions);
  };

  onClose = () => {
    this.setState({
      drawerOpen: false,
    });
  };
  showDrawer = () => {
    this.setState({
      drawerOpen: true,
    });
  };
  updateDimensions = () => {
    this.setState({ width: document.body.clientWidth });
  };

  render = () => {
    const { width } = this.state;
    const { routes } = this.props;
    if (width <= breakWidth) {
      return [
        <MenuBarStyle>
          <ClickableElement onClick={this.showDrawer}>
            <LogoWrapper>
              <img src={Logo} alt="Logo" width={32} height={32} />
            </LogoWrapper>
          </ClickableElement>
        </MenuBarStyle>,
        <Drawer
          placement="top"
          closable={false}
          onClose={this.onClose}
          visible={this.state.drawerOpen}
          height={menuBarHeight * routes.length}
          style={{ padding: 0, margin: 0 }}
        >
          <MenuBarStyle>
            {routes.map(route => (
              <NavLink
                to={route.to}
                exact={route.exact}
                onClick={this.onClose}
                key={route.key}
              >
                {route.label}
              </NavLink>
            ))}
          </MenuBarStyle>
        </Drawer>,
      ];
    }
    return (
      <MenuBarStyle>
        <LogoWrapper>
          <img src={Logo} alt="Logo" width={32} height={32} />
        </LogoWrapper>
        {this.props.routes.map(route => (
          <NavLink to={route.to} exact={route.exact} key={route.key}>
            {route.label}
          </NavLink>
        ))}
      </MenuBarStyle>
    );
  };
}

export default MenuBar;
