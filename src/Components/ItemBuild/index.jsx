import React from 'react';
import styled from 'styled-components';
import { ClickableItem } from 'components/Item';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { ChatBox, Export, Compare } from 'Images/Icons';

const OrderNumberBox = styled.div`
  display: inline-block;
  text-align: center;
  margin-bottom: 10px;
  font-weight: 500;
  width: ${({ itemWidth }) => itemWidth + 1}px;
`;

const ItemWrapper = styled.div`
  display: inline-table;
  border: 1px solid ${({ theme }) => theme.border.brown};
  border-right-width: 0;
  &:last-child {
    border-right-width: 1px;
  }
`;

const ItemsWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(6, auto);
`;

const ButtonWithoutStyle = styled.div.attrs({
  tabIndex: 0,
})`
  cursor: pointer;
  line-height: 1;
  color: ${({ theme }) => theme.font.main};
  &:hover {
    color: ${({ theme }) => theme.font.bright};
  }
`;

const ItemBuildWrapper = styled.div`
  display: flex;
  flex: 0;
`;
const ContextButtonWrappers = styled.div`
  > * {
    margin: 0 5px;
    margin-bottom: 3px;
  }
`;

const ItemBuild = ({
  items,
  itemWidth,
  displayBuildOrder,
  displayExportButton,
  displayChatButton,
  displayCompareButton,
}) => (
  <ItemBuildWrapper>
    <ItemsWrapper>
      {displayBuildOrder &&
        _.range(6).map(index => (
          <OrderNumberBox itemWidth={itemWidth}>{index + 1}</OrderNumberBox>
        ))}
      {items.map((itemId, index) => (
        <ItemWrapper key={index}>
          <ClickableItem itemId={itemId} width={itemWidth} itemIndex={index} />
        </ItemWrapper>
      ))}
    </ItemsWrapper>

    <ContextButtonWrappers>
      {displayCompareButton && (
      <ButtonWithoutStyle>
        <Compare style={{ width: 15, height: 15 }} alt="Compare this build" />
      </ButtonWithoutStyle>
      )}
      {displayChatButton && (
      <ButtonWithoutStyle>
        <ChatBox style={{ width: 15, height: 15 }} alt="Post build to chat" />
      </ButtonWithoutStyle>
      )}
      {displayExportButton && (
        <ButtonWithoutStyle>
          <Export style={{ width: 15, height: 15 }} alt="Export build" />
        </ButtonWithoutStyle>
      )}
    </ContextButtonWrappers>
  </ItemBuildWrapper>
);
ItemBuild.propTypes = {
  itemWidth: PropTypes.number.isRequired,
  displayBuildOrder: PropTypes.bool,
  displayExportButton: PropTypes.bool,
  displayChatButton: PropTypes.bool,
  displayCompareButton: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.number).isRequired,
};
ItemBuild.defaultProps = {
  displayBuildOrder: true,
  displayExportButton: true,
  displayChatButton: true,
  displayCompareButton: true,
};
export default ItemBuild;
