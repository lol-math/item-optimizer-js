import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const StyledMenu = styled.div``;
export const StyledMenuItemNavLink = styled(NavLink)`
  display: block;
  padding: 5px 10px;
  font-size: 15px;
  color: ${({ theme }) => theme.menu.font};
  &.active {
    background: ${({ theme }) => theme.menu.bgActive};
  }
  &:focus {
    text-decoration: none;
  }
  &:hover {
    color: ${({ theme }) => theme.menu.fontActive};
  }
  .icon {
    display: inline-block;
    margin-right: 5px;
  }
`;
export const MenuHeader = styled.div`
  padding: 5px 10px;
  color: ${({ theme }) => theme.font.bright};
`;
