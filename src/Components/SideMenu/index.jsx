import React from 'react';
import PropTypes from 'prop-types';
import { StyledMenuItemNavLink, StyledMenu, MenuHeader } from './style';

const SideMenu = ({ routes, header }) => (
  <StyledMenu>
    <MenuHeader>{header}</MenuHeader>
    {routes.map(route => (
      <StyledMenuItemNavLink
        to={`/itemop/${route.key}`}
        exact={route.exact}
        // onClick={this.onClose}
        key={route.key}
      >
        <div className="icon">{route.icon}</div>

        {route.label}
      </StyledMenuItemNavLink>
    ))}
  </StyledMenu>
);

SideMenu.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.node,
    name: PropTypes.string,
  })),
};

export default SideMenu;
