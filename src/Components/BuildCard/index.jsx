import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';
import styled from 'styled-components';
import ItemBuild from 'components/ItemBuild';
import { Gold } from 'Images/Icons';

const itemWidth = 35;

const CardContentWrapper = styled.div`
  display: flex;
`;

const CostWrapper = styled.div`
  text-align: center;
  cost {
    /* font-size: 1.5em; */
    font-weight: 500;
    color: ${({ theme }) => theme.font.gold};
  }
`;
const PowerScoreWrapper = styled.div`
  text-align: center;
`;

const Stats = styled.div`
  display: grid;
  margin: 0 10px;
  height: 100%;
`;
const StatIncreaseBar = ({ amount, type, label }) => {
  const LabelWrapper = styled.div`
    display: grid;
    grid-template-columns: auto auto;
  `;
  const ColoredLabel = styled.label`
    color: ${({ theme }) => theme.stats[type]};
    font-weight: 500;
    /* line-height: 20px; */
    text-align: right;
  `;
  const StatIncreaseWrapper = styled.div`
    background-color: #ffffff20;
  `;
  const StatIncreaseAmount = styled.div`
    width: ${amount * 10}%;
    height: 4px;
    background-color: ${({ theme }) => theme.stats[type]};
  `;
  return (
    <div>
      <LabelWrapper>
        <ColoredLabel style={{ textAlign: 'left' }}>{label}</ColoredLabel>
        <ColoredLabel style={{ textAlign: 'right' }}>
          +{+(amount * 100).toFixed(2)}%
        </ColoredLabel>
      </LabelWrapper>
      <StatIncreaseWrapper>
        <StatIncreaseAmount />
      </StatIncreaseWrapper>
    </div>
  );
};
StatIncreaseBar.propTypes = {
  amount: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

const BuildCard = ({
  items,
  statIncreases,
  cost,
  title,
  bodyStyle,
  headStyle,
  highlight,
}) => (
  <Card
    title={title}
    style={{ marginBottom: 16 }}
    bodyStyle={{
      padding: 10,
      backgroundColor: highlight ? 'hsl(221, 88%, 90%)' : null,
      color: highlight ? 'hsl(210, 80%, 27%)' : null,
      ...bodyStyle,
    }}
    headStyle={{
      backgroundColor: highlight ? 'hsl(221, 88%, 90%)' : null,
      color: highlight ? 'hsl(210, 80%, 27%)' : null,
      ...headStyle,
    }}
  >
    <CardContentWrapper>
      <ItemBuild items={items} itemWidth={itemWidth} />

      <div style={{ flex: 1 }}>
        <Stats>
          {[
            {
              type: 'offense',
              label: 'Damage',
            },
            {
              type: 'defense',
              label: 'Defense',
            },
            {
              type: 'utility',
              label: 'Utility',
            },
          ].map(({ type, label }, index) => [
            <StatIncreaseBar
              type={type}
              amount={statIncreases[type]}
              label={label}
              key={index}
            />,
          ])}
        </Stats>
      </div>
      <div>
        <CostWrapper>
          <cost style={{ color: highlight ? 'hsl(0, 100%, 27%)' : null }}>
            {cost} <Gold />
          </cost>
        </CostWrapper>
        <PowerScoreWrapper>
          <div>P score:</div>
          <div>1000</div>
        </PowerScoreWrapper>
      </div>
    </CardContentWrapper>
  </Card>
);
BuildCard.propTypes = {
  items: PropTypes.arrayOf(PropTypes.number).isRequired,
  cost: PropTypes.number.isRequired,
  statIncreases: PropTypes.shape({
    offense: PropTypes.number.isRequired,
    defense: PropTypes.number.isRequired,
    utility: PropTypes.number.isRequired,
  }).isRequired,
};
BuildCard.defaultProps = {
  bodyStyle: {},
};

export default BuildCard;
