import {
  INPUT_CHANGE,
  CHANGE_SKILL_ORDER,
  CHANGE_BLOCKED_ITEMS,
  CHANGE_FORCED_ITEM,
  SELECT_CHAMPION,
} from 'Actions/types';
import _ from 'lodash';
import defaultSettings, { getChampionDefaults } from 'defaultSettings/index';

const initialState = {
  ...defaultSettings,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INPUT_CHANGE: {
      return _.set({ ...state }, action.payload.path, action.payload.value);
    }
    case CHANGE_SKILL_ORDER: {
      return _.set({ ...state }, 'championAbilitySet.Skill_Order', [
        action.payload[0],
        action.payload[1],
        action.payload[2],
        action.payload[0],
        action.payload[0],
        'R',
        action.payload[0],
        action.payload[1],
        action.payload[0],
        action.payload[1],
        'R',
        action.payload[1],
        action.payload[1],
        action.payload[2],
        action.payload[2],
        'R',
        action.payload[2],
        action.payload[2],
      ]);
    }
    case CHANGE_FORCED_ITEM: {
      if (
        state.forcedItems[action.payload.itemIndex] === action.payload.itemId
      ) {
        return _.set(
          { ...state },
          `forcedItems[${action.payload.itemIndex}]`,
          null,
        );
      }
      return _.set(
        { ...state },
        `forcedItems[${action.payload.itemIndex}]`,
        action.payload.itemId,
      );
    }
    case CHANGE_BLOCKED_ITEMS: {
      const { items } = state;
      const { moveKeys, direction } = action.payload;
      moveKeys.forEach((moveKey) => {
        items[moveKey].enabled = direction === 'left';
      });
      return { ...state, items: { ...items } };
    }
    case SELECT_CHAMPION: {
      const { championKey } = action.payload;
      return {
        ...state,
        ...getChampionDefaults(championKey),
      };
    }
    default:
      return state;
  }
};
