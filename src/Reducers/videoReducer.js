import { GET_VIDEOS } from 'Actions/types';

const initialState = {
  videos: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_VIDEOS:
      if (state.videos.filter(video => video.videoId === action.payload.videoId).length === 0) {
        return {
          ...state,
          videos: [
            ...state.videos,
            action.payload,
          ],
        };
      }
      return state;
    default:
      return state;
  }
};
