import { combineReducers } from 'redux';

import settingsReducer from 'Reducers/settingsReducer';

export default combineReducers({
  settings: settingsReducer,
});

