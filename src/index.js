import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import App from 'containers/App'; // eslint-disable-line import/extensions
import { ThemeProvider } from 'styled-components';
import { ConnectedRouter } from 'connected-react-router';
import { ApolloProvider } from 'react-apollo';
import registerServiceWorker from './registerServiceWorker';

import store, { history } from './store';
import { league } from './themes';
import { client } from './apollo';

/* eslint-disable react/jsx-filename-extension */
const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <ApolloProvider client={client}>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            {/* place ConnectedRouter under Provider */}
            <ThemeProvider theme={league}>
              <App />
            </ThemeProvider>
          </ConnectedRouter>
        </Provider>
      </ApolloProvider>
    </AppContainer>,
    document.getElementById('root'),
  );
};
/* eslint-enable react/jsx-filename-extension */

render(App);
// registerServiceWorker();

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('containers/App', () => {
    render(App);
  });
}
