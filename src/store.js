import { createBrowserHistory } from 'history';

import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import { connectRouter } from 'connected-react-router';

import rootReducer from './Reducers';

export const history = createBrowserHistory();

const initialState = {};
const middleware = [thunk];
const store = createStore(
  connectRouter(history)(rootReducer),
  initialState,
  composeWithDevTools(applyMiddleware(...middleware)),
);

export default store;
