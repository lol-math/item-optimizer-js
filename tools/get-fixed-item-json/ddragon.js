const dd = {
  baseurl: 'https://ddragon.leagueoflegends.com',
  images: {
    profileicon(id) {
      return `${dd.baseurl}/img/profileicon/${id}.png`;
    },
    splash(name, num) {
      return `${dd.baseurl}/img/champion/splash/${name}_${num}.jpg`;
    },
    loading(name, num) {
      return `${dd.baseurl}/img/champion/loading/${name}_${num}.jpg`;
    },
    tiles(name, num) {
      return `${dd.baseurl}/img/champion/tiles/${name}_${num}.jpg`;
    },
    champion(name) {
      return `${dd.baseurl}/img/champion/${name}.png`;
    },
    passive(full) {
      return `${dd.baseurl}/img/passive/${full}`;
    },
    spell(full) {
      return `${dd.baseurl}/img/spell/${full}`;
    },
    summoner(name) {
      return `${dd.baseurl}/img/spell/Summoner${name}.png`;
    },
    item(full) {
      return `${dd.baseurl}/img/item/${full}`;
    },
    sprite(type, num) {
      return `${dd.baseurl}/img/sprite/${type}${num}.png`;
    },
    map(id) {
      return `${dd.baseurl}/img/map/map${id}.png`;
    },
    sticker(name) {
      return `${dd.baseurl}/img/sticker/${name}.png`;
    },
    ui: {
      champion() {
        return `${dd.baseurl}/img/ui/champion.png`;
      },
      gold() {
        return `${dd.baseurl}/img/ui/gold.png`;
      },
      items() {
        return `${dd.baseurl}/img/ui/items.png`;
      },
      minion() {
        return `${dd.baseurl}/img/ui/minion.png`;
      },
      score() {
        return `${dd.baseurl}/img/ui/score.png`;
      },
      spells() {
        return `${dd.baseurl}/img/ui/spells.png`;
      },
    },
  },
  data: {
    profileicon(language) {
      return (`${dd.baseurl}/data/${language}/profileicon.json`);
    },
    champion(language) {
      return (`${dd.baseurl}/data/${language}/champion.json`);
    },
    championFull(language) {
      return (`${dd.baseurl}/data/${language}/championFull.json`);
    },
    individualchampion(language, name) {
      return (`${dd.baseurl}/data/${language}champion/${name}.json`);
    },
    item(language) {
      return (`${dd.baseurl}/data/${language}/item.json`);
    },
    summoner(language) {
      return (`${dd.baseurl}/data/${language}/summoner.json`);
    },
    map(language) {
      return (`${dd.baseurl}/data/${language}/map.json`);
    },
    runesReforged(language) {
      return (`${dd.baseurl}/data/${language}/runesReforged.json`);
    },
    language(language) {
      return (`${dd.baseurl}/data/${language}/language.json`);
    },
    sticker(language) {
      return (`${dd.baseurl}/data/${language}/sticker.json`);
    },
  },
  languages() {
    return (`${dd.baseurl}/languages.json`);
  },
};
export default dd;
