const fs = require('fs');
const perks = require('./perks.json');

const perksTooltips = {};

for (const rune of perks) {
  perksTooltips[rune.id] = rune.longDesc;
}
fs.writeFile('perkstooltips.json', JSON.stringify(perksTooltips), (err) => {
  if (err) {
    return console.log(err);
  }

  console.log('The file was saved!');
});
